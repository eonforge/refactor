#ifndef _NODE_H
#define _NODE_H

#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>

#include "compiler.h"
#include "parser.tab.h"
struct type;

enum node_kind {
  NODE_NUMBER,
  NODE_IDENTIFIER,
  NODE_BINARY_OPERATION,
  NODE_EXPRESSION_STATEMENT,
  NODE_STATEMENT_LIST,
  NODE_NULL_STATEMENT,
  NODE_DECL,
  NODE_DECL_LIST,
  NODE_SIGNED_INT,
  NODE_UNSIGNED_INT,
  NODE_CHAR,
  NODE_SHORT,
  NODE_INT, 
  NODE_LONG,
  NODE_EXPR,
  NODE_UNARY_EXPR,
  NODE_TERNARY_EXPR,
  NODE_QUINARY_EXPR,
  NODE_EXPR_LIST,
  NODE_PARAM_LIST,
  NODE_EQUAL,
  NODE_PLUS_EQUAL,
  NODE_MINUS_EQUAL,
  NODE_ASTERISK_EQUAL,
  NODE_SLASH_EQUAL,
  NODE_PERCENT_EQUAL,
  NODE_LEFT_SHIFT_EQUAL,
  NODE_RIGHT_SHIFT_EQUAL,
  NODE_AMPERSAND_EQUAL,
  NODE_XOR_EQUAL,
  NODE_OR_EQUAL,
  NODE_FUNCTION,
  NODE_DEREF,
  NODE_ARRAY_DECL,
  NODE_POINTER,
  NODE_LABEL,
  NODE_DSPECIFIER
};
struct node {
  enum node_kind kind;
  struct location location;
  struct ir_section *ir;
  struct result result;
  int file_scope;
  int visited;
  union {
    struct {
      unsigned long value;
      bool overflow;
      struct result result;
    } number;
    struct {
      char name[IDENTIFIER_MAX + 1];
      struct symbol *symbol;
      int is_op;
      int is_terminal;
      int is_string;
    } identifier;
    struct {
      int is_signed;
    } type;
    struct {
      int operation;
      struct node *left_operand;
      struct node *right_operand;
      struct result result;
    } binary_operation;
    struct {
      struct node *expression;
    } expression_statement;
    struct {
      struct node *init;
      struct node *statement;
      int is_child;
    } statement_list;
    struct {
      struct node *dspec;
      struct node *dlist;
    } decl; 
    struct {
      struct node *dlist;
      struct node *decl;
    } decl_list;
    struct {
      struct node *expr;
    } expr;
    struct {
      struct node *p1;
      struct node *p2;
      int is_array;
      int is_do;
      int is_compound;
      int is_open;
    } unary_expr;
    struct {
      struct node *unary_expr;
      struct node *ternary_op;
      struct node *ternary_expr;
      struct node *bin_op;
      int is_conditional;
      int is_open;
    } ternary_expr;
    struct {
      struct node *p1;
      struct node *p2;
      struct node *p3;
      struct node *p4;
      struct node *p5;
      int is_open;
    } quinary_expr;
    struct {
      struct node *curr;
      struct node *next;
      int length;
      int is_spaced;
      int is_const;
      int is_func_decl;
      int is_cast;
      int is_call;
      int is_return;
      int is_if;
      int is_if_else;
      int is_for_loop;
      int is_do;
      int is_while;
      int is_incr;
    } expr_list;
  } data;
};

enum node_binary_operation {
  BINOP_MULTIPLICATION,
  BINOP_DIVISION,
  BINOP_MOD,
  BINOP_ADDITION,
  BINOP_SUBTRACTION,
  BINOP_ASSIGN,
  BINOP_EQUALITY,
  BINOP_LOGICAL_OR,
  BINOP_LOGICAL_AND,
  BINOP_XOR,
  BINOP_RELATIONAL,
  BINOP_SHIFT
};

/* Constructors */
struct node *node_decl(YYLTYPE location, struct node *dspec, struct node *dlist);
struct node *node_decl_list(YYLTYPE location, struct node *dlist, struct node *decl);
struct node *node_number(YYLTYPE location, char *text);
struct node *node_string(YYLTYPE location, char *text, int length);
struct node *node_identifier(YYLTYPE location, char *text, int length);
struct node *node_identifier_id(YYLTYPE location, char *text, int length);
struct node *node_operator(YYLTYPE location, char *text, int length);
struct node *node_binary_operation(YYLTYPE location, enum node_binary_operation operation,
                                   struct node *left_operand, struct node *right_operand);
struct node *node_expression_statement(YYLTYPE location, struct node *expression);
struct node *node_statement_list(YYLTYPE location, struct node *init, struct node *statement);
struct node *node_null_statement(YYLTYPE location);
struct node *node_expr(YYLTYPE location, struct node *expr);
struct node *node_open_expr(YYLTYPE location, struct node *p1, struct node *p2);
struct node *node_function(YYLTYPE location, struct node *p1, struct node *p2);
struct node *node_compound_expr(YYLTYPE location, struct node *p1, struct node *p2);
struct node *node_array_expr(YYLTYPE location, struct node *decl, struct node *expr);
struct node *node_opent_expr(YYLTYPE location, struct node *unary_expr, struct node *ternary_op, struct node *ternary_expr);
struct node *node_assign(YYLTYPE location, struct node *unary_expr, struct node *ternary_op, struct node *ternary_expr);
struct node *node_binop(YYLTYPE location, enum node_binary_operation type, struct node *unary_expr, struct node *ternary_op, struct node *ternary_expr);
struct node *node_conditional_expr(YYLTYPE location, struct node *cond, struct node *ntrue, struct node *nfalse);
struct node *node_openq_expr(YYLTYPE location, struct node *p1, struct node *p2, struct node *p3, struct node *p4, struct node *p5);
struct node *node_do_expr(YYLTYPE location, struct node *statement, struct node *cond);
struct  node *node_const_expr(YYLTYPE location, struct node *expr);
struct node *node_create(enum node_kind kind, YYLTYPE location);

struct result *node_get_result(struct node *expression);
int is_match(struct node* n, char *str);

struct node *get_statement(struct node *stmt_list, int i);
void node_print_statement_list(FILE *output, struct node *statement_list);

struct node *node_char(YYLTYPE location, int is_signed);
struct node *node_short(YYLTYPE location, int is_signed);
struct node *node_int(YYLTYPE location, int is_signed);
struct node *node_long(YYLTYPE location, int is_signed);
struct node *node_expression_list(YYLTYPE location, struct node *expr_list, struct node *p2, struct node *curr);
struct node *node_expr_list(YYLTYPE location, int len, ...);
struct node *node_spaced_expr(YYLTYPE location, int len, ...);
struct node *node_param_list(YYLTYPE location, int len, ...);
struct node *node_deref(YYLTYPE location, int len, ...);
struct node *node_array_decl(YYLTYPE location, int len, ...);
struct node *node_pointer(YYLTYPE location, int len, ...);
struct node *node_label(YYLTYPE location, int len, ...);
struct node *node_dspec(YYLTYPE location, int len, ...);
struct node *node_function_decl(YYLTYPE location, struct node *decl, struct node *params);
struct node *node_cast(YYLTYPE location, int len, ...);
struct node *node_call(YYLTYPE location, int len, ...);
struct node *node_return(YYLTYPE location, int len, ...);
struct node *node_if(YYLTYPE location, int len, ...);
struct node *node_if_else(YYLTYPE location, int len, ...);
struct node *node_for_loop(YYLTYPE location, int len, ...);
struct node *node_do(YYLTYPE location, int len, ...);
struct node *node_while(YYLTYPE location, int len, ...);
struct node *node_increment(YYLTYPE location, struct node *p1, struct node *p2);

#endif
