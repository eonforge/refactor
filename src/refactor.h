#ifndef _SYMBOL_H
#define _SYMBOL_H

#include <stdio.h>

#include "compiler.h"
struct node;
struct type;

int refactor_statement_list(struct symbol_table *table, struct node *statement_list);

#endif /* _SYMBOL_H */
