%verbose
%debug
%defines
%locations
%pure-parser
%lex-param {yyscan_t scanner}
%parse-param {YYSTYPE *root}
%parse-param {int *error_count}
%parse-param {yyscan_t scanner}
%token-table

%{
  #include <stdio.h>

  #include "compiler.h"
  #include "parser.tab.h"
  #include "scanner.yy.h"
  #include "node.h"

  #define YYERROR_VERBOSE
  static void yyerror(YYLTYPE *loc, YYSTYPE *root,
                      int *error_count, yyscan_t scanner,
                      char const *s);
%}

%token IDENTIFIER NUMBER STRING STRUCT UNION

%token BREAK CHAR CONTINUE DO ELSE FOR GOTO IF
%token INT LONG RETURN SHORT SIGNED UNSIGNED VOID WHILE

%token LEFT_PAREN RIGHT_PAREN LEFT_SQUARE RIGHT_SQUARE LEFT_CURLY RIGHT_CURLY

%token AMPERSAND ASTERISK CARET COLON COMMA EQUAL EXCLAMATION GREATER OPERATOR
%token LESS MINUS PERCENT PLUS SEMICOLON SLASH QUESTION TILDE VBAR

%token AMPERSAND_AMPERSAND AMPERSAND_EQUAL ASTERISK_EQUAL CARET_EQUAL
%token EQUAL_EQUAL EXCLAMATION_EQUAL GREATER_EQUAL GREATER_GREATER
%token GREATER_GREATER_EQUAL LESS_EQUAL LESS_LESS LESS_LESS_EQUAL
%token MINUS_EQUAL MINUS_MINUS PERCENT_EQUAL PLUS_EQUAL PLUS_PLUS
%token SLASH_EQUAL VBAR_EQUAL VBAR_VBAR
%token OR_EQUAL XOR_EQUAL NOT_EQUAL 
%token LOGICAL_OR LOGICAL_AND XOR NOT  
%token RESERVED SCANNING_ERROR

%token LEFT_SHIFT RIGHT_SHIFT
%token LEFT_SHIFT_EQUAL RIGHT_SHIFT_EQUAL

%start translation_unit

%%

abstract_declarator :  pointer {$$ = $1;}  | 
                 direct_abstract_declarator {$$ = $1;} |
                 pointer  direct_abstract_declarator 
		 {$$ = node_expr_list(yyloc, 2, $1, $2);} 

additive_expr :  multiplicative_expr {$$ = $1;} | 
                 additive_expr  add_op  multiplicative_expr  
		 {$$ = node_binop(yyloc, BINOP_ADDITION, $1, $2, $3);}

add_op :  PLUS  | 
          MINUS  

address_expr :  AMPERSAND  cast_expr {$$ = node_expr_list(yyloc, 2, $1, $2);}  

array_declarator :  direct_declarator  LEFT_SQUARE  RIGHT_SQUARE {$$ = node_array_decl(yyloc, 4, $1, $2, NULL, $3);}

array_declarator :  direct_declarator  LEFT_SQUARE  constant_expr  RIGHT_SQUARE {$$ = node_array_decl(yyloc, 4, $1, $2, $3, $4);} 

assignment_expr :  conditional_expr {$$ = $1;}| 
                   unary_expr  assignment_op  assignment_expr  
		   {$$ = node_assign(yyloc, $1, $2, $3);}

assignment_op :  EQUAL |
                 PLUS_EQUAL |
                 MINUS_EQUAL |
                 ASTERISK_EQUAL |
                 SLASH_EQUAL |
                 PERCENT_EQUAL |
                 LEFT_SHIFT_EQUAL |
                 RIGHT_SHIFT_EQUAL |
                 AMPERSAND_EQUAL |
                 XOR_EQUAL |
                 OR_EQUAL 


bitwise_and_expr :  equality_expr {$$ = $1;} | 
                    bitwise_and_expr  AMPERSAND  equality_expr {$$ = node_expr_list(yyloc, 3, $1, $2, $3);} 


bitwise_negation_expr :  TILDE  cast_expr {$$ = node_expr_list(yyloc, 2, $1, $2);} 

bitwise_or_expr :  bitwise_xor_expr {$$ = $1;} | 
                   bitwise_or_expr  VBAR  bitwise_xor_expr {$$ = node_expr_list(yyloc, 3, $1, $2, $3);} 

bitwise_xor_expr :  bitwise_and_expr {$$ = $1;} | 
                    bitwise_xor_expr  XOR  bitwise_and_expr {$$ = node_binop(yyloc, BINOP_XOR, $1, $2, $3);} 

break_statement :  BREAK  SEMICOLON {$$ = node_expr_list(yyloc, 2, $1, $2);} 


cast_expr :  unary_expr {$$ = $1;} | 
             LEFT_PAREN  type_name  RIGHT_PAREN  cast_expr {$$ = node_cast(yyloc, 4, $1, $2, $3, $4);} 



character_type_specifier :  CHAR {$$ = node_char(yyloc, true);} | 
                            SIGNED  CHAR {$$ = node_char(yyloc, true);} | 
                            UNSIGNED  CHAR  {$$ = node_char(yyloc, false);}

comma_expr :  assignment_expr {$$ = $1;} | 
              comma_expr  COMMA  assignment_expr  
	      {$$ = node_spaced_expr(yyloc, 3, $$, $2, $3);}

compound_statement :  LEFT_CURLY  RIGHT_CURLY {$$ = node_expr_list(yyloc, 2, $1, $2);}

compound_statement :  LEFT_CURLY  declaration_or_statement_list  RIGHT_CURLY {$$ = node_expr_list(yyloc, 3, $1, $2, $3);} 

conditional_expr :  logical_or_expr {$$ = $1;} | 
                    logical_or_expr  QUESTION  expr  COLON  conditional_expr {$$ = node_expr_list(yyloc, 5, $1, $2, $3, $4, $5);} 


conditional_statement :  if_statement  | 
                         if_else_statement  

constant :  integer_constant  | 
            string_constant  

constant_expr :  conditional_expr 

continue_statement :  CONTINUE  SEMICOLON {$$ = node_expr_list(yyloc, 2, $1, $2);} 

decl :  declaration_specifiers  initialized_declarator_list  SEMICOLON  
    {$$ = node_decl(yyloc, $1, $2);}

declaration_or_statement :  decl  | 
                            statement  

declaration_or_statement_list :  declaration_or_statement { $$ = node_statement_list(yyloc, NULL, $1);} | 
                                 declaration_or_statement_list  declaration_or_statement  
				 {$$ = node_statement_list(yyloc, $1, $2);}

declaration_specifiers :   type_specifier {$$ = node_dspec(yyloc, 1, $1);} 

declarator :  pointer_declarator  | 
              direct_declarator  



direct_abstract_declarator :  LEFT_PAREN  abstract_declarator  RIGHT_PAREN  {$$ = node_expr_list(yyloc, 3, $1, $2, $3);} |
                              LEFT_PAREN  RIGHT_PAREN {$$ = node_expr_list(yyloc, 2, $1, $2);} |
                              direct_abstract_declarator  LEFT_PAREN  RIGHT_PAREN {$$ = node_expr_list(yyloc, 3, $1, $2, $3);} |
                              LEFT_PAREN  constant_expr  RIGHT_PAREN {$$ = node_expr_list(yyloc, 3, $1, $2, $3);} |
                              direct_abstract_declarator  LEFT_PAREN  constant_expr  RIGHT_PAREN {$$ = node_expr_list(yyloc, 4, $1, $2, $3, $4);} 


direct_declarator :  simple_declarator {$$ = $1;} |
                     LEFT_PAREN  declarator  RIGHT_PAREN {$$ = node_expr_list(yyloc, 3, $1, $2, $3);}  |
                     function_declarator {$$ = $1;} | 
                     array_declarator {$$ = $1;} 

do_statement :  DO  statement  WHILE  LEFT_PAREN  expr  RIGHT_PAREN  SEMICOLON {$$ = node_do(yyloc, 7, $1, $2, $3, $4, $5, $6, $7);} 



equality_expr :  relational_expr {$$ = $1;} | 
                 equality_expr  equality_op  relational_expr  {$$ = node_binop(yyloc, BINOP_EQUALITY, $1, $2, $3);}

equality_op :  EQUAL_EQUAL {$$ = $1;} | 
               NOT_EQUAL {$$ = $1;} 

expr :  comma_expr  

expression_list :  assignment_expr {$$ = $1;} | 
                   expression_list  COMMA  assignment_expr {$$ = node_expr_list(yyloc, 3, $1, $2, $3);} 

expression_statement :  expr  SEMICOLON  
            {$$ = node_expr_list(yyloc, 2, $1, $2);}


for_expr :  LEFT_PAREN  SEMICOLON  SEMICOLON  RIGHT_PAREN {$$ = node_spaced_expr(yyloc, 2, $2, $2);}

for_expr :  LEFT_PAREN  initial_clause  SEMICOLON  SEMICOLON  RIGHT_PAREN {$$ = node_spaced_expr(yyloc, 5, $1, $2, $3, $4, $5);} 

for_expr :  LEFT_PAREN  SEMICOLON  expr  SEMICOLON  RIGHT_PAREN {$$ = node_spaced_expr(yyloc, 5, $1, $2, $3, $4, $5);}

for_expr :  LEFT_PAREN  initial_clause  SEMICOLON  expr  SEMICOLON  RIGHT_PAREN  {$$ = node_spaced_expr(yyloc, 5, $1, $2, $3, $4, $5);}


for_expr :  LEFT_PAREN  SEMICOLON  SEMICOLON  expr  RIGHT_PAREN  {$$ = node_spaced_expr(yyloc, 5, $1, $2, $3, $4, $5);}

for_expr :  LEFT_PAREN  initial_clause  SEMICOLON  SEMICOLON  expr  RIGHT_PAREN {$$ = node_spaced_expr(yyloc, 5, $1, $2, $3, $4, $5);}

for_expr :  LEFT_PAREN  SEMICOLON  expr  SEMICOLON  expr  RIGHT_PAREN {$$ = node_spaced_expr(yyloc, 6, $1, $2, $3, $4, $5, $6);}

for_expr :  LEFT_PAREN  initial_clause  SEMICOLON  expr  SEMICOLON  expr  RIGHT_PAREN {$$ = node_spaced_expr(yyloc, 7, $1, $2, $3, $4, $5, $6, $7);}


for_statement :  FOR  for_expr  statement {$$ = node_for_loop(yyloc, 3, $1, $2, $3);} 

function_call :  postfix_expr  LEFT_PAREN  RIGHT_PAREN {$$ = node_call(yyloc, 3, $1, $2, $3);}


function_call :  postfix_expr  LEFT_PAREN  expression_list  RIGHT_PAREN  {$$ = node_call(yyloc, 4, $1, $2, $3, $4);} 



function_declarator :  direct_declarator  LEFT_PAREN  RIGHT_PAREN {$$ = node_function_decl(yyloc, $1, node_param_list(yyloc, 2, $2, $3));} 

function_declarator :  direct_declarator  LEFT_PAREN  parameter_type_list  RIGHT_PAREN {$$ = node_function_decl(yyloc, $1, node_param_list(yyloc, 3, $2, $3, $4));} 


function_definition :  function_def_specifier  compound_statement {$$ = node_function(yyloc, $1, $2);} 

function_def_specifier :  declaration_specifiers  declarator {$$ = node_expr_list(yyloc, 2, $1, $2);}


goto_statement :  GOTO  named_label  SEMICOLON  {$$ = node_spaced_expr(yyloc, 3, $1, $2, $3);} 


if_else_statement :  IF  LEFT_PAREN  expr  RIGHT_PAREN  statement  ELSE  statement {$$ = node_if_else(yyloc, 7, $1, $2, $3, $4, $5, $6, $7);} 

if_statement :  IF  LEFT_PAREN  expr  RIGHT_PAREN  statement {$$ = node_if(yyloc, 5, $1, $2, $3, $4, $5);} 


indirection_expr : ASTERISK  cast_expr  {$$ = node_deref(yyloc, 2, $1, $2);}

initial_clause :  expr  


initialized_declarator :  declarator  


initialized_declarator_list :  initialized_declarator {$$ = $1;} | 
                               initialized_declarator_list  COMMA  initialized_declarator  
                               {$$ = node_spaced_expr(yyloc, 3, $$, $2, $3);}


integer_constant  : NUMBER

integer_type_specifier :  signed_type_specifier  | 
                          unsigned_type_specifier  | 
                          character_type_specifier  
                         

iterative_statement :  while_statement  | 
                       do_statement  | 
                       for_statement  

label :  named_label  {$$ = node_label(yyloc, 1, $1);}
        
        

labeled_statement :  label  COLON  statement {$$ = node_spaced_expr(yyloc, 3, $1, $2, $3);}  

logical_and_expr :  bitwise_or_expr  {$$ = $1;} | 
                    logical_and_expr  LOGICAL_AND  bitwise_or_expr {$$ = node_binop(yyloc, BINOP_LOGICAL_AND, $1, $2, $3);} 

logical_negation_expr :  NOT  cast_expr {$$ = node_binop(yyloc, BINOP_XOR, $1, $2, node_number(yyloc, "1"));} 

logical_or_expr :  logical_and_expr {$$ = $1;} | 
                   logical_or_expr  LOGICAL_OR  logical_and_expr  
		   {$$ = node_binop(yyloc, BINOP_LOGICAL_OR, $1, $2, $3);}


multiplicative_expr :  cast_expr {$$ = $1;} | 
                       multiplicative_expr  mult_op  cast_expr  
		       {$$ = node_binop(yyloc, BINOP_MULTIPLICATION, $1, $2, $3);}

mult_op :  ASTERISK  | 
           SLASH  | 
           PERCENT  

named_label :  IDENTIFIER  
    

null_statement :  SEMICOLON  


parameter_decl :  declaration_specifiers  declarator {$$ = node_decl(yyloc, $1, $2);} |
                  declaration_specifiers {$$ = node_decl(yyloc, $1, NULL);}  |
                  declaration_specifiers  abstract_declarator {$$ = node_decl(yyloc, $1, $2);} 

parameter_list :  parameter_decl {$$ = $1;} | 
                  parameter_list  COMMA  parameter_decl {$$ = node_decl_list(yyloc, $$, $3);} 

parameter_type_list :  parameter_list  
                      

parenthesized_expr :  LEFT_PAREN  expr  RIGHT_PAREN {$$ = node_expr_list(yyloc, 3, $1, $2, $3);} 

pointer :  ASTERISK {$$ = $1;} | 
           ASTERISK  pointer {$$ = node_pointer(yyloc, 2, $1, $2);} 

pointer_declarator :  pointer  direct_declarator  {$$ = node_pointer(yyloc, 2, $1, $2);} 

postdecrement_expr :  postfix_expr  MINUS_MINUS {$$ = node_increment(yyloc, $1, $2);} 

postfix_expr :  primary_expr  | 
                subscript_expr  | 
                function_call  | 
                postincrement_expr  | 
                postdecrement_expr  


postincrement_expr :  postfix_expr  PLUS_PLUS {$$ = node_increment(yyloc, $1, $2);}

predecrement_expr :  MINUS_MINUS  unary_expr {$$ = node_increment(yyloc, $1, $2);} 

preincrement_expr :  PLUS_PLUS  unary_expr {$$ = node_increment(yyloc, $1, $2);} 



primary_expr :  IDENTIFIER {$$ = $1;} | 
                constant {$$ = $1;} | 
                parenthesized_expr {$$ = $1;} 


relational_expr :  shift_expr {$$ = $1;} | 
                   relational_expr  relational_op  shift_expr {$$ = node_binop(yyloc, BINOP_RELATIONAL, $1, $2, $3);} 

relational_op :  LESS {$$ = $1;} | 
                 LESS_EQUAL {$$ = $1;} | 
                 GREATER {$$ = $1;} | 
                 GREATER_EQUAL {$$ = $1;} 

return_statement :  RETURN  SEMICOLON {$$ = node_spaced_expr(yyloc, 2, $1, $2);} 

return_statement :  RETURN  expr  SEMICOLON {$$ = node_return(yyloc, 3, $1, $2, $3);} 


shift_expr :  additive_expr {$$ = $1;} |  
              shift_expr  shift_op  additive_expr {$$ = node_binop(yyloc, BINOP_SHIFT, $1, $2, $3);} 

shift_op :  LEFT_SHIFT {$$ = $1;} | 
            RIGHT_SHIFT {$$ = $1;} 

signed_type_specifier :  SHORT  {$$ = node_short(yyloc, true);} | 
                         SHORT  INT {$$ = node_int(yyloc, true);} | 
                         SIGNED  SHORT {$$ = node_short(yyloc, true);} | 
                         SIGNED  SHORT  INT {$$ = node_int(yyloc, true);} | 
                         INT {$$ = node_int(yyloc, true);} | 
                         SIGNED  INT {$$ = node_int(yyloc, true); } | 
                         SIGNED {$$ = node_int(yyloc, true);} | 
                         LONG  {$$ = node_long(yyloc, true);} | 
                         LONG  INT {$$ = node_long(yyloc, true);} | 
                         SIGNED  LONG  {$$ = node_long(yyloc, true);} | 
                         SIGNED  LONG  INT  {$$ = node_long(yyloc, true);}

simple_declarator :  IDENTIFIER

statement :  expression_statement  | 
             labeled_statement  | 
             compound_statement  | 
             conditional_statement  | 
             iterative_statement  | 
             break_statement  | 
             continue_statement  | 
             return_statement  | 
             goto_statement  | 
             null_statement  

string_constant  : STRING

subscript_expr :  postfix_expr  LEFT_SQUARE  expr  RIGHT_SQUARE {$$ = node_deref(yyloc, 4, $1, $2, $3, $4);} 

top_level_decl :  decl  | 
                  function_definition  

translation_unit :  top_level_decl { *root = node_statement_list(yyloc, NULL, $1);} | 
                    translation_unit  top_level_decl  
		    { *root = node_statement_list(yyloc, *root, $2); }

type_name :  declaration_specifiers {$$ = $1;} 

type_name :  declaration_specifiers  abstract_declarator {$$ = node_spaced_expr(yyloc, 2, $1, $2);} 


type_specifier : 
                  integer_type_specifier  | 
                  void_type_specifier |
		  class_type_specifier |
		  struct_type_specifier

class_type_specifier : IDENTIFIER

struct_type_specifier : STRUCT IDENTIFIER { $$ = node_spaced_expr(yyloc, 1, $1, $2); } 

unary_expr :  postfix_expr  | 
              unary_minus_expr  | 
              unary_plus_expr  | 
              logical_negation_expr  | 
              bitwise_negation_expr  | 
              address_expr  | 
              indirection_expr  | 
              preincrement_expr  | 
              predecrement_expr  

unary_minus_expr :  MINUS  cast_expr {$$ = node_expr_list(yyloc, 2, $1, $2);} 

unary_plus_expr : PLUS  cast_expr {$$ = node_expr_list(yyloc, 2, $1, $2);} 


unsigned_type_specifier :  UNSIGNED  SHORT {$$ = node_short(yyloc, false);} |
			   UNSIGNED  SHORT  INT {$$ =node_int(yyloc, false);} |
                           UNSIGNED  {$$ = node_int(yyloc, false);}
                           UNSIGNED  INT {$$ = node_int(yyloc, false);} |
                           UNSIGNED  LONG {$$ = node_long(yyloc, false);} |
                           UNSIGNED  LONG  INT {$$ =node_long(yyloc, false);}


void_type_specifier :  VOID {$$ = $1;}

while_statement :  WHILE  LEFT_PAREN  expr  RIGHT_PAREN  statement {$$ = node_while(yyloc, 5, $1, $2, $3, $4, $5);} 



%%

static void yyerror(YYLTYPE *loc,
                    YYSTYPE *root __attribute__((unused)),
                    int *error_count,
                    yyscan_t scanner __attribute__((unused)),
                    char const *s)
{
  fprintf(stdout, "MD.parser: yyerror\n");
  compiler_print_error(*loc, s);
  (*error_count)++;
}

struct node *parser_create_tree(int *error_count, yyscan_t scanner) {
  struct node *parse_tree;
  int result;
  result = yyparse(&parse_tree, error_count, scanner);
  if (result == 1 || *error_count > 0) {
    return NULL;
  } else if (result == 2) {
    fprintf(stdout, "Parser ran out of memory.\n");
    return NULL;
  } else {
    return parse_tree;
  }
}

char const *parser_token_name(int token) {
  return yytname[token - 255];
}

