#ifndef _SYMBOL_H
#define _SYMBOL_H

#include <stdio.h>

#include "compiler.h"
struct node;
struct type;

struct symbol {
  char name[IDENTIFIER_MAX + 1];
  struct result result;
};

struct symbol_list {
  struct symbol symbol;
  struct symbol_table *table;
  struct symbol_list *next;
};

struct symbol_table {
  struct symbol_list *variables;
  struct symbol_table *parent;
  int table_count;
  int reg_count;
};

struct node *get_id(struct node *n);
char *get_name(struct node *n);
char **get_names(struct node *n);
int size(char** a);
struct node *get_expr(struct node *expr, int i);
bool expr_type(int entry);
int next_register(struct symbol_table *table);
struct symbol_table *find_table(struct node *n);

void symbol_initialize_table(struct symbol_table *table);
int symbol_add_from_statement_list(struct symbol_table *table, struct node *statement_list);
void symbol_print_table(FILE *output, struct symbol_table *table);
void symbol_print(FILE* output, struct symbol *sym);

#endif /* _SYMBOL_H */
