#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>

#include "node.h"
#include "symbol.h"
#include "ir.h"
#include "type.h"
#include "debug.h"

/***************************
 * CREATE TYPE EXPRESSIONS *
 ***************************/

struct type *type_basic(bool is_unsigned, enum type_basic_kind datatype) {
  struct type *basic;

  basic = malloc(sizeof(struct type));
  assert(NULL != basic);

  basic->kind = TYPE_BASIC;
  basic->data.basic.is_unsigned = is_unsigned;
  basic->data.basic.datatype = datatype;
  basic->data.basic.parent_type = NULL;
  return basic;
}

/****************************************
 * TYPE EXPRESSION INFO AND COMPARISONS *
 ****************************************/
struct type* peek_second(struct type *t);

static bool compare(enum type_basic_kind a, enum type_basic_kind b) {
  return a == TYPE_BASIC_LONG && b == TYPE_BASIC_LONG || 
         a != TYPE_BASIC_LONG && b != TYPE_BASIC_LONG;
}

static bool type_is_equal(struct type *left, struct type *right) {
  bool eq = false;
  if (!left || !right)
	return left == right;
  if (left->kind == right->kind) {
    switch (left->kind) {
      case TYPE_BASIC:
        eq = left->data.basic.is_unsigned == right->data.basic.is_unsigned
            && compare(left->data.basic.datatype, right->data.basic.datatype);
	break;
      default:
        //assert(0);
        break;
    }
    if (eq) {
	left = peek_parent_type(left);
	if (left && (left->kind == TYPE_FUNCTION || left->kind == TYPE_DECL))
	    left = peek_second(left);
	right = peek_parent_type(right);
	if (right && (right->kind == TYPE_FUNCTION || right->kind == TYPE_DECL))
	    right = peek_second(right);
	eq = (left? left->kind: -1) == (right? right->kind: -1);
    }
  } else {
    eq = false;
  }
  return eq;
}

bool type_is_arithmetic(struct type *t) {
  return TYPE_BASIC == t->kind;
}

bool type_is_unsigned(struct type *t) {
  return type_is_arithmetic(t) && t->data.basic.is_unsigned;
}

static int assertEqual(struct node *n, struct type *t1, struct type *t2);

int type_size(struct type *t) {
  switch (t->kind) {
    case TYPE_BASIC:
      switch (t->data.basic.datatype) {
        case TYPE_BASIC_CHAR:
          return 1;
        case TYPE_BASIC_SHORT:
          return 2;
        case TYPE_BASIC_INT:
          return 4;
        case TYPE_BASIC_LONG:
          return 4;
        default:
          assert(0);
          break;
      }
    case TYPE_POINTER:
      return 4;
    default:
      return 0;
  }
}

static struct type* pop_parent_type(struct node *n);

static struct result *update_result(struct node *n) {
  struct node *expr = n;
  struct result *res = NULL;
  struct result *res1 = NULL;
  if (n) {
      switch (n->kind) {
        case NODE_IDENTIFIER:
          if (!n->data.identifier.is_op && !n->data.identifier.is_terminal &&
		!n->data.identifier.is_string)
             res = node_get_result(n);
          break;
	case NODE_BINARY_OPERATION:
	  res = node_get_result(n);
	  break;
        case NODE_ARRAY_DECL:
        case NODE_POINTER:
        case NODE_EXPR_LIST:
          while (expr) {
              res = update_result(expr->data.expr_list.curr);
              if (res) break;
              expr = expr->data.expr_list.next;
          }
	  break;
	case NODE_DEREF:
   	  break;
	case NODE_TERNARY_EXPR:
	  if (expr->data.ternary_expr.bin_op) {
	      res = update_result(expr->data.ternary_expr.bin_op);
	  } else if (expr->data.ternary_expr.is_conditional) {
	      res = update_result(expr->data.ternary_expr.ternary_op);
	      res1 = update_result(expr->data.ternary_expr.ternary_expr);
	      assertEqual(n, res? res->type: NULL, res1? res1->type: NULL);
	  }
	  break;
        default:
          break;
      }
      if (res) {
        n->result.type = copyType(res->type);
        n->result.ir_operand = res->ir_operand;
      }
  }
  return res;
}


struct type *copyType(struct type *t) {
    struct type *c = NULL;
    struct type *cp = NULL;
    if (!t) return c;
    c = type_basic(t->data.basic.is_unsigned, t->data.basic.datatype);
    c->kind = t->kind;
    cp = c;
    while (t && t->data.basic.parent_type) {
        t = t->data.basic.parent_type;
        cp->data.basic.parent_type = type_basic(t->data.basic.is_unsigned, t->data.basic.datatype);
        cp->data.basic.parent_type->kind = t->kind;
        cp = cp->data.basic.parent_type;
    }
    return c;
}

/*****************
 * TYPE CHECKING *
 *****************/

static int assertEqual(struct node *n, struct type *t1, struct type *t2) {
    bool assertion = type_is_equal(t1, t2);
    if (!assertion) {
        compiler_print_error(n->location, "Type check error: LHS differs from RHS");
	if (t1) type_print(stdout, t1);
	else fprintf(stdout, "no-type");
	fprintf(stdout, " vs ");
	if (t2) type_print(stdout, t2);
	else fprintf(stdout, "no-type");
	fprintf(stdout, "\n");
    	return 1;
    }
    return 0;
}

static int assertTrue(struct node *n, char *msg, bool assertion) {
    if (!assertion) {
        compiler_print_error(n->location, msg);
        return 1;
    }
    return 0;
}

static int type_assign_in_expression(struct node *expression);

static int type_convert_usual_binary(struct node *binary_operation) {
  struct result *r1 = node_get_result(binary_operation->data.binary_operation.left_operand);
  struct result *r2 = node_get_result(binary_operation->data.binary_operation.right_operand);
  int errcount = 0;
  debugf("type_convert_usual_binary.r1=%p, r2=%p, n1=%n, n2=%n, t1=%t, t2=%t\n", r1, r2, binary_operation->data.binary_operation.left_operand, binary_operation->data.binary_operation.right_operand, r1->type, r2->type);
  assert(NODE_BINARY_OPERATION == binary_operation->kind);
  debugf("calling assertEqual1\n");
  errcount += assertEqual(binary_operation, r1? r1->type: NULL, r2? r2->type: NULL);
  if (r1->type)
    binary_operation->result.type = r1->type;
  debugf("type_convert_usual_binary returning t1 = %t\n", binary_operation->result.type);
  return errcount;
}

static int type_convert_assignment(struct node *binary_operation) {
  struct result *r1 = node_get_result(binary_operation->data.binary_operation.left_operand);
  struct result *r2 =  node_get_result(binary_operation->data.binary_operation.right_operand);
  int errcount = 0;
  assert(NODE_BINARY_OPERATION == binary_operation->kind);
  debugf("type_convert_assignment.r1=%p, r2=%p, n1=%n, n2=%n, t1=%t, t2=%t\n", r1, r2, binary_operation->data.binary_operation.left_operand, binary_operation->data.binary_operation.right_operand, r1->type, r2->type);
  debugf("calling assertEqual2\n");
  errcount += assertEqual(binary_operation, r1? r1->type: NULL, r2? r2->type: NULL);
  //binary_operation->data.binary_operation.result.type = r1? copyType(r1->type): NULL;
  return errcount;
}

static int type_assign_in_binary_operation(struct node *binary_operation) {
  int errcount = 0;
  assert(NODE_BINARY_OPERATION == binary_operation->kind);
  errcount += type_assign_in_expression(binary_operation->data.binary_operation.left_operand);
  errcount += type_assign_in_expression(binary_operation->data.binary_operation.right_operand);

  switch (binary_operation->data.binary_operation.operation) {
    case BINOP_MULTIPLICATION:
    case BINOP_DIVISION:
    case BINOP_MOD:
    case BINOP_ADDITION:
    case BINOP_SUBTRACTION:
    case BINOP_EQUALITY:
    case BINOP_LOGICAL_OR:
    case BINOP_XOR:
    case BINOP_RELATIONAL:
    case BINOP_SHIFT:
      errcount += type_convert_usual_binary(binary_operation);
      break;

    case BINOP_LOGICAL_AND:
      break;

    case BINOP_ASSIGN:
      errcount += type_convert_assignment(binary_operation);
      break;

    default:
      assert(0);
      break;
  }
  if (binary_operation->result.type == NULL)
    binary_operation->result.type = binary_operation->data.binary_operation.left_operand->result.type;
  debugf("type_assign_in_binop returning t=%t\n", binary_operation->result.type? binary_operation->result.type: NULL);
  return errcount;
}

static void push_parent(struct type *type, int ptype) {
    if (!type) return;
    while (type->data.basic.parent_type)
        type = type->data.basic.parent_type;
    type->data.basic.parent_type =
        type_basic(type->data.basic.is_unsigned, TYPE_BASIC_INT);
    type->data.basic.parent_type->kind = ptype;
}

static void push_parent_type(struct node *n, int ptype) {
    if (!n->result.type)
	n->result.type = type_basic(n->result.type->data.basic.is_unsigned, TYPE_BASIC_LONG);
    push_parent(n->result.type, ptype);
}

void push_symbol_type(struct symbol *sym, int ptype) {
    struct type *type = NULL;
    if (!sym->result.type)
	sym->result.type = type_basic(sym->result.type->data.basic.is_unsigned, TYPE_BASIC_LONG);
    push_parent(sym->result.type, ptype);
    type = peek_parent_type(sym->result.type);
}

static struct type* pop_parent(struct type *type) {
    struct type *ptype = NULL;
    if (type) {
      if (type->data.basic.parent_type)  {
          while (type->data.basic.parent_type->data.basic.parent_type)
            type = type->data.basic.parent_type;
	ptype = type->data.basic.parent_type;
	ptype = type_basic(ptype->data.basic.is_unsigned, ptype->data.basic.datatype);
	ptype->kind = type->data.basic.parent_type->kind;
        type->data.basic.parent_type = NULL;
      }
    }
    return ptype;
}

static struct type* pop_parent_type(struct node *n) {
    if (!n->result.type) 
	return NULL;
    return pop_parent(n->result.type);
}

struct type* peek_parent_type(struct type *t) {
    struct type *ptype = pop_parent(t);
    if (ptype) push_parent(t, ptype->kind);
    return ptype;
}

struct type* peek_second(struct type *t) {
    struct type *ptype[] = { pop_parent(t), pop_parent(t) };
    if (ptype[1]) push_parent(t, ptype[1]->kind);
    if (ptype[0]) push_parent(t, ptype[0]->kind);
    return ptype[1];
}

struct type *get_type(struct node *n) {
    struct result *res = n? node_get_result(n): NULL;
    return res? res->type: NULL;
}

static bool is_pointer_to(struct type* t, int ptype) {
    struct type *t1 = pop_parent(t);
    struct type *t2 = peek_parent_type(t);
    return t1 && t2 && t2->kind == ptype && t1->kind == TYPE_POINTER;
}

static bool is_parent_to(struct type* t, int ptype) {
    struct type *parent = peek_parent_type(t);
    return t && t->kind == ptype;
}

static bool is_pointer(struct type* t) {
    return is_parent_to(t, TYPE_POINTER);
}

static struct ir_operand *get_operand(struct node *n) {
    return (n && n->kind == NODE_IDENTIFIER)?
        n->data.identifier.symbol->result.ir_operand:
        n? n->result.ir_operand: NULL;
}

static char* msgs = { 
    "Only object and void pointers, and 0 can be assigned to void pointers",
    "Only pointers and 0 can be cast to function pointers",
    "Only function pointers and 0 can be cast to object or void pointers",
    "Only arithmetic and pointer types can be cast to arithmetic type",
    "Only arithmetic and pointer types can be cast to pointer type"
};

static int type_assign_in_op(char *op, struct node *expr, struct node *e1, struct node *e2) {
    int errcount = 0;
    int rank[] = { 20, 30, 40, 50 };
    struct type *t1 = copyType(get_type(e1));
    struct type *t2 = copyType(get_type(e2));
    struct type *t = NULL;
    int bt1 = t1? t1->data.basic.datatype: 0;
    int bt2 = t2? t2->data.basic.datatype: 0;
    struct ir_operand *ir2 = get_operand(e2);
    long v2 = ir2? ir2->data.number: ULONG_MAX;
    if (strcmp(op, "=") == 0) {
	errcount += type_assign_in_expression(e1);
	expr->result.type = t1;
	if (is_pointer_to(t1, TYPE_VOID)) {
	    errcount += assertTrue(expr, msgs[0], !is_pointer_to(t2, TYPE_FUNCTION) && is_pointer(t2) ||
	 	is_pointer_to(t2, TYPE_VOID) || v2 == 0);
	} else if (!is_pointer_to(t1, TYPE_FUNCTION) && is_pointer(t1)) {
	    errcount += assertTrue(expr, msgs[1], is_pointer(t2) || v2 == 0);
	} else if (!is_pointer_to(t1, TYPE_FUNCTION)) {
	    errcount += assertTrue(expr, msgs[2], is_pointer_to(t2, TYPE_FUNCTION) || v2 == 0 || type_is_equal(t1, t2));
	}
    } else if (strcmp(op, "cast") == 0) {
	errcount += type_assign_in_expression(e1);
	t1 = copyType(get_type(e1));
	expr->result.type = t1;
	if (type_is_arithmetic(t1)) {
	    errcount += assertTrue(expr, msgs[3], type_is_arithmetic(t2) || is_pointer(t2));
	} else if (is_pointer(t2)) {
	    errcount += assertTrue(expr, msgs[4], type_is_arithmetic(t2) || is_pointer(t2));
	} else if (is_parent_to(t1, TYPE_ARRAY)) {
	    compiler_print_error(e1->location, "casts to arrays are not permitted\n");
	    expr->result.type = NULL;
	    return ++errcount;
	} else if (t1->kind == TYPE_VOID) {
	    return ++errcount;
	}
    } else if (e1 && e2) {
	if (rank[bt1] >= rank[bt2])
	    expr->result.type = t1;
	else 
	    expr->result.type = t2;
    } else if (e1) {
	t2 = pop_parent(t1);
        if (t1) expr->result.type = t1;
	/*if (t2 && t2->kind == TYPE_ARRAY) {
	    push_parent_type(expr, TYPE_POINTER);
        } else if (t2 && t2->kind == TYPE_FUNCTION) {
	    push_parent_type(expr, TYPE_FUNCTION);
	    push_parent_type(expr, TYPE_POINTER);
        } else*/ if (rank[bt1] < rank[TYPE_BASIC_INT]) {
	    //if (t1) expr->result.type->data.basic.datatype = TYPE_BASIC_INT;
	}
	if (t2) push_parent(t1, t2->kind);
    }
    return errcount;
}

static int type_assign_in_expression_list(struct node *expr) {
    int errcount = 0;
    struct node **nodes = malloc(5 * sizeof(struct node));
    struct type *type = expr? copyType(expr->result.type): NULL;
    struct node *main_expr = expr;
    struct result *res = NULL;
    char *op = NULL;
    int i = 0;
    for (i=0; i<5; i++) nodes[i] = NULL;
    i = 0;
    if (expr->data.expr_list.is_call && expr->result.type) {
        errcount += type_assign_in_expression(get_expr(expr, 2));
	return errcount;
    }
    nodes[i++] = expr; 
    while (expr) {
	debugf("expr_list.curr = %n\n", expr->data.expr_list.curr);
	if (expr->data.expr_list.curr) {
	  errcount += type_assign_in_expression(expr->data.expr_list.curr);
	  debugf("expr_list.curr.0=%n\n", expr->data.expr_list.curr);
	  res = node_get_result(expr->data.expr_list.curr);
	  if (res && res->type && type == NULL) {
            type = res->type;
	    main_expr->result.type = type;
          }
	}
	debugf("expr_list.curr1 = %n\n", expr->data.expr_list.curr);
	if (main_expr->kind == NODE_DSPECIFIER) {
	  if (expr->data.expr_list.curr->result.type) {
	    if (type == NULL) {
		type = expr->data.expr_list.curr->result.type;
	    } else {
		push_parent(type, expr->data.expr_list.curr->result.type->kind);
	    }
	  }
	}
        if (expr->data.expr_list.curr && i < 5) {
            if (expr->data.expr_list.curr->kind == NODE_IDENTIFIER) {
		if (expr->data.expr_list.curr->data.identifier.is_op) 
                   op = expr->data.expr_list.curr->data.identifier.name;
		else if (expr->data.expr_list.curr->data.identifier.is_string) {
		    nodes[i++] = expr->data.expr_list.curr;
	        }
	    } else if (expr->data.expr_list.curr->kind != NODE_STATEMENT_LIST) {
                if (expr->data.expr_list.curr->result.type == NULL)
		    update_result(expr->data.expr_list.curr);
                nodes[i++] = expr->data.expr_list.curr;
            }
        }
        expr = expr->data.expr_list.next;
    }
    if (op) {
        errcount += type_assign_in_op(op, nodes[0], nodes[1], nodes[2]);
    } else if (nodes[0] && nodes[0]->data.expr_list.is_cast) {
	errcount += type_assign_in_op("cast", nodes[0], nodes[1], nodes[2]);
    } else if (nodes[0] && nodes[0]->data.expr_list.is_return) {
	errcount += type_assign_in_op("return", nodes[0], nodes[1], nodes[2]);
    } else {
	errcount += type_assign_in_op("", nodes[0], nodes[1], nodes[2]);
    }
    if (nodes[0]->data.expr_list.is_return) {
	pop_parent(type);
	debugf("return type assign n=%n, t=%t\n", nodes[0]->data.expr_list.next, nodes[0]->result.type);
	type_assign_in_expression(nodes[0]->data.expr_list.next);
	debugf("return.next=%n, t=%t\n", nodes[0]->data.expr_list.next, nodes[0]->data.expr_list.next->result.type);
	nodes[0] = nodes[0]->data.expr_list.next;
    	debugf("calling assertEqual3 n1=%n, t1=%t, t2=%t\n", nodes[0], nodes[0]->result.type, type);
   	type->data.basic.is_unsigned = nodes[0]->result.type->data.basic.is_unsigned;
        errcount += assertEqual(nodes[0], nodes[0]->result.type, type);
	push_parent(type, TYPE_FUNCTION);
    }
    if (main_expr->result.type == NULL)
	main_expr->result.type = type;
    return errcount;
}

static int type_assign_in_expression(struct node *expression) {
  int errcount = 0; 
  struct result * res;
  struct type *type = NULL;
  struct node *expr = expression;
  debugf("type_assign_in_expression.expression=%n\n", expression);
  switch (expression->kind) {
    case NODE_IDENTIFIER:
      if (!expression->data.identifier.is_terminal &&
          !expression->data.identifier.is_op &&
	  !expression->data.identifier.is_string &&
	  (!expression->data.identifier.symbol || 
	   !expression->data.identifier.symbol->result.type)) {
	if (!expression->data.identifier.symbol)
	    expression->data.identifier.symbol = (struct symbol *) malloc(sizeof(struct symbol));
        expression->data.identifier.symbol->result.type = type_basic(false, TYPE_BASIC_INT);
      }
      break;

    case NODE_NUMBER:
      expression->data.number.result.type = type_basic(false, TYPE_BASIC_INT);
      debugf("expression.number.type=%t, n=%n\n", expression->data.number.result.type, expression);
      break;
    case NODE_BINARY_OPERATION:
      errcount += type_assign_in_binary_operation(expression);
      break;
    case NODE_CHAR:
    case NODE_SHORT:
    case NODE_INT:
    case NODE_LONG:
      break;
    case NODE_DEREF:
      debugf("node_deref1=%n, type=%t\n", expression, expression->result.type);
      type_assign_in_expression_list(expression);
      pop_parent_type(expression);
      /*while (expr && expr->data.expr_list.curr) {
	//if (expr->data.expr_list.curr->kind == NODE_DEREF) {
	    if (!expr->data.expr_list.curr->result.type)
	      expr->data.expr_list.curr->result.type = expr->result.type;
	    errcount += type_assign_in_expression(expr->data.expr_list.curr);
	    if (expr->data.expr_list.curr->result.type)
		expression->result.type = expr->data.expr_list.curr->result.type;
            debugf("node_deref1.1=%n, type=%t\n", expr->data.expr_list.curr, expr->data.expr_list.curr->result.type);
	//}
	expr = expr->data.expr_list.next;
      }*/
      debugf("node_deref2=%n, type=%t\n", expression, expression->result.type);
      break;
    case NODE_EXPR_LIST:
      errcount += type_assign_in_expression_list(expression);
      break;
    case NODE_TERNARY_EXPR:
      if (expression->data.ternary_expr.is_conditional) {
        if (expression->data.ternary_expr.unary_expr) {
          errcount += type_assign_in_expression(expression->data.ternary_expr.unary_expr);
	  debugf("ternary_expr.unary_expr.t=%t\n", expression->data.ternary_expr.unary_expr->result.type);
	  res = node_get_result(expression->data.ternary_expr.unary_expr);
	  errcount += assertTrue(expression, "1st part of conditional must be boolean", type_is_arithmetic(res? res->type: NULL));
	}
	if (expression->data.ternary_expr.ternary_op) {
	  errcount += type_assign_in_expression(expression->data.ternary_expr.ternary_op);
	  expression->result.type = expression->data.ternary_expr.ternary_op->result.type;
	}
	if (expression->data.ternary_expr.ternary_expr) {
	  errcount += type_assign_in_expression(expression->data.ternary_expr.ternary_expr);
	  expression->result.type = expression->data.ternary_expr.ternary_expr->result.type;
	}
      }
      if (expression->data.ternary_expr.bin_op) {
          errcount += type_assign_in_binary_operation(expression->data.ternary_expr.bin_op);
	  debugf("ternary_expr.t=%t, n=%n\n", expression->data.ternary_expr.bin_op->result.type, expression);
	  expression->result.type = expression->data.ternary_expr.bin_op->result.type;
      }
      break;
    case NODE_DSPECIFIER:
      errcount += type_assign_in_expression_list(expression);
      break;
    case NODE_STATEMENT_LIST:
      errcount += type_assign_in_statement_list(expression); 
      break;
    default:
      debugf("expression = %n\n", expression);
      assert(0);
      break;
  }
  return errcount;
}

static int type_assign_in_expression_statement(struct node *expression_statement) {
  int errcount = 0;
  switch (expression_statement->kind) {
    case NODE_EXPRESSION_STATEMENT:
      errcount += type_assign_in_expression(expression_statement->data.expression_statement.expression); break;
    case NODE_DECL:
      break;
    case NODE_FUNCTION:
      errcount += type_assign_in_expression(expression_statement->data.unary_expr.p2); break;
    case NODE_TERNARY_EXPR:
    case NODE_EXPR_LIST:
      errcount += type_assign_in_expression(expression_statement); break;
    default:
      assert(0);
  }
  return errcount;
}

int type_assign_in_statement_list(struct node *statement_list) {
  int errcount = 0;
  assert(NODE_STATEMENT_LIST == statement_list->kind);
  if (NULL != statement_list->data.statement_list.init) {
    errcount += type_assign_in_statement_list(statement_list->data.statement_list.init);
  }
  errcount += type_assign_in_expression_statement(statement_list->data.statement_list.statement);
  return errcount;
}


/**************************
 * PRINT TYPE EXPRESSIONS *
 **************************/

static void type_print_basic(FILE *output, struct type *basic) {
  assert(TYPE_BASIC == basic->kind);

  if (basic->data.basic.is_unsigned) {
    fputs("unsigned", output);
  } else {
    fputs("  signed", output);
  }

  switch (basic->data.basic.datatype) {
    case TYPE_BASIC_INT:
      fputs("  int", output);
      break;
    case TYPE_BASIC_LONG:
      fputs(" long", output);
      break;
    case TYPE_BASIC_CHAR:
	fputs("  char", output);
	break;
    case TYPE_BASIC_SHORT:
	fputs("  short", output);
	break;
    default:
      assert(0);
      break;
  }
}

void type_print_any(FILE *output, struct type *type) {
    struct type *head = type;
    if (type && type->kind!=TYPE_VOID && type->kind!=TYPE_LABEL) {
        fprintf(output, type->data.basic.is_unsigned? "unsigned ": "signed ");
        fprintf(output, "%s ", type_string(type->data.basic.datatype));
    }
    while (type && type->data.basic.parent_type) {
      fprintf(output, "(");
      type = type->data.basic.parent_type;
    }
    type = head;
    while (type) {
      if (type->kind==TYPE_POINTER)
        fprintf(output, "<pointer>");
      if (type->kind==TYPE_ARRAY)
        fprintf(output, "<array>");
      if (type->kind==TYPE_FUNCTION)
        fprintf(output, "<function>");
      if (type->kind==TYPE_LABEL)
        fprintf(output, "<label>");
      if (type->kind==TYPE_DECL)
        fprintf(output, "<decl>");
      if (type->kind == TYPE_VOID)
        fprintf(output, "<void>");
      if (type->data.basic.parent_type) fprintf(output, ")");
      type = type->data.basic.parent_type;
    }
    fprintf(output, "\n");
}

void type_print(FILE *output, struct type *kind) {
  struct type *parent_type = peek_parent_type(kind);
  assert(NULL != kind);
  
  if (parent_type) {
      type_print_any(output, parent_type);
      return;
  } 
  switch (kind->kind) {
    case TYPE_BASIC:
      type_print_basic(output, kind);
      break;
    default:
      type_print_any(output, kind);
      break;
  }
}






