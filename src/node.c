#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>

#include "node.h"
#include "symbol.h"
#include "type.h"

/***************************
 * CREATE PARSE TREE NODES *
 ***************************/

/* Allocate and initialize a generic node. */
struct node *node_create(enum node_kind kind, YYLTYPE location) {
  struct node *n;

  n = malloc(sizeof(struct node));
  assert(NULL != n);

  n->kind = kind;
  n->location = location;
  n->file_scope = 0;
  n->visited = 0;

  n->ir = NULL;
  n->result.type = NULL;
  n->result.ir_operand = NULL;
  return n;
}

/*
 * node_identifier - allocate a node to represent an identifier
 *
 * Parameters:
 *   text - string - contains the name of the identifier
 *   length - integer - the length of text (not including terminating NUL)
 *
 * Side-effects:
 *   Memory may be allocated on the heap.
 *
 */
struct node *node_identifier_id(YYLTYPE location, char *text, int length)
{
  struct node *node = node_create(NODE_IDENTIFIER, location);
  memset(node->data.identifier.name, 0, IDENTIFIER_MAX + 1);
  strncpy(node->data.identifier.name, text, length);
  node->data.identifier.symbol = NULL;
  node->data.identifier.is_op = 0;
  node->data.identifier.is_terminal = 0;
  node->data.identifier.is_string = 0;
  return node;
}

struct node *node_identifier(YYLTYPE location, char *text, int length)
{
  struct node *n = node_identifier_id(location, text, length);
  n->data.identifier.is_terminal = 1;
  return n;
}

struct node *node_operator(YYLTYPE location, char *text, int length)
{
  struct node *n = node_identifier_id(location, text, length);
  n->data.identifier.is_op = 1;
  return n;
}

struct node *node_string(YYLTYPE location, char *text, int length)
{
  struct node *n = node_identifier_id(location, text, length);
  n->data.identifier.is_string = 1;
  return n;
}

struct node *node_expression_list(YYLTYPE location, struct node *expr_list, struct node *p2, struct node *curr) {
  struct node *n = node_create(NODE_EXPR_LIST, location);
  struct node *child = node_create(NODE_EXPR_LIST, location);
  n->data.expr_list.curr = curr;
  n->data.expr_list.next = child;
  child->data.expr_list.curr = p2;
  child->data.expr_list.next = expr_list;
  return n;
}

struct node *node_expr_list_with_args(YYLTYPE location, int length, va_list args) {
  struct node *n = node_create(NODE_EXPR_LIST, location);
  struct node *head = n;
  n->data.expr_list.is_spaced = 0;
  n->data.expr_list.is_const = 0;
  n->data.expr_list.is_func_decl = 0;
  n->data.expr_list.is_cast = 0;
  n->data.expr_list.is_call = 0;
  n->data.expr_list.is_return = 0;
  n->data.expr_list.is_if = 0;
  n->data.expr_list.is_if_else = 0;
  n->data.expr_list.is_for_loop = 0;
  n->data.expr_list.is_do = 0;
  n->data.expr_list.is_while = 0;
  while (length > 0) {
     n->data.expr_list.length = length--;
     n->data.expr_list.curr = va_arg(args, struct node*);
     n->data.expr_list.next = length==0? NULL: node_create(NODE_EXPR_LIST, location);
     n = n->data.expr_list.next;
  }
  return head;
}

struct node *node_expr_list(YYLTYPE location, int length, ...) {
  va_list args;
  va_start(args, length);
  return node_expr_list_with_args(location, length, args);
}

struct node *node_spaced_expr(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->data.expr_list.is_spaced = 1;
  return n;
}

struct node *node_cast(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->data.expr_list.is_spaced = 1;
  n->data.expr_list.is_cast = 1;
  return n;
}

struct node *node_call(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->data.expr_list.is_spaced = 1;
  n->data.expr_list.is_call = 1;
  return n;
}

struct node *node_return(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->data.expr_list.is_spaced = 1;
  n->data.expr_list.is_return = 1;
  return n;
}

struct node *node_if(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->data.expr_list.is_spaced = 1;
  n->data.expr_list.is_if = 1;
  return n;
}

struct node *node_if_else(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->data.expr_list.is_spaced = 1;
  n->data.expr_list.is_if_else = 1;
  return n;
}

struct node *node_for_loop(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->data.expr_list.is_spaced = 1;
  n->data.expr_list.is_for_loop = 1;
  return n;
}

struct node *node_do(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->data.expr_list.is_spaced = 1;
  n->data.expr_list.is_do = 1;
  return n;
}

struct node *node_while(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->data.expr_list.is_spaced = 1;
  n->data.expr_list.is_while = 1;
  return n;
}

struct node *node_param_list(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->kind = NODE_PARAM_LIST;
  return n;
}

struct node *node_deref(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->kind = NODE_DEREF;
  return n;
}

struct node *node_array_decl(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->kind = NODE_ARRAY_DECL;
  return n;
}

struct node *node_pointer(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->kind = NODE_POINTER;
  return n;
}

struct node *node_label(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->kind = NODE_LABEL;
  return n;
}

struct node *node_dspec(YYLTYPE location, int length, ...) {
  struct node *n;
  va_list args;
  va_start(args, length);
  n = node_expr_list_with_args(location, length, args);
  n->kind = NODE_DSPECIFIER;
  return n;
}

struct node *node_decl(YYLTYPE location, struct node *dspec, struct node *dlist) {
  struct node *n = node_create(NODE_DECL, location);
  n->data.decl.dspec = dspec;
  n->data.decl.dlist = dlist;
  return n;
}

struct node *node_decl_list(YYLTYPE location, struct node *dlist, struct node *decl) {
  struct node *n = node_create(NODE_DECL_LIST, location);
  n->data.decl_list.dlist = dlist;
  n->data.decl_list.decl = decl;
  return n;
}

struct node *node_expr(YYLTYPE location, struct node *expr) {
  struct node *n = node_create(NODE_EXPR, location);
  n->data.expr.expr = expr;
  return n;
}

struct node *node_unary_expr(YYLTYPE location, struct node *p1, struct node *p2) {
  struct node *n = node_create(NODE_UNARY_EXPR, location);
  n->data.unary_expr.p1 = p1;
  n->data.unary_expr.p2 = p2;
  n->data.unary_expr.is_array = 0;
  n->data.unary_expr.is_do = 0;
  n->data.unary_expr.is_compound = 0;
  n->data.unary_expr.is_open = 0;
  return n;
}

struct node *node_function(YYLTYPE location, struct node *p1, struct node *p2) {
  struct node *n = node_create(NODE_FUNCTION, location);
  n->data.unary_expr.p1 = p1;
  n->data.unary_expr.p2 = p2;
  return n;
}

struct node *node_compound_expr(YYLTYPE location, struct node *decl, struct node *expr)
 {
  struct node *n = node_unary_expr(location, decl, expr);
  n->data.unary_expr.is_compound = 1;
  n->data.unary_expr.is_open = 1;
  return n;
}

struct node *node_open_expr(YYLTYPE location, struct node *decl, struct node *expr)
 {
  struct node *n = node_unary_expr(location, decl, expr);
  n->data.unary_expr.is_open = 1;
  return n;
}

struct node *node_array_expr(YYLTYPE location, struct node *decl, struct node *expr) {
  struct node *n = node_unary_expr(location, decl, expr);
  n->data.unary_expr.is_array = 1;
  n->data.unary_expr.is_open = 1;
  return n;
}

struct node *node_do_expr(YYLTYPE location, struct node *statement, struct node *cond) {
  struct node *n = node_unary_expr(location, statement, cond);
  n->data.unary_expr.is_do = 1;
  return n;
}

struct node *node_const_expr(YYLTYPE location, struct node *expr) {
  struct node *n = node_expr_list(location, 1, expr);
  n->data.expr_list.is_const = 1;
  return n;
}

struct node *node_ternary_expr(YYLTYPE location, struct node *unary_expr, struct node *ternary_op, struct node *ternary_expr) {
  struct node *n = node_create(NODE_TERNARY_EXPR, location);
  n->data.ternary_expr.unary_expr = unary_expr;
  n->data.ternary_expr.ternary_op = ternary_op;
  n->data.ternary_expr.ternary_expr = ternary_expr;
  n->data.ternary_expr.is_conditional = 0;
  n->data.ternary_expr.is_open = 0;
  n->data.ternary_expr.bin_op = NULL;
  return n;
}

struct node *node_opent_expr(YYLTYPE location, struct node *unary_expr, struct node *ternary_op, struct node *ternary_expr) {
  struct node *n = node_ternary_expr(location, unary_expr, ternary_op, ternary_expr);
  n->data.ternary_expr.is_open = 1;
  return n;
}

struct node *node_assign(YYLTYPE location, struct node *unary_expr, struct node *ternary_op, struct node *ternary_expr) {
  struct node *n = node_ternary_expr(location, unary_expr, ternary_op, ternary_expr);
  n->data.ternary_expr.bin_op = node_binary_operation(location, BINOP_ASSIGN, unary_expr, ternary_expr);
  return n;
}

int is_match(struct node* n, char *str) {
  return n && n->kind == NODE_IDENTIFIER &&
        strcmp(str, n->data.identifier.name) == 0;
}

struct node *node_binop(YYLTYPE location, enum node_binary_operation type, struct node *unary_expr, struct node *ternary_op, struct node *ternary_expr) {
  struct node *n = node_ternary_expr(location, unary_expr, ternary_op, ternary_expr);
  if (type == BINOP_MULTIPLICATION) {
    if (is_match(ternary_op, "/"))
    	type = BINOP_DIVISION;
    else if (is_match(ternary_op, "%"))
    	type = BINOP_MOD;
  }
  n->data.ternary_expr.bin_op = node_binary_operation(location, type, unary_expr, ternary_expr);
  return n;
}

struct node *node_binop1(YYLTYPE location, enum node_binary_operation type, struct node *unary_expr, struct node *ternary_op, struct node *ternary_expr) {
  struct node *n = node_expr_list(location, 3, unary_expr, ternary_op, ternary_expr);
  n->data.expr_list.is_spaced = 1;
  return n;
}

struct node *node_quinary_expr(YYLTYPE location, struct node *p1, struct node *p2, struct node *p3, struct node *p4, struct node *p5) {
  struct node *n = node_create(NODE_QUINARY_EXPR, location);
  n->data.quinary_expr.p1 = p1;
  n->data.quinary_expr.p2 = p2;
  n->data.quinary_expr.p3 = p3;
  n->data.quinary_expr.p4 = p4;
  n->data.quinary_expr.p5 = p5;
  n->data.quinary_expr.is_open = 0;
  return n;
}

struct node *node_openq_expr(YYLTYPE location, struct node *p1, struct node *p2, struct node *p3, struct node *p4, struct node *p5) {
  struct node *n = node_quinary_expr(location, p1, p2, p3, p4, p5);
  n->data.quinary_expr.is_open = 1;
  return n;
}


struct node *node_conditional_expr(YYLTYPE location, struct node *cond, struct node *ntrue, struct node *nfalse) {
  struct node *n = node_ternary_expr(location, cond, ntrue, nfalse);
  n->data.ternary_expr.is_conditional = 1;
  return n;
}

struct node *node_function_decl(YYLTYPE location, struct node *p1, struct node *p2) {
  struct node *n = node_expr_list(location, 2, p1, p2);
  n->data.expr_list.is_func_decl = 1;
}

struct node *node_increment(YYLTYPE location, struct node *p1, struct node *p2) {
  struct node *n = node_expr_list(location, 2, p1, p2);
  n->data.expr_list.is_incr = 1;
  return n;
}

/*
 * node_number - allocate a node to represent a number
 *
 * Parameters:
 *   text - string - contains the numeric literal
 *   length - integer - the length of text (not including terminating NUL)
 *
 * Side-effects:
 *   Memory may be allocated on the heap.
 */
struct node *node_number(YYLTYPE location, char *text)
{
  struct node *node = node_create(NODE_NUMBER, location);
  errno = 0;
  node->data.number.value = strtoul(text, NULL, 10);
  if (node->data.number.value == ULONG_MAX && ERANGE == errno) {
    /* Strtoul indicated overflow. */
    node->data.number.overflow = true;
    node->data.number.result.type = type_basic(false, TYPE_BASIC_LONG);
  } else if (node->data.number.value > 0xFFFFFFFFul) {
    /* Value is too large for 32-bit unsigned long type. */
    node->data.number.overflow = true;
    node->data.number.result.type = type_basic(false, TYPE_BASIC_LONG);
  } else {
    node->data.number.overflow = false;
    node->data.number.result.type = type_basic(false, TYPE_BASIC_LONG);
  }

  node->data.number.result.ir_operand = NULL;
  return node;
}

struct node *node_char(YYLTYPE location, int is_signed) {
  struct node *n = node_create(NODE_CHAR, location);
  n->data.type.is_signed = is_signed;
  n->result.type = type_basic(!is_signed, TYPE_BASIC_INT);
  return n; 
}

struct node *node_short(YYLTYPE location, int is_signed) {
  struct node *n = node_create(NODE_SHORT, location);
  n->data.type.is_signed = is_signed;
  n->result.type = type_basic(!is_signed, TYPE_BASIC_SHORT);
  return n;
}

struct node *node_int(YYLTYPE location, int is_signed) {
  struct node *n = node_create(NODE_INT, location);
  n->data.type.is_signed = is_signed;
  n->result.type = type_basic(!is_signed, TYPE_BASIC_INT);
  return n;
}

struct node *node_long(YYLTYPE location, int is_signed) {
  struct node *n = node_create(NODE_LONG, location);
  n->data.type.is_signed = is_signed;
  n->result.type = type_basic(!is_signed, TYPE_BASIC_LONG);
  return n;
}

struct node *node_binary_operation(YYLTYPE location,
                                   enum node_binary_operation operation,
                                   struct node *left_operand,
                                   struct node *right_operand)
{
  struct node *node = node_create(NODE_BINARY_OPERATION, location);
  node->data.binary_operation.operation = operation;
  node->data.binary_operation.left_operand = left_operand;
  node->data.binary_operation.right_operand = right_operand;
  node->data.binary_operation.result.type = NULL;
  node->data.binary_operation.result.ir_operand = NULL;
  return node;
}

struct node *node_expression_statement(YYLTYPE location, struct node *expression)
{
  struct node *node = node_create(NODE_EXPRESSION_STATEMENT, location);
  node->data.expression_statement.expression = expression;
  return node;
}

struct node *node_statement_list(YYLTYPE location,
                                 struct node *init,
                                 struct node *statement)
{
  struct node *node = node_create(NODE_STATEMENT_LIST, location);
  node->data.statement_list.init = init;
  node->data.statement_list.statement = statement;
  node->data.statement_list.is_child = 0;
  return node;
}

struct node *get_statement(struct node *stmt_list, int i) {
  if (!stmt_list) return NULL;
  assert(stmt_list->kind == NODE_STATEMENT_LIST);

  if (i <= 0)
    return stmt_list->data.statement_list.statement;
  else 
    return get_statement(stmt_list->data.statement_list.init, --i);
}

struct node *node_null_statement(YYLTYPE location)
{
  return node_create(NODE_NULL_STATEMENT, location);
}

struct result *node_get_result(struct node *expression) {
  switch (expression->kind) {
    case NODE_NUMBER:
      return &expression->data.number.result;
    case NODE_IDENTIFIER:
      return expression->data.identifier.symbol?
	&expression->data.identifier.symbol->result: NULL;
    case NODE_BINARY_OPERATION:
      return &expression->data.binary_operation.result;
    default:
      return &expression->result;
  }
}

/**************************
 * PRINT PARSE TREE NODES *
 **************************/

static void node_print_expression(FILE *output, struct node *expression);
static void node_print_expression_statement(FILE *output, struct node *expression_statement);
void node_print_decl(FILE *output, struct node *n);
void node_print_decl_list(FILE *output, struct node *n);

static void node_print_binary_operation(FILE *output, struct node *binary_operation) {
  static const char *binary_operators[] = {
    "*",    /*  0 = BINOP_MULTIPLICATION */
    "/",    /*  1 = BINOP_DIVISION */
    "+",    /*  2 = BINOP_ADDITION */
    "-",    /*  3 = BINOP_SUBTRACTION */
    "=",    /*  4 = BINOP_ASSIGN */
    NULL
  };

  assert(NODE_BINARY_OPERATION == binary_operation->kind);

  fputs("(", output);
  node_print_expression(output, binary_operation->data.binary_operation.left_operand);
  fputs(" ", output);
  fputs(binary_operators[binary_operation->data.binary_operation.operation], output);
  fputs(" ", output);
  node_print_expression(output, binary_operation->data.binary_operation.right_operand);
  fputs(")", output);
}

int level = -1;

void indent(FILE *stream) {
  int i;
  for (i=0; i<level; i++)
    fprintf(stream, "    ");
}

void inprintf(FILE *stream, const char *format, int val) {
  indent(stream);
  if (val == INT_MAX)
    fprintf(stream, format);
  else 
    fprintf(stream, format, val);
}

void isprintf(FILE *stream, const char *format, char *val) {
  indent(stream);
  fprintf(stream, format, val);
}

void iprintf(FILE *stream, const char *format) {
  fprintf(stream, format, INT_MAX);
}

static void node_print_number(FILE *output, struct node *number) {
  assert(NODE_NUMBER == number->kind);

  fprintf(output, "%lu", number->data.number.value);
}

/*
 * After the symbol table pass, we can print out the symbol address
 * for each identifier, so that we can compare instances of the same
 * variable and ensure that they have the same symbol.
 */
static void node_print_identifier(FILE *output, struct node *identifier) {
  assert(NODE_IDENTIFIER == identifier->kind);

  fprintf(output, "%s", identifier->data.identifier.name);
  if (identifier->data.identifier.symbol) {
      fprintf(output, " <symbol table");
      symbol_print(output, identifier->data.identifier.symbol);
      fprintf(output, ">" );
  }
}

int is_container(struct node *expression) {
  return expression->kind==NODE_FUNCTION || expression->data.unary_expr.is_open;
}

int is_opent(struct node *expression) {
  return (!expression->data.ternary_expr.unary_expr ||
           expression->data.ternary_expr.is_open);
}

int has_identifiers(int size, ...) {
  int has_id;
  struct node *n;
  va_list(args);
  va_start(args, size);
  has_id = 0;
  while (size-- > 0) {
    n = va_arg(args, struct node*);
    has_id |= n? n->kind == NODE_IDENTIFIER: 0;
  }
  return has_id;
}

void node_print_spaced(FILE *output, struct node *expression, int parent_has_id) {
  if (expression) {
    node_print_expression(output, expression);
    if (!parent_has_id) 
      fprintf(output, " ");
  }
}

static void node_print_expression(FILE *output, struct node *expression) {
  int has_id;
  assert(NULL != expression);

  switch (expression->kind) {
    case NODE_BINARY_OPERATION:
      node_print_binary_operation(output, expression);
      break;
    case NODE_IDENTIFIER:
      node_print_identifier(output, expression);
      break;
    case NODE_NUMBER:
      node_print_number(output, expression);
      break;
    case NODE_DECL:
      node_print_decl(output, expression);
      break;
    case NODE_DECL_LIST:
      node_print_decl_list(output, expression);
      break;
    case NODE_SIGNED_INT:
      fprintf(output, "signed int");
      break;
    case NODE_UNSIGNED_INT:
      fprintf(output, "unsigned int");
      break;
    case NODE_CHAR:
      fprintf(output, expression->data.type.is_signed? "signed char" : "unsigned char");
      break;
    case NODE_SHORT:
    case NODE_INT:
      fprintf(output, expression->data.type.is_signed? "signed int" : "unsigned int");
      break;
    case NODE_LONG:
      fprintf(output, expression->data.type.is_signed? "signed long" : "unsigned long");
      break;
    case NODE_EXPRESSION_STATEMENT:
      node_print_expression_statement(output, expression);
      break;
    case NODE_STATEMENT_LIST:
      node_print_statement_list(output, expression);
      break;
    case NODE_EXPR:
      node_print_expression(output, expression->data.expr.expr);
      break;
    case NODE_FUNCTION:
    case NODE_UNARY_EXPR:
      has_id = has_identifiers(2, expression->data.unary_expr.p1,
        expression->data.unary_expr.p2);
      fprintf(output, (is_container(expression)? "": "("));
      node_print_spaced(output, expression->data.unary_expr.p1, has_id);
      node_print_spaced(output, expression->data.unary_expr.p2, has_id);
      if (!is_container(expression)) fprintf(output, ")");
      break;
    case NODE_TERNARY_EXPR:
      if (!is_opent(expression)) fprintf(output, "(");
      if (expression->data.ternary_expr.unary_expr)
        node_print_expression(output, expression->data.ternary_expr.unary_expr);
      if (expression->data.ternary_expr.ternary_op)
        node_print_expression(output, expression->data.ternary_expr.ternary_op);
      if (expression->data.ternary_expr.ternary_expr)
        node_print_expression(output, expression->data.ternary_expr.ternary_expr);
      if (!is_opent(expression)) fprintf(output, ")");
      break;
    case NODE_QUINARY_EXPR:
      if (!expression->data.quinary_expr.is_open) fprintf(output, "(");
      if (expression->data.quinary_expr.p1)
        node_print_expression(output, expression->data.quinary_expr.p1);
      if (expression->data.quinary_expr.p2)
        node_print_expression(output, expression->data.quinary_expr.p2);
      if (expression->data.quinary_expr.p3)
        node_print_expression(output, expression->data.quinary_expr.p3);
      if (expression->data.quinary_expr.p4)
        node_print_expression(output, expression->data.quinary_expr.p4);
      if (expression->data.quinary_expr.p5)
        node_print_expression(output, expression->data.quinary_expr.p5);
      if (!expression->data.quinary_expr.is_open) fprintf(output, ")");
      break;
    case NODE_PARAM_LIST:
    case NODE_DEREF:
    case NODE_ARRAY_DECL:
    case NODE_POINTER:
    case NODE_LABEL:
    case NODE_DSPECIFIER:
    case NODE_EXPR_LIST:
      while (expression) {
        if (expression->data.expr_list.curr)
          node_print_expression(output, expression->data.expr_list.curr);
	if (expression->data.expr_list.is_spaced)
	   fprintf(output, " ");
	expression = expression->data.expr_list.next; 
      }
      break;
    case NODE_EQUAL: fprintf(output, "="); break;
    case NODE_PLUS_EQUAL: fprintf(output, "+="); break;
    case NODE_MINUS_EQUAL: fprintf(output, "-="); break;
    case NODE_ASTERISK_EQUAL: fprintf(output, "*="); break;
    case NODE_SLASH_EQUAL: fprintf(output, "/="); break;
    case NODE_LEFT_SHIFT_EQUAL: fprintf(output, "<<="); break;
    case NODE_RIGHT_SHIFT_EQUAL: fprintf(output, ">>="); break;
    case NODE_AMPERSAND_EQUAL: fprintf(output, "&="); break;
    case NODE_XOR_EQUAL: fprintf(output, "^="); break;
    case NODE_OR_EQUAL: fprintf(output, "|="); break;
    case NODE_PERCENT_EQUAL: fprintf(output, "%s", "%="); break;
    default:
      fprintf(output, "\nUnprintable type: %d\n", expression->kind);
      assert(0);
      break;
  }
}

void node_print_decl(FILE *output, struct node *n) {
  assert(NODE_DECL == n->kind);
  if (n->data.decl.dspec != NULL)
      node_print_expression(output, n->data.decl.dspec);
  fprintf(output, " ");
  if (n->data.decl.dlist != NULL) 
      node_print_expression(output, n->data.decl.dlist);
}

void node_print_decl_list(FILE *output, struct node *n) {
  assert(NODE_DECL_LIST == n->kind);
  if (n->data.decl_list.decl != NULL)
      node_print_expression(output, n->data.decl_list.decl);
  if (n->data.decl_list.dlist != NULL) {
      fprintf(output, ", ");
      node_print_expression(output, n->data.decl_list.dlist);
  }
}

static void node_print_expression_statement(FILE *output, struct node *expression_statement) {
  assert(NODE_EXPRESSION_STATEMENT == expression_statement->kind);

  node_print_expression(output, expression_statement->data.expression_statement.expression);

}

void node_print_statement_list(FILE *output, struct node *statement_list) {
  assert(NODE_STATEMENT_LIST == statement_list->kind);

  if (!statement_list->data.statement_list.is_child) {
    level++;
    fputs("\n", output);
  } 
  /* indent(output); */
  if (statement_list->data.statement_list.init) {
    statement_list->data.statement_list.init->data.statement_list.is_child = 1;
    node_print_statement_list(output, statement_list->data.statement_list.init);
  }
  if (statement_list->data.statement_list.statement != NULL) {
    indent(output);
    switch (statement_list->data.statement_list.statement->kind) {
      case NODE_EXPRESSION_STATEMENT:
        node_print_expression_statement(output, statement_list->data.statement_list.statement);
	break;
      case NODE_DECL:
        node_print_decl(output, statement_list->data.statement_list.statement);
	fprintf(output, "; ");
	break;
      case NODE_FUNCTION:
      case NODE_EXPR:
      case NODE_UNARY_EXPR:
      case NODE_TERNARY_EXPR:
      case NODE_QUINARY_EXPR:
      case NODE_EXPR_LIST:
        node_print_expression(output, statement_list->data.statement_list.statement);
	break;
      default: 
      assert(0);
    }
    if (statement_list->data.statement_list.is_child)
      fputs("\n", output); 
  }
  if (!statement_list->data.statement_list.is_child) {
    level--;
    fputs("\n", output); 
    indent(output);
  }
}

