#ifndef _IR_H
#define _IR_H

#include <stdio.h>
#include <stdbool.h>

struct node;
struct symbol;
struct symbol_table;

enum ir_operand_kind {
  OPERAND_NUMBER,
  OPERAND_TEMPORARY,
  OPERAND_IDENTIFIER
};
struct ir_operand {
  enum ir_operand_kind kind;
  int is_signed;

  union {
    unsigned long number;
    long signed_number;
    int temporary;
    char *name;
  } data;
};

enum ir_instruction_kind {
  IR_NO_OPERATION,
  IR_MULTIPLY,
  IR_DIVIDE,
  IR_MOD,
  IR_ADD,
  IR_ADD_IMMEDIATE,
  IR_SUBTRACT,
  IR_OR,
  IR_XOR,
  IR_AND,
  IR_SLT,
  IR_SLL,
  IR_SRL,
  IR_JR,
  IR_JL,
  IR_J,
  IR_BEQ,
  IR_BNE,
  IR_DATA,
  IR_LOAD_ADDRESS,
  IR_LOAD_IMMEDIATE,
  IR_LOAD_BYTE,
  IR_LOAD_WORD,
  IR_STORE_WORD,
  IR_STORE_BYTE,
  IR_COPY,
  IR_ADDRESS_OF,
  IR_LABEL,
  IR_DECL,
  IR_PRINT_NUMBER,
  IR_CALL,
  IR_PROC_BEGIN,
  IR_PROC_END
};
struct ir_instruction {
  enum ir_instruction_kind kind;
  struct ir_instruction *prev, *next;
  struct ir_operand operands[3];
  int offset;
};

struct ir_section {
  struct ir_instruction *first, *last;
};

int ir_generate_for_statement_list(struct node *statement_list);

struct ir_operand* new_operand(long number);

struct ir_operand *new_register(int offset);

struct ir_instruction *ir_instruction(enum ir_instruction_kind kind);

void ir_operand_register(struct ir_instruction *instruction, int position, int offset);

void ir_operand_identifier(struct ir_instruction *instruction, int position, char *name);

struct ir_section *ir_deep_copy(struct ir_section *orig);

void ir_print_section(FILE *output, struct ir_section *section);
struct ir_section *ir_section(struct ir_instruction *first, struct ir_instruction *last);

#endif
