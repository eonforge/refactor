#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <limits.h>

#include "node.h"
#include "symbol.h"
#include "type.h"
#include "ir.h"
#include "debug.h"

#define ZERO_REG_OFFSET	-8
#define V0_REG_OFFSET	-6
#define ARG0_REG_OFFSET	-4
#define STACK_PTR_OFFSET  21
#define RA_REG_OFFSET	23


/************************
 * CREATE IR STRUCTURES *
 ************************/


static int for_count = 0;
static int const_count = 0;
static int if_count = 0;


/*
 * An IR section is just a list of IR instructions. Each node has an associated
 * IR section if any code is required to implement it.
 */
struct ir_section *ir_section(struct ir_instruction *first, struct ir_instruction *last) {
  struct ir_section *code;
  code = malloc(sizeof(struct ir_section));
  assert(NULL != code);

  code->first = first;
  code->last = last;
  return code;
}

static struct ir_section *ir_append(struct ir_section *section,
                                                           struct ir_instruction *instruction);

static struct ir_section *ir_copy(struct ir_section *orig) {
  return ir_section(orig->first, orig->last);
}

struct ir_section *ir_deep_copy(struct ir_section *orig) {
  struct ir_section * ir = ir_section(NULL, NULL);
  struct ir_instruction *curr = NULL;
  struct ir_instruction *copy = NULL;
  int i = 0;
  for (curr = orig->first; curr != orig->last; curr = curr->next) {
    copy = ir_instruction(curr->kind);
    copy->offset = curr->offset;
    for (i=0; i<3; i++) 
	copy->operands[i] = curr->operands[i];
    ir = ir_append(ir, copy);
  } 
  return ir;
}

bool is_empty(struct ir_section *ir) {
  return ir == NULL || (ir->first == NULL && ir->last == NULL);
}
/*
 * This joins two IR sections together into a new IR section.
 */
static struct ir_section *ir_concatenate(struct ir_section *before, struct ir_section *after) {
  /* patch the two sections together */
  before->last->next = after->first;
  after->first->prev = before->last;

  return ir_section(before->first, after->last);
}

static struct ir_section *ir_append(struct ir_section *section,
                                                           struct ir_instruction *instruction) {
  if (NULL == section) {
    section = ir_section(instruction, instruction);

  } else if (NULL == section->first || NULL == section->last) {
    assert(NULL == section->first && NULL == section->last);
    section->first = instruction;
    section->last = instruction;
    instruction->prev = NULL;
    instruction->next = NULL;

  } else {
    instruction->next = section->last->next;
    if (NULL != instruction->next) {
      instruction->next->prev = instruction;
    }
    section->last->next = instruction;

    instruction->prev = section->last;
    section->last = instruction;
  }
  return section;
}

/*
 * An IR instruction represents a single 3-address statement.
 */
struct ir_instruction *ir_instruction(enum ir_instruction_kind kind) {
  struct ir_instruction *instruction;

  instruction = malloc(sizeof(struct ir_instruction));
  assert(NULL != instruction);

  instruction->kind = kind;

  instruction->next = NULL;
  instruction->prev = NULL;

  instruction->offset = 0;

  return instruction;
}

static void ir_operand_number(struct ir_instruction *instruction, int position, struct node *number) {
  instruction->operands[position].kind = OPERAND_NUMBER;
  instruction->operands[position].data.number = number->data.number.value;
  instruction->operands[position].is_signed = 0;
}

static void ir_operand_signed_number(struct ir_instruction *instruction, int position, long number) {
  instruction->operands[position].kind = OPERAND_NUMBER;
  instruction->operands[position].data.signed_number = number;
  instruction->operands[position].is_signed = 1;
}

static void ir_operand_temporary(struct ir_instruction *instruction, int position) {
  static int next_temporary;
  instruction->operands[position].kind = OPERAND_TEMPORARY;
  while (!is_valid_reg(++next_temporary))
    next_temporary += 0;
  instruction->operands[position].data.temporary = next_temporary;
}

void ir_operand_identifier(struct ir_instruction *instruction, int position, char *name) {
  instruction->operands[position].kind = OPERAND_IDENTIFIER;
  instruction->operands[position].data.name = name;
}

void ir_operand_register(struct ir_instruction *instruction, int position, int offset) {
  instruction->operands[position].kind = OPERAND_TEMPORARY;
  instruction->operands[position].data.temporary = offset;
}

static void ir_operand_generic(struct ir_instruction *instruction, int pos, int op, enum ir_operand_kind type, char*name) {
  if (type == OPERAND_TEMPORARY) ir_operand_register(instruction, pos, op);
  else if (type == OPERAND_NUMBER) ir_operand_signed_number(instruction, pos, op);
  else if (type == OPERAND_IDENTIFIER) ir_operand_identifier(instruction, pos, name);
}

static struct ir_instruction *ir_instruction_triple(enum ir_instruction_kind kind, int op1
, int type1, int op2, int type2, int op3, int type3, char *name) {
  struct ir_instruction *instruction = ir_instruction(kind);
  ir_operand_generic(instruction, 0, op1, type1, name);
  ir_operand_generic(instruction, 1, op2, type2, name);
  ir_operand_generic(instruction, 2, op3, type3, name);
  return instruction;
}

static struct ir_instruction *ir_instruction_tuple(enum ir_instruction_kind kind, int op1, int type1, int op2, int type2, char *name) {
  struct ir_instruction *instruction = ir_instruction(kind);
  ir_operand_generic(instruction, 0, op1, type1, name);
  ir_operand_generic(instruction, 1, op2, type2, name);
  return instruction;
}

static struct ir_instruction *ir_instruction_single(enum ir_instruction_kind kind, int op1, int type1, char *name) {
  struct ir_instruction *instruction = ir_instruction(kind);
  ir_operand_generic(instruction, 0, op1, type1, name);
  return instruction;
}

static void ir_operand_copy(struct ir_instruction *instruction, int position, struct ir_operand *operand) {
    instruction->operands[position] = *operand;
}

struct ir_operand *new_operand(long number) {
  struct ir_operand *n;
  n = malloc(sizeof(struct ir_operand));
  assert(NULL != n);
  n->kind = OPERAND_NUMBER;
  n->data.number = number;

  return n;
}

struct ir_operand *new_register(int offset) {
  struct ir_operand *n = new_operand(0);
  n->kind = OPERAND_TEMPORARY;
  n->data.temporary = offset;
  return n;
}

int get_next_register(struct node *n) {
  return next_register(n? n->result.table: NULL);
}

static struct ir_operand* ir_operand_get(struct node *n);
struct ir_section *push(struct ir_operand *reg);
struct ir_section *pop(struct ir_operand *reg);
char* concat_str(char* s1, int i);

/*******************************
 * GENERATE IR FOR EXPRESSIONS *
 *******************************/
static int ir_generate_for_number(struct node *number) {
  int errcount = 0;
  struct ir_instruction *instruction;
  assert(NODE_NUMBER == number->kind);

  instruction = ir_instruction(IR_LOAD_IMMEDIATE);
  ir_operand_register(instruction, 0, get_next_register(number));
  ir_operand_number(instruction, 1, number);

  number->ir = ir_section(instruction, instruction);

  number->data.number.result.ir_operand = &instruction->operands[0];
  return errcount;
}


static int ir_generate_for_identifier(struct node *identifier) {
  int errcount = 0;
  struct ir_instruction *instruction;
  struct node *n = identifier;
  assert(NODE_IDENTIFIER == identifier->kind);
  instruction = ir_instruction(IR_NO_OPERATION);
  identifier->ir = ir_section(instruction, instruction);
  if (!identifier->data.identifier.symbol) 
     identifier->data.identifier.symbol = (struct symbol *) malloc(sizeof(struct symbol));
  if (!identifier->data.identifier.symbol->result.ir_operand) {
     identifier->data.identifier.symbol->result.ir_operand = new_operand(0);
     identifier->data.identifier.symbol->result.ir_operand->kind = OPERAND_TEMPORARY;
  }
  if (identifier->data.identifier.is_string) {
    n->ir = ir_section(NULL, NULL);
    ir_append(n->ir, ir_instruction(IR_DATA));
    ir_operand_identifier(n->ir->last, 0, concat_str("str", ++const_count));
    ir_operand_identifier(n->ir->last, 1, ".asciiz");
    ir_operand_identifier(n->ir->last, 2, identifier->data.identifier.name);
    ir_append(n->ir, ir_instruction(IR_LOAD_ADDRESS));
    n->ir->last->operands[0] = *identifier->data.identifier.symbol->result.ir_operand;
    ir_operand_identifier(n->ir->last, 1, concat_str("str", const_count));
  }
  assert(NULL != identifier->data.identifier.symbol->result.ir_operand);
  return errcount;
}

static int ir_generate_for_expression(struct node *expression);

static struct ir_operand* ir_operand_get(struct node *n) {
  if (node_get_result(n) && node_get_result(n)->ir_operand) 
    return node_get_result(n)->ir_operand;
  else if (n->ir && n->ir->last) 
    return &n->ir->last->operands[0];
  return new_register(get_next_register(n->result.table));
}

static void ir_operand_update(struct ir_instruction *instruction, int position, struct node *n) {
  if (node_get_result(n)->ir_operand) {
    ir_operand_copy(instruction, position, node_get_result(n)->ir_operand);
  } else if (n->ir) {
    ir_operand_copy(instruction, position, &n->ir->last->operands[0]);
  }
}

static int ir_operand_register_coerce(struct instruction *inst, int pos, struct node *n) {
  if (ir_operand_get(n)->kind != OPERAND_TEMPORARY) {
    
  }
}

static int ir_generate_for_arithmetic_binary_operation(enum ir_instruction_kind kind, struct node *binary_operation) {
  int errcount = 0;
  int pos = 0;
  int treg = 0;
  struct ir_instruction *instruction;
  struct node *left = binary_operation->data.binary_operation.left_operand;
  struct node *right = binary_operation->data.binary_operation.right_operand;
  assert(NODE_BINARY_OPERATION == binary_operation->kind);
  
  if (left->result.table == NULL) left->result.table = binary_operation->result.table; 
  if (right->result.table == NULL) right->result.table = binary_operation->result.table; 
  errcount += ir_generate_for_expression(left);
  errcount += ir_generate_for_expression(right);

  binary_operation->ir = ir_section(NULL, NULL); 
  instruction = ir_instruction(kind);
  ir_operand_register(instruction, 0, get_next_register(binary_operation));
  ir_operand_update(instruction, 1, left);
  ir_operand_update(instruction, 2, right);
  debugf("ir_generate_for_arithmetic.nodes.left=%n, right=%n\n", binary_operation->data.binary_operation.left_operand, binary_operation->data.binary_operation.right_operand);
  for (pos = 1; pos<=2; pos++) {
    if (instruction->operands[pos].kind != OPERAND_TEMPORARY) {
      treg = get_next_register(binary_operation);
      binary_operation->ir = push(new_register(treg));
      ir_append(binary_operation->ir, ir_instruction_tuple(IR_LOAD_IMMEDIATE, treg, OPERAND_TEMPORARY, instruction->operands[pos].data.number, OPERAND_NUMBER, NULL));
      ir_operand_register(instruction, pos, treg);
    }
  }
  ir_append(binary_operation->ir, instruction);
  if (treg > 0)
      binary_operation->ir = ir_concatenate(binary_operation->ir, pop(new_register(treg)));

  binary_operation->ir = ir_concatenate(left->ir, binary_operation->ir);
  binary_operation->ir = ir_concatenate(right->ir, binary_operation->ir);
  
  binary_operation->data.binary_operation.result.ir_operand = &instruction->operands[0];
  debugf("ir_generate_for_arithmetic returning, n=%n, op=%o\n", binary_operation, binary_operation->data.binary_operation.result.ir_operand);
  return errcount;
}

static struct ir_instruction *inverse(struct ir_instruction *cmd) {
  struct ir_instruction *inv = NULL;
  switch (cmd->kind) {
    case IR_LOAD_WORD: inv = ir_instruction(IR_STORE_WORD); break;
    case IR_LOAD_BYTE: inv = ir_instruction(IR_STORE_BYTE); break;
    case IR_STORE_WORD: inv = ir_instruction(IR_LOAD_WORD); break;
    case IR_STORE_BYTE: inv = ir_instruction(IR_LOAD_BYTE); break;
    default: return NULL;
  }
  ir_operand_register(inv, 0, cmd->operands[0].data.temporary);
  ir_operand_register(inv, 1, cmd->operands[1].data.temporary);
  return inv;
}

static int ir_generate_for_simple_assignment(struct node *binary_operation) {
  int errcount = 0;
  struct ir_instruction *cp;
  struct ir_instruction *cmd = NULL;
  struct node *left;
  assert(NODE_BINARY_OPERATION == binary_operation->kind);

  debugf("ir_generate_for_simple_assignment.op=%n\n", binary_operation);
  left = binary_operation->data.binary_operation.left_operand;
  errcount += ir_generate_for_expression(binary_operation->data.binary_operation.right_operand);

  errcount += ir_generate_for_expression(left);

  debugf("ir_generate_for_simple_assignment.left=%n, right=%n\n", left, binary_operation->data.binary_operation.right_operand);
  assert(NODE_IDENTIFIER == left->kind || NODE_DEREF == left->kind);
  
  cp = ir_instruction(IR_ADD_IMMEDIATE);
  ir_operand_register(cp, 0, ir_operand_get(left)->data.temporary);
  ir_operand_register(cp, 1, ir_operand_get(binary_operation->data.binary_operation.right_operand)->data.temporary);
  ir_operand_signed_number(cp, 2, 0);

  if (left->kind == NODE_DEREF) {
    cmd = inverse(left->ir->last);
    left->ir->last->prev->next = NULL;
    left->ir->last = left->ir->last->prev;
  }
  binary_operation->ir = ir_copy(left->ir);
  binary_operation->ir = ir_concatenate(binary_operation->ir, binary_operation->data.binary_operation.right_operand->ir);
  ir_append(binary_operation->ir, cp);
  if (cmd) ir_append(binary_operation->ir, cmd);
  binary_operation->data.binary_operation.result.ir_operand = &binary_operation->ir->last->operands[0];

  debugf("ir_generate_for_simple_assignment.result=%o\n", binary_operation->data.binary_operation.result.ir_operand);
  return errcount;
}

static int ir_generate_for_binary_operation(struct node *binary_operation) {
  int errcount = 0;
  debugf("ir_generate_for_binary_operation.binary_operation=%n, operation=%s\n", binary_operation, binstr[binary_operation->data.binary_operation.operation]);
  assert(NODE_BINARY_OPERATION == binary_operation->kind);

  switch (binary_operation->data.binary_operation.operation) {
    case BINOP_MULTIPLICATION:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_MULTIPLY, binary_operation);
      break;

    case BINOP_DIVISION:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_DIVIDE, binary_operation);
      break;

    case BINOP_MOD:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_MOD, binary_operation);
      break;

    case BINOP_ADDITION:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_ADD, binary_operation);
      break;

    case BINOP_SUBTRACTION:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_SUBTRACT, binary_operation);
      break;

    case BINOP_ASSIGN:
      errcount += ir_generate_for_simple_assignment(binary_operation);
      break;

    case BINOP_EQUALITY:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_SUBTRACT, binary_operation);
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_SLT, binary_operation);
      break;
    case BINOP_LOGICAL_OR:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_OR, binary_operation);
      break;
    case BINOP_LOGICAL_AND:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_AND, binary_operation);
      break;
    case BINOP_XOR:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_XOR, binary_operation);
      break;
    case BINOP_RELATIONAL:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_SLT, binary_operation);
      break;
    case BINOP_SHIFT:
      errcount+=ir_generate_for_arithmetic_binary_operation(IR_SLL, binary_operation);
      break;
    default:
      assert(0);
      break;
  }
  return errcount;
}

void update_ir(struct node *expr, struct node *child) {
  if (child->ir) {
    if (!expr->ir)
      expr->ir = ir_copy(child->ir);
    else
      ir_concatenate(expr->ir, ir_copy(child->ir));
  }
}


struct ir_section *push(struct ir_operand *reg) {
  struct ir_instruction *addi = ir_instruction(IR_ADD_IMMEDIATE);
  struct ir_instruction *sw = ir_instruction(IR_STORE_WORD);
  struct ir_section *cmds = ir_section(NULL, NULL);
  ir_operand_register(addi, 0, STACK_PTR_OFFSET);
  ir_operand_register(addi, 1, STACK_PTR_OFFSET);
  ir_operand_signed_number(addi, 2, -4); 
  sw->operands[0].kind = reg->kind;
  sw->operands[0].data = reg->data;
  ir_operand_register(sw, 1, STACK_PTR_OFFSET);
  ir_append(cmds, addi);
  ir_append(cmds, sw);
  return cmds;
}

struct ir_section *pop(struct ir_operand *reg) {
  struct ir_instruction *lw = ir_instruction(IR_LOAD_WORD);
  struct ir_instruction *addi = ir_instruction(IR_ADD_IMMEDIATE);
  struct ir_section *cmds = ir_section(NULL, NULL);
  lw->operands[0].kind = reg->kind;
  lw->operands[0].data = reg->data;
  ir_operand_register(lw, 1, STACK_PTR_OFFSET);
  ir_operand_register(addi, 0, STACK_PTR_OFFSET);
  ir_operand_register(addi, 1, STACK_PTR_OFFSET);
  ir_operand_signed_number(addi, 2, 4);
  ir_append(cmds, lw);
  ir_append(cmds, addi);
  return cmds;
}

struct ir_section *push_all() {
  struct ir_section *cmds = push(new_register(0));
  int r;
  for (r = 1; r < 2; r++)
    cmds = ir_concatenate(cmds, push(new_register(r)));
  return cmds;
}

struct ir_section *pop_all() {
  struct ir_section *cmds = pop(new_register(0));
  int r;
  for (r = 1; r < 2; r++) 
    cmds = ir_concatenate(cmds, pop(new_register(r)));
  return cmds;
}

enum ir_instruction_kind to_load_kind(struct ir_operand op) {
  switch (op.kind) {
    case OPERAND_NUMBER:
	return IR_LOAD_IMMEDIATE;
    case OPERAND_TEMPORARY:
  	return IR_ADD_IMMEDIATE;
    case OPERAND_IDENTIFIER:
	return IR_LOAD_WORD;
    default:
   	return IR_NO_OPERATION;
  }
}

struct ir_instruction *load_value(struct node *n, struct ir_operand *to) {
  struct ir_operand *op = ir_operand_get(n);
  struct ir_instruction *instruction;
  if (op) {
    instruction = ir_instruction(to_load_kind(*op));
    ir_operand_register(instruction, 0, to->data.temporary);
    ir_operand_copy(instruction, 1, op);
    if (to_load_kind(*op) == IR_ADD_IMMEDIATE)
      ir_operand_signed_number(instruction, 2, 0);
    return instruction;
  }
  return NULL;
}

static int ir_generate_for_return(struct node *n) {
  int errcount = 0;
  struct node *expr = n;
  n->ir = ir_append(n->ir, load_value(n, new_register(V0_REG_OFFSET)));
  ir_append(n->ir, ir_instruction(IR_JR));
  ir_operand_register(n->ir->last, 0, RA_REG_OFFSET);
  return errcount;
}

struct node *get_dfs_expr(struct node *expr, int i) {
  while (expr->data.expr_list.curr && i-- > 0) {
        expr = expr->data.expr_list.next;
  }
  return expr;
}

bool is_expr(struct node *n) {
  return n && n->kind == NODE_EXPR_LIST;
}

static int sys_call_code(char *fname) {
  if (strcmp(fname, "print_int") == 0) return 1;
  else if (strcmp(fname, "print_float") == 0) return 2;
  else if (strcmp(fname, "print_double") == 0) return 3;
  else if (strcmp(fname, "print_string") == 0) return 4;
  else if (strcmp(fname, "read_integer") == 0) return 5;
  else if (strcmp(fname, "read_float") == 0) return 6;
  else if (strcmp(fname, "read_double") == 0) return 7;
  else if (strcmp(fname, "read_string") == 0) return 8;
  else if (strcmp(fname, "malloc") == 0) return 9;
  else if (strcmp(fname, "exit") == 0) return 10;
  else if (strcmp(fname, "print_character") == 0) return 11;
  else if (strcmp(fname, "read_character") == 0) return 12;
  return 0;
}

static int ir_generate_for_call(struct node *n) {
    int errcount = 0;
    int param_count = 0;
    struct node *expr = n;
    struct ir_instruction *instruction = ir_instruction(IR_NO_OPERATION);
    struct ir_instruction *fn_call = ir_instruction(IR_JL);
    struct ir_operand* ra = new_register(RA_REG_OFFSET);
    struct ir_operand* v0 = new_register(V0_REG_OFFSET);
    char *fname;
    debugf("ir_generate_for_call.n=%n\n", n);
    if (!expr->ir)
        expr->ir = ir_section(instruction, instruction);
    expr->ir = ir_concatenate(expr->ir, push_all());
    expr->ir = ir_concatenate(expr->ir, push(ra));
    n = get_expr(expr, 2)->data.expr_list.curr; // expression_list
    if (is_expr(n) && is_match(get_expr(n, 1)->data.expr_list.curr, ",")) { // expression_list COMMA assignment_expr
        while (n) {
            // get result of assignment_expr and get args register
            expr->ir = ir_concatenate(expr->ir, n->data.expr_list.curr->ir);
    	    if (param_count < 4) {
	      ir_append(expr->ir, ir_instruction(IR_ADD_IMMEDIATE));
	      ir_operand_register(expr->ir->last, 0, ARG0_REG_OFFSET + param_count++);
	      expr->ir->last->operands[1] = *ir_operand_get(n->data.expr_list.curr);
	      if (expr->ir->last->operands[1].kind == OPERAND_NUMBER) {
	      	expr->ir->last->operands[2] = expr->ir->last->operands[1];
		expr->ir->last->operands[2].is_signed = 1;
		ir_operand_register(expr->ir->last, 1, ZERO_REG_OFFSET);
       	      } else {
	        ir_operand_signed_number(expr->ir->last, 2, 0);
	      }
	    } else {
              expr->ir = ir_concatenate(expr->ir, push(ir_operand_get(n->data.expr_list.curr)));
	    }
            n = get_expr(n, 2);
        }
    } else if (n && !is_expr(n) || !is_match(get_expr(n, 1)->data.expr_list.curr, ")")) {
	n = is_expr(n)? get_expr(n, 1)->data.expr_list.curr: n; 
        expr->ir = ir_concatenate(expr->ir, n->ir);
	expr->ir = ir_append(expr->ir, ir_instruction(IR_ADD_IMMEDIATE));
        ir_operand_register(expr->ir->last, 0, ARG0_REG_OFFSET);
    	expr->ir->last->operands[1] = *ir_operand_get(n);
        if (expr->ir->last->operands[1].kind == OPERAND_NUMBER) {
          expr->ir->last->operands[2] = expr->ir->last->operands[1];
          ir_operand_register(expr->ir->last, 1, ZERO_REG_OFFSET);
        } else {
          ir_operand_signed_number(expr->ir->last, 2, 0);
        }
	expr->ir->last->operands[2].is_signed = 1;
    }
    fname = get_name(expr);
    if (sys_call_code(fname) > 0) {
        fn_call = ir_instruction(IR_CALL);
	ir_append(expr->ir, ir_instruction(IR_LOAD_IMMEDIATE));
	ir_operand_register(expr->ir->last, 0, V0_REG_OFFSET);
	ir_operand_signed_number(expr->ir->last, 1, sys_call_code(fname));
    }
    ir_operand_identifier(fn_call, 0, get_name(expr));
    ir_append(expr->ir, fn_call);
    expr->ir = ir_concatenate(expr->ir, pop(ra));
    expr->ir = ir_concatenate(expr->ir, pop_all());
    ir_append(expr->ir, ir_instruction(IR_ADD_IMMEDIATE));
    ir_operand_register(expr->ir->last, 0, ir_operand_get(expr)->data.temporary);
    ir_operand_register(expr->ir->last, 1, V0_REG_OFFSET);
    ir_operand_signed_number(expr->ir->last, 2, 0);
    debugf("ir_generate_for_call returning\n", n);
    return errcount;
}

char* concat_str(char* s1, int i) {
  char *str = malloc((IDENTIFIER_MAX + 1) * sizeof(char));
  sprintf(str, "%s%d", s1, i);
  return str;
}


static int ir_generate_for_if(struct node *n, int has_else) {
  int errcount = 0;
  struct node *expr = n;
  errcount += ir_generate_for_expression(get_expr(n, 2));
  info("ir_generate_for_if.cond=%n\n", get_expr(n, 2));
  errcount += ir_generate_for_expression(get_expr(n, 4));
  info("ir_generate_for_if.body=%n\n", get_expr(n, 4));
  n->ir = ir_concatenate(n->ir, get_expr(n, 2)->ir);
  ir_append(n->ir, ir_instruction(IR_BEQ));
  ir_operand_register(n->ir->last, 0, ir_operand_get(get_expr(n, 2))->data.temporary);
  ir_operand_register(n->ir->last, 1, ZERO_REG_OFFSET);
  ir_operand_identifier(n->ir->last, 2, concat_str((has_else? "Else": "Endif"), ++if_count));
  n->ir = ir_concatenate(n->ir, get_expr(n, 4)->ir);
  ir_append(n->ir, ir_instruction(IR_J));
  ir_operand_identifier(n->ir->last, 0, concat_str("Endif", if_count));
  if (has_else) {
    errcount += ir_generate_for_expression(get_expr(n, 6));
    ir_append(n->ir, ir_instruction(IR_LABEL));
    ir_operand_identifier(n->ir->last, 0, concat_str("Else", if_count));
    n->ir = ir_concatenate(n->ir, get_expr(n, 6)->ir);
  }
  ir_append(n->ir, ir_instruction(IR_LABEL));
  ir_operand_identifier(n->ir->last, 0, concat_str("Endif", if_count));
  return errcount;
}


static int ir_generate_for_do(struct node *n) {
  int errcount = 0;
  struct node *stmt = get_expr(n, 1)->data.expr_list.curr;
  struct node *cond = get_expr(n, 4)->data.expr_list.curr;
  errcount += ir_generate_for_expression(cond);
  errcount += ir_generate_for_expression(stmt);
  n->ir = ir_append(n->ir, ir_instruction(IR_LABEL));
  ir_operand_identifier(n->ir->last, 0, concat_str("do", ++for_count));
  n->ir = ir_concatenate(n->ir, stmt->ir);
  n->ir = ir_concatenate(n->ir, cond->ir);
  ir_append(n->ir, ir_instruction(IR_BEQ));
  ir_operand_register(n->ir->last, 0, ir_operand_get(cond)->data.temporary);
  ir_operand_register(n->ir->last, 1, ZERO_REG_OFFSET);
  ir_operand_identifier(n->ir->last, 2, concat_str("loop", for_count));
  ir_append(n->ir, ir_instruction(IR_J));
  ir_operand_identifier(n->ir->last, 0, concat_str("do", for_count));
  return errcount;
}

static int ir_generate_for_while(struct node *n) {
  int errcount = 0;
  struct node *cond = get_expr(n, 2)->data.expr_list.curr;
  struct node *stmt = get_expr(n, 4)->data.expr_list.curr;
  errcount += ir_generate_for_expression(cond);
  errcount += ir_generate_for_expression(stmt);
  n->ir = ir_concatenate(n->ir, cond->ir);
  ir_append(n->ir, ir_instruction(IR_LABEL));
  ir_operand_identifier(n->ir->last, 0, concat_str("while", ++for_count));
  ir_append(n->ir, ir_instruction(IR_BNE));
  ir_operand_register(n->ir->last, 0, ir_operand_get(cond)->data.temporary);
  ir_operand_register(n->ir->last, 1, ZERO_REG_OFFSET);
  ir_operand_identifier(n->ir->last, 2, concat_str("done", for_count));
  n->ir = ir_concatenate(n->ir, stmt->ir);
  ir_append(n->ir, ir_instruction(IR_J));
  ir_operand_identifier(n->ir->last, 0, concat_str("while", for_count));
  ir_append(n->ir, ir_instruction(IR_LABEL));
  ir_operand_identifier(n->ir->last, 0, concat_str("done", for_count));
  return errcount;
}


static int ir_generate_for_forloop(struct node *n) {
  int errcount = 0;
  struct node *control = get_expr(n, 1)->data.expr_list.curr;
  struct node *body = get_expr(n, 2)->data.expr_list.curr;
  struct node *init = get_expr(control, 1)->data.expr_list.curr; 
  struct node *cond = get_expr(control, 3)->data.expr_list.curr;
  struct node *incr = get_expr(control, 5)->data.expr_list.curr;
  if (init != NULL && !is_match(init, ";") &&
      cond != NULL && !is_match(cond, ";") &&
      incr != NULL && !is_match(incr, ")")) {
      errcount += ir_generate_for_expression(init);
      errcount += ir_generate_for_expression(cond);
      errcount += ir_generate_for_expression(incr);
      info("ir_generate_for_forloop.init=%n, cond=%n, incr=%n, control=%n, body=%n\n", init, cond, incr, control, body);
      errcount += ir_generate_for_expression(body);
      info("ir_generate_for_forloop.init.ir=%r, cond.ir=%r, incr=%r, body=%r\n", init->ir, cond->ir, incr->ir, body->ir);
     n->ir = ir_copy(init->ir);
     ir_append(n->ir, ir_instruction(IR_LABEL));
     ir_operand_identifier(n->ir->last, 0, concat_str("top", ++for_count));
     n->ir = ir_concatenate(n->ir, cond->ir);
     ir_append(n->ir, ir_instruction(IR_BEQ));
     ir_operand_register(n->ir->last, 0, ir_operand_get(cond)->data.temporary);
     ir_operand_register(n->ir->last, 1, ZERO_REG_OFFSET);
     ir_operand_identifier(n->ir->last, 2, concat_str("done", for_count));
     n->ir = ir_concatenate(n->ir, body->ir);
     n->ir = ir_concatenate(n->ir, incr->ir);
     ir_append(n->ir, ir_instruction(IR_J));
     ir_operand_identifier(n->ir->last, 0, concat_str("top", for_count));
     ir_append(n->ir, ir_instruction(IR_LABEL));
     ir_operand_identifier(n->ir->last, 0, concat_str("done", for_count));
  } else {
     info("ir_generate_for_forloop.malformed.init=%n, cond=%n, incr=%n, control=%n\n", init, cond, incr, control);
     compiler_print_error(n->location, "for loop is malformed.");
     errcount++;
	exit(1);
  }
  return errcount;
}

static int ir_generate_for_incr(struct  node *n) {
  int errcount = 0;
  struct node *expr = n;
  struct node *var = NULL;
  struct node *one = node_number(n->location, "1");
  struct node *incr = NULL;
  struct node *assign = NULL;
  enum node_binary_operation type;
  int is_postfix = 0;
  debugf("ir_generate_for_incr n=%n, len=%d\n", n, n->data.expr_list.length);
  if (is_match(get_expr(expr, 0)->data.expr_list.curr, "++") || 
      is_match(get_expr(expr, 0)->data.expr_list.curr, "--")) {
      var = get_expr(expr, 1)->data.expr_list.curr;
      if (is_match(get_expr(expr, 0)->data.expr_list.curr, "++")) type = BINOP_ADDITION;
      else type = BINOP_SUBTRACTION;
  } else  { //if (is_match(get_expr(expr, 2), "++") || is_match(get_expr(expr, 2), "--")) {
      var = get_expr(expr, 0)->data.expr_list.curr;
      if (is_match(get_expr(expr, 1)->data.expr_list.curr, "++")) type = BINOP_ADDITION;
      else type = BINOP_SUBTRACTION;
      is_postfix = 1;
  }
  incr = node_binary_operation(expr->location, type, var, one);
  assign = node_binary_operation(expr->location, BINOP_ASSIGN, var, incr);
  errcount += ir_generate_for_binary_operation(assign);
  expr->ir = ir_concatenate(expr->ir, assign->ir);
  if (is_postfix) {
   	incr = node_binary_operation(expr->location, 
	  type==BINOP_ADDITION? BINOP_SUBTRACTION: BINOP_ADDITION, var, one);
	errcount += ir_generate_for_binary_operation(incr);
  	expr->ir = ir_concatenate(expr->ir, incr->ir);
	expr->ir->last->operands[0].data.temporary = get_next_register(var);
  }
  expr->result.ir_operand = &expr->ir->last->operands[0];
  debugf("ir_generate_for_incr returning op=%o\n", expr->result.ir_operand);
  return errcount;
}

static bool is_terminal_or_op(struct node *n) {
  return n && n->kind == NODE_IDENTIFIER &&
  	(n->data.identifier.is_terminal || n->data.identifier.is_op);
}

static int ir_generate_for_expr_list(struct node *n) {
  int errcount = 0;
  struct node *expr = n;
  expr->ir = NULL;
  while (n) {
    if (!n->data.expr_list.is_func_decl) {
      debugf("ir_generate_for_expr_list.curr = %n\n", n->data.expr_list.curr);
      errcount += ir_generate_for_expression(n->data.expr_list.curr);
      update_ir(expr, n->data.expr_list.curr);
      if (!is_terminal_or_op(n->data.expr_list.curr))
	expr->result.ir_operand = ir_operand_get(n->data.expr_list.curr);
    }
    n = n->data.expr_list.next;
  }
  if (expr->data.expr_list.is_call) {
    errcount += ir_generate_for_call(expr);
  } else if (expr->data.expr_list.is_return) {
    errcount += ir_generate_for_return(expr);
  } else if (expr->data.expr_list.is_if) {
    errcount += ir_generate_for_if(expr, 0);
  } else if (expr->data.expr_list.is_if_else) {
    errcount += ir_generate_for_if(expr, 1);
  } else if (expr->data.expr_list.is_do) {
    errcount += ir_generate_for_do(expr);
  } else if (expr->data.expr_list.is_while) {
    errcount += ir_generate_for_while(expr);
  } else if (expr->data.expr_list.is_for_loop) {
    errcount += ir_generate_for_forloop(expr);
  } else if (expr->data.expr_list.is_incr) {
    errcount += ir_generate_for_incr(expr);
  }
  return errcount;
}

static int level = 0;
static int has_main = 0;

static int ir_generate_for_function(struct node *n) {
  int errcount = 0;
  char* function_name = get_name(n->data.unary_expr.p1);
  char* end_name = malloc(IDENTIFIER_MAX * sizeof(char));
  struct ir_instruction *begin_instr = ir_instruction(IR_PROC_BEGIN); 
  struct ir_instruction *end_instr = ir_instruction(IR_PROC_END); 
  struct node *params = NULL;
  ir_operand_identifier(begin_instr, 0, function_name);
  ir_operand_identifier(begin_instr, 1, NULL);
  ir_operand_identifier(begin_instr, 2, NULL);
  if (strcmp(function_name, "main") == 0)
    has_main = 1;
  strcpy(end_name, function_name);
  strcat(end_name, "_end");
  ir_operand_identifier(end_instr, 0, end_name);
  ir_operand_identifier(end_instr, 1, NULL);
  ir_operand_identifier(end_instr, 2, NULL);
  
  n->ir = ir_section(begin_instr, begin_instr);
  params = get_expr(n->data.unary_expr.p1, 1);
  params = (params && params->data.expr_list.curr? get_expr(params->data.expr_list.curr, 1): params);
  params = (params && params->data.expr_list.curr? get_expr(params->data.expr_list.curr, 1): params);
  params = (params && params->data.expr_list.curr? params->data.expr_list.curr: params);
  if (params && !is_match(params, ")")) {
    errcount += ir_generate_for_expression(params);
    if (!is_empty(params->ir))
	n->ir = ir_concatenate(n->ir, ir_copy(params->ir));
  }

  errcount += ir_generate_for_expression(n->data.unary_expr.p2);
  n->ir = ir_concatenate(n->ir, ir_copy(n->data.unary_expr.p2->ir));
  // If a return statement was encoutered, the following command is never reached
  ir_append(n->ir, ir_instruction(IR_JR)); 
  ir_operand_register(n->ir->last, 0, RA_REG_OFFSET);
  n->ir = ir_append(n->ir, end_instr);
  debugf("ir_generate_for_function done, ir=%r\n", n->ir);
  return errcount;
}

static int get_deref_level(struct node *n) {
  int level = 0;
  while (n) {
    if (is_match(n->data.expr_list.curr, "*") || is_match(n->data.expr_list.curr, "["))
	level++;
    n = n->data.expr_list.next;
  }
  return level;
}

static int get_height(struct type* type) {
  int height = 0;
  while (type) {
    height++;
    type = type->data.basic.parent_type;
  }
  return height;
}

static int get_to_reg(struct ir_operand* oldto, int from) {
  if (oldto == NULL)
    return get_next_register(NULL);
  if (oldto->data.temporary == from)
    oldto->data.temporary = get_next_register(NULL);
  return oldto->data.temporary;
}

int add_subscript(struct node *n, struct node *subscript, int reg, int is_byte_size) {
  int sub_reg = get_next_register(NULL);
  if (subscript->kind != NODE_NUMBER) { //calculate offset
    n->ir = ir_append(n->ir, ir_instruction(IR_LOAD_IMMEDIATE));
    ir_operand_register(n->ir->last, 0, sub_reg);
    ir_operand_signed_number(n->ir->last, 1, is_byte_size? 1: 4);
    n->ir = ir_append(n->ir, ir_instruction(IR_MULTIPLY));
    ir_operand_register(n->ir->last, 0, sub_reg);
    n->ir->last->operands[1] = *ir_operand_get(subscript);
    ir_operand_register(n->ir->last, 2, sub_reg);
  }
  n->ir = ir_append(n->ir, ir_instruction(subscript->kind==NODE_NUMBER? IR_ADD_IMMEDIATE: IR_ADD));
  ir_operand_register(n->ir->last, 0, sub_reg);
  ir_operand_register(n->ir->last, 1, reg);
  if (subscript->kind==NODE_NUMBER) {
    subscript->data.number.value = (is_byte_size? 1: 4) * subscript->data.number.value;
    ir_operand_number(n->ir->last, 2, subscript);
  } else {
    if (sub_reg > -9)
      ir_operand_register(n->ir->last, 2, sub_reg);
    else
      n->ir->last->operands[2] = *ir_operand_get(subscript);
  }
  return sub_reg;
}

static int ir_generate_for_deref(struct node *n) {
  int errcount = 0;
  struct node *ptr = get_expr(n, 1);
  struct node *subscript_node = NULL;
  int is_subscript = is_match(ptr->data.expr_list.curr, "[");
  int subscript = 0;
  int height = 0; // pointer height, i.e height(***p) = 3, height(p[][][]) = 3
  int level = get_deref_level(n);
  int init_level = level;
  int reg = 0, offset_reg = -9;
  int is_byte_size = 0;
  struct node *id = get_id(n);
  int id_reg = ir_operand_get(id)? ir_operand_get(id)->data.temporary: get_next_register(id);
  struct type* ptype = NULL;
  debugf("ir_generate_for_deref.n=%n, ptr=%n, id=%n, sym=%p, id_reg=%d\n", n, ptr, id, id->data.identifier.symbol, id_reg);
  errcount += ir_generate_for_expression(ptr);
  n->ir = ir_copy(ptr->ir);
  if (is_subscript) {
    ptr = get_expr(n, 0)->data.expr_list.curr;
    subscript_node = get_expr(get_expr(n, 1), 1)->data.expr_list.curr;
    errcount += ir_generate_for_expression(subscript_node);
    n->ir = ir_concatenate(subscript_node->ir, n->ir);
  }
  ptype = node_get_result(ptr)->type;
  height = get_height(ptype);
  is_byte_size = height <= 2 && ptype->data.basic.datatype == TYPE_BASIC_CHAR; 
  if (is_subscript && ptype->data.basic.datatype == TYPE_BASIC_CHAR && height > 2) {
    //level = height - level;
    while (level-- > 0) {
   	reg = level==init_level-1? id_reg: ir_operand_get(ptr)->data.temporary;
	offset_reg = add_subscript(n, subscript_node, reg, is_byte_size);
	n->ir = ir_append(n->ir, ir_instruction(IR_LOAD_WORD));
        ir_operand_register(n->ir->last, 0, get_to_reg(ir_operand_get(ptr), reg));
        ir_operand_register(n->ir->last, 1, offset_reg);
        //n->ir->last->offset = 4 * subscript;
    }
  } else {
    if (is_subscript)
    	offset_reg = add_subscript(n, subscript_node, id_reg, is_byte_size);
    ir_append(n->ir, ir_instruction(is_byte_size? IR_LOAD_BYTE: IR_LOAD_WORD));
    ir_operand_register(n->ir->last, 0, get_to_reg(ir_operand_get(ptr), id_reg));
    ir_operand_register(n->ir->last, 1, is_subscript? offset_reg: id_reg);
    //n->ir->last->offset = (is_byte_size? 1: 4) * subscript;
  }
  
  n->result.ir_operand = &n->ir->last->operands[0];
  debugf("ir_generate_for_deref returning last[0]=%o, last[1]=%o\n", &n->ir->last->operands[0], &n->ir->last->operands[1]);
  return errcount;
}

struct ir_instruction* read_arg(struct node *decl, int pos) {
  struct ir_instruction *instruction = ir_instruction(IR_ADD_IMMEDIATE);
  instruction->operands[0] = *ir_operand_get(decl);
  ir_operand_register(instruction, 1, ARG0_REG_OFFSET + pos);
  ir_operand_signed_number(instruction,  2, 0);
  return instruction;
}

static int ir_generate_for_decl_impl(struct node *n, int is_param);

static int ir_generate_for_decl_list(struct node *expr) {
  int errcount = 0;
  int param_count = 0;
  int total_params = 0;
  struct node *n = expr;
  struct node *decl = NULL;
  struct ir_instruction *instruction = NULL;
  expr->ir = ir_section(NULL, NULL);
  while (n) { 
    total_params++; 
    n = n->kind==NODE_DECL_LIST? n->data.decl_list.dlist: NULL; 
  }
  n = expr;
  total_params = total_params>4? 4: total_params;
  while (n) {
     decl = n->kind==NODE_DECL_LIST? n->data.decl_list.decl: n;
     errcount += ir_generate_for_decl_impl(decl, 1);
     if (param_count < 4) {
 	ir_append(expr->ir, read_arg(decl, total_params - ++param_count));
     } else {
       expr->ir = ir_concatenate(expr->ir, pop(ir_operand_get(decl)));
     }
     n = n->kind==NODE_DECL_LIST? n->data.decl_list.dlist: NULL;
  }
   debugf("ir_generate_for_decl_list.ir=%r\n", expr->ir);
  return errcount;
}


static int ir_generate_for_decl(struct node *n) {
  return ir_generate_for_decl_impl(n, 0);
}

static int ir_generate_for_decl_impl(struct node *n, int is_param) {
  int errcount = 0;
  struct ir_instruction *instruction = NULL;
  char **dspecs = get_names(n->data.decl.dspec);
  char **dlist = get_names(n->data.decl.dlist);
  struct node *params = get_expr(n->data.decl.dlist, 1);
  params = params && expr_type(params->kind)? params->data.expr_list.curr: NULL;
  int last = size(dspecs) - 1;
  int i = 0;
  if (params && params->kind == NODE_PARAM_LIST) {
    //errcount += ir_generate_for_expression(get_expr(params, 1)); 
    return errcount;
  }
  n->ir = ir_section(NULL, NULL);
  if (n->file_scope && !is_param) {
    while (dlist != NULL && dlist[i] != NULL) {
     instruction = ir_instruction(IR_DATA);
     ir_operand_identifier(instruction, 0, dlist[i++]);
     ir_operand_identifier(instruction, 1, NULL);
     ir_operand_identifier(instruction, 2, NULL);
     if (last>=0)
	ir_operand_identifier(instruction, 1, dspecs[last]);
     if (n->ir == NULL)
	n->ir = ir_section(instruction, instruction);
     else
	ir_append(n->ir, instruction);
    }
  }
  if (!n->file_scope) {
    instruction = read_arg(n, 0);
    n->ir = ir_section(instruction, instruction);
  }
  return errcount;
}

static int ir_generate_for_expression(struct node *expression) {
  int errcount = 0;
  struct ir_instruction *instruction;
  if (!expression || expression->visited)
	return errcount;
  expression->visited = 1;
  debugf("ir_generate_for_expression.expression=%n\n", expression);
  switch (expression->kind) {
    case NODE_IDENTIFIER:
      if (!expression->data.identifier.is_op && !expression->data.identifier.is_terminal)
          errcount += ir_generate_for_identifier(expression);
      break;
    case NODE_CHAR:
    case NODE_SHORT:
    case NODE_INT:
    case NODE_LONG:
    case NODE_EQUAL:
      break;
    case NODE_NUMBER:
      errcount += ir_generate_for_number(expression);
      break;

    case NODE_BINARY_OPERATION:
      errcount += ir_generate_for_binary_operation(expression);
      break;
    case NODE_TERNARY_EXPR:
      if (expression->data.ternary_expr.bin_op) {
	 errcount += ir_generate_for_expression(expression->data.ternary_expr.bin_op);
	 expression->ir = ir_copy(expression->data.ternary_expr.bin_op->ir);
	 expression->result = *node_get_result(expression->data.ternary_expr.bin_op);
      } else if (expression->data.ternary_expr.is_conditional) { // TODO
	//errcount += ir_generate_for_expression(expression->data.ternary_expr.is_conditional);
      }
      break;
    case NODE_DSPECIFIER:
    case NODE_PARAM_LIST:
    case NODE_ARRAY_DECL:
      break;
    case NODE_EXPR_LIST:
      errcount += ir_generate_for_expr_list(expression);
      break;
    case NODE_DECL:
      errcount += ir_generate_for_decl(expression);
      break;
    case NODE_DECL_LIST:
      errcount += ir_generate_for_decl_list(expression);
      break;
    case NODE_DEREF:
      errcount += ir_generate_for_deref(expression);
      break;
    case NODE_FUNCTION:
      errcount += ir_generate_for_function(expression);
      break;
    case NODE_STATEMENT_LIST:
      errcount += ir_generate_for_statement_list(expression);
      break;
    default:
      debug("default expression", expression, 0);
      assert(0);
      break;
  }
  debugf("ir_generate_for_expression.expression returning %n ir=%r\n", expression, expression->ir);
  return errcount;
}

static int ir_generate_for_expression_statement(struct node *expression_statement) {
  int errcount = 0;
  struct ir_instruction *instruction;
  struct node *expression = expression_statement; 
  struct ir_section * ir = NULL;
  struct result * result = NULL;
  debug("ir_generate_for_expression_statement.expression", expression, 0);
  errcount += ir_generate_for_expression(expression);

  instruction = ir_instruction(IR_NO_OPERATION);
  expression_statement->ir = expression->ir? ir_copy(expression->ir): ir_section(NULL, NULL);
  ir_append(expression_statement->ir, instruction);
  return errcount;
}

int ir_generate_for_statement_list(struct node *statement_list) {
  struct node *init = statement_list->data.statement_list.init;
  struct node *statement = statement_list->data.statement_list.statement;
  struct ir_section *j_main = NULL;
  int errcount = 0;
  int curr_level = level++;

  assert(NODE_STATEMENT_LIST == statement_list->kind);

  if (NULL != init) {
    errcount += ir_generate_for_statement_list(init);
    errcount += ir_generate_for_expression_statement(statement);
    statement_list->ir = ir_concatenate(init->ir, statement->ir);
  } else {
    errcount += ir_generate_for_expression_statement(statement);
    statement_list->ir = statement->ir;
  }
  if (curr_level == 0 && !has_main) {
    compiler_print_error(statement_list->location, "main function not found");
    errcount++;
  }
  if (curr_level == 0) {
    j_main = ir_section(NULL, NULL);
    ir_append(j_main, ir_instruction_single(IR_JL, 0,OPERAND_IDENTIFIER, "main"));
    ir_append(j_main, ir_instruction_tuple(IR_LOAD_IMMEDIATE, V0_REG_OFFSET, OPERAND_TEMPORARY, 10, OPERAND_NUMBER, NULL));
    ir_append(j_main, ir_instruction_single(IR_CALL, 0, OPERAND_IDENTIFIER, ""));
    statement_list->ir = ir_concatenate(j_main, statement_list->ir);
    ir_append(statement_list->ir, ir_instruction(IR_LABEL));
    ir_operand_identifier(statement_list->ir->last, 0, "end_of_program");
  }
  return errcount;
}

/***********************
 * PRINT IR STRUCTURES *
 ***********************/

static void ir_print_opcode(FILE *output, enum ir_instruction_kind kind) {
  static char const * const instruction_names[] = {
    "NOP",
    "MULT",
    "DIV",
    "MOD",
    "ADD",
    "ADDI",
    "SUB",
    "OR",
    "XOR",
    "AND",
    "SLT",
    "SLL",
    "SRL",
    "JR",
    "JAL",
    "J",
    "BEQ",
    "BNE",
    "DATA",
    "LA",
    "LI",
    "LB",
    "LW",
    "SW",
    "SB",
    "COPY",
    "ADDRESS",
    "LABEL",
    "DECL",
    "PNUM",
    "CALL",
    "PROC_BGN",
    "PROC_END",
    NULL
  };

  fprintf(output, "%-8s", instruction_names[kind]);
}

static void ir_print_operand(FILE *output, struct ir_operand *operand) {
  switch (operand->kind) {
    case OPERAND_NUMBER:
      if (operand->is_signed)
        fprintf(output, "%10ld", operand->data.signed_number);
      else
        fprintf(output, "%10hu", (unsigned short)operand->data.number);
      break;

    case OPERAND_TEMPORARY:
      fprintf(output, "     t%04d", operand->data.temporary);
      break;

    case OPERAND_IDENTIFIER:
      fprintf(output, "     %s", operand->data.name);
  }
}

static void ir_print_operand_braced(FILE *output, struct ir_operand *operand, int offset) {
  switch (operand->kind) {
    case OPERAND_NUMBER:
      if (operand->is_signed)
	fprintf(output, "%10d", operand->data.signed_number);
      else
        fprintf(output, "%10hu", (unsigned short)operand->data.number);
      break;

    case OPERAND_TEMPORARY:
      fprintf(output, "     %d(t%04d)", offset, operand->data.temporary);
      break;

    case OPERAND_IDENTIFIER:
      fprintf(output, "     %d(%s)", offset, operand->data.name);
  }
}
static void ir_print_instruction(FILE *output, struct ir_instruction *instruction) {
  ir_print_opcode(output, instruction->kind);

  switch (instruction->kind) {
    case IR_MULTIPLY:
    case IR_DIVIDE:
    case IR_MOD:
    case IR_ADD:
    case IR_ADD_IMMEDIATE:
    case IR_SUBTRACT:
    case IR_SLT:
    case IR_SLL:
    case IR_SRL:
    case IR_BEQ:
    case IR_BNE:
    case IR_DATA:
    case IR_OR:
    case IR_XOR:
    case IR_AND:
      ir_print_operand(output, &instruction->operands[0]);
      fprintf(output, ", ");
      ir_print_operand(output, &instruction->operands[1]);
      fprintf(output, ", ");
      ir_print_operand(output, &instruction->operands[2]);
      break;
    case IR_LOAD_ADDRESS:
    case IR_LOAD_IMMEDIATE:
    case IR_COPY:
    case IR_LABEL:
    case IR_DECL:
      ir_print_operand(output, &instruction->operands[0]);
      fprintf(output, ", ");
      ir_print_operand(output, &instruction->operands[1]);
      break;

    case IR_LOAD_BYTE:
    case IR_LOAD_WORD:
    case IR_STORE_WORD:
    case IR_STORE_BYTE:
      ir_print_operand(output, &instruction->operands[0]);
      fprintf(output, ", ");
      ir_print_operand_braced(output, &instruction->operands[1], instruction->offset);
      break;
    case IR_JR:
    case IR_JL:
    case IR_J:
    case IR_ADDRESS_OF:
    case IR_CALL:
    case IR_PROC_BEGIN:
    case IR_PROC_END:
    case IR_PRINT_NUMBER:
      ir_print_operand(output, &instruction->operands[0]);
      break;
    case IR_NO_OPERATION:
      break;
    default:
      assert(0);
      break;
  }
}

void ir_print_section(FILE *output, struct ir_section *section) {
  int i = 0;
  struct ir_instruction *iter = section->first;
  struct ir_instruction *prev = NULL;
  while (NULL != iter && section->last != prev) {
    fprintf(output, "%5d     ", i++);
    ir_print_instruction(output, iter);
    fprintf(output, "\n");

    iter = iter->next;
  }
}
