#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include <dirent.h>

#include "node.h"
#include "symbol.h"
#include "type.h"
#include "ir.h"
#include "debug.h"


int *indices = NULL;
int indices_size = -1;
struct node **rules = NULL;

int init(char *path) {
  DIR *dirp;
  struct dirent* dp;
  int i = 0, j = 0;
  dirp = opendir(path);
  while (dirp && (dp = readdir(dirp)) != NULL) 
    if (dp->d_name && !strcmp(dp->d_name, ".") && !strcmp(dp->d_name, "..")) 
        i++;
  if (i>0 && !indices) {
    indices = (int *) malloc(i * sizeof(int));
    for (j=0; j<i; j++)
        indices[j] = 0;
  }
  indices_size = i;
  debugf("init.path=%s, indices_size=%d\n", path, indices_size);
  return i;
}

struct node **parse_path(char *path) {
  DIR *dirp;
  struct dirent* dp;
  struct node *nodes[(indices_size>=0? indices_size: init(path)) + 1]; 
  int i = 0;
  dirp = opendir(path);
  while (dirp && (dp = readdir(dirp)) != NULL) {
    if (dp->d_name && !strcmp(dp->d_name, ".") && !strcmp(dp->d_name, "..")) {
	nodes[i++] = parse(dp->d_name);
        debugf("parse_path.file=%s\n", dp->d_name);
    }
  }
  nodes[i] = NULL;
  return nodes;
}

/*
auto apply(auto vals, int val_size, auto fn) {
  int i = 0;
  auto results[val_size];
  for (i=0; i<val_size; i++) {
    results[i] = fn(vals[i]);
  }
  return results;
}
*/

int refactor_expression(struct symbol_table *table, struct node *expr, struct node *search, struct node *replace, int *is_match) {
  int error_count = 0, i = 0;
  switch (expr->kind) {

  }
  return error_count;
}

int refactor_expression_statement(struct symbol_table *table, struct node *statement)
{
  int error_count = 0, i = 0, is_match = 0;
  debugf("refactor_expression_statement.statement=%n\n", statement);

  if (statement->kind == NODE_EXPRESSION_STATEMENT)
    return refactor_expression_statement(table, statement->data.expression_statement.expression);
  while (rules && rules[i]) {
    error_count += refactor_expression(table, statement, get_statement(rules[i], indices[i]), rules[i+1], &is_match);
    if (is_match) {
 	if (!get_statement(rules[i], ++indices[i])) { // Rule is fully matched
	  
	}
	i+=2;
    }
  }
  return error_count;
}

int refactor_statement_list(struct symbol_table *table, struct node *statement_list, char *rules_path)
{
  int error_count = 0, i = 0, pos = 0;
  if (!rules) rules = parse_path(rules_path);
  assert(NODE_STATEMENT_LIST == statement_list->kind);
  

  if (NULL != statement_list->data.statement_list.init) {
    error_count += refactor_statement_list(table, statement_list->data.statement_list.init, rules_path);
  }
  return error_count
       + refactor_expression_statement(table, statement_list->data.statement_list.statement);
}
