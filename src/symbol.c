#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>

#include "node.h"
#include "symbol.h"
#include "type.h"
#include "ir.h"
#include "debug.h"

#define FIRST_USABLE_REGISTER	8
#define MAX_USABLE_REGISTER 	4

/************************
 * CREATE SYMBOL TABLES *
 ************************/

void symbol_initialize_table(struct symbol_table *table) {
  table->variables = NULL;
  table->parent = NULL;
  table->table_count = 0;
  table->reg_count = 8;
}

static struct symbol *symbol_get(struct symbol_table *table, char name[]) {
  struct symbol_list *iter;
  if (name) {
    for (iter = table->variables; NULL != iter; iter = iter->next) {
      if (!strcmp(name, iter->symbol.name)) {
        return &iter->symbol;
      }
    }
    if (table->parent)
      return symbol_get(table->parent, name);
  }
  return NULL;
}

static struct symbol *symbol_get_by_type(struct symbol_table *table, int ptype) {
  struct symbol_list *iter;
  struct type *type;
  for (iter = table->variables; NULL != iter; iter = iter->next) {
      if (iter->symbol.result.type) {
	  type = peek_parent_type(iter->symbol.result.type);
	  if (type && type->kind == ptype || 
	      iter->symbol.result.type->data.basic.parent_type &&
	      iter->symbol.result.type->data.basic.parent_type->kind == ptype)
		return &iter->symbol;
      }
      if (table->parent)
          return symbol_get_by_type(table->parent, ptype);
  }
  return NULL;
}

static struct symbol_table *top_table = NULL;

static struct symbol_table *find(struct node *n, struct symbol_table *table) {
  struct symbol *sym = NULL; //n? get_id(n)->data.identifier.symbol: NULL;
  struct symbol_list *iter;
  sym = n? get_id(n)->data.identifier.symbol: NULL;
  if (!sym || !table) 
    return NULL;
  for (iter = table->variables; NULL != iter; iter = iter->next) {
    if (&iter->symbol == sym)
      return table;
  }
  return find(n, table->variables->table);
}

struct symbol_table *find_table(struct node *n) {
  return find(n, top_table);
}

bool is_valid_reg(int reg) {
  int abs = reg + FIRST_USABLE_REGISTER; // absolute value of register 'reg'
  if (reg < MAX_USABLE_REGISTER || abs >=24 && abs <=25 || abs > 31)
    return true;
  return false;
}

static int reg_count = 0;

int next_register(struct symbol_table *table) { 
   // table-based registers is proving unstable, so using a single register counter
   while (!is_valid_reg(++reg_count)) {}
   return reg_count;
}

static struct symbol *symbol_put(struct symbol_table *table, char name[]) {
  struct symbol_list *symbol_list;

  symbol_list = malloc(sizeof(struct symbol_list));
  assert(NULL != symbol_list);

  strncpy(symbol_list->symbol.name, name, IDENTIFIER_MAX);
  symbol_list->symbol.result.type = NULL;
  symbol_list->symbol.result.ir_operand = new_register(next_register(table));
  symbol_list->symbol.result.table = table;
  symbol_list->table = NULL;

  symbol_list->next = table->variables;
  table->variables = symbol_list;

  return &symbol_list->symbol;
}

static void set_base_type_int(struct symbol *sym, int btype, int is_unsigned);

static int symbol_add_from_identifier(struct symbol_table *table, struct node *identifier, bool define) {
  assert(NODE_IDENTIFIER == identifier->kind);

  if (identifier->data.identifier.is_string) {
     //identifier->data.identifier.symbol->result.ir_operand = new_register(table->reg_count++);
  }
  if (identifier->data.identifier.is_op || identifier->data.identifier.is_terminal ||
      identifier->data.identifier.is_string)
    return 0;
  identifier->data.identifier.symbol = symbol_get(table, identifier->data.identifier.name);
  if (NULL == identifier->data.identifier.symbol) {
    if (define) {
      identifier->data.identifier.symbol = symbol_put(table, identifier->data.identifier.name);
    } else {
      if (identifier->data.identifier.is_string) {
          identifier->data.identifier.symbol = symbol_put(table, identifier->data.identifier.name);
	  push_symbol_type(identifier->data.identifier.symbol, TYPE_POINTER);
	  //set_base_type_int(identifier->data.identifier.symbol, TYPE_BASIC_CHAR, 0);
          return 0;
      }
      symbol_print_table(stdout, table);
      symbol_print_table(stdout, table->parent);
      compiler_print_error(identifier->location, "undefined identifier %s", identifier->data.identifier.name);
      return 1;
    }
  } else if (define) {
    compiler_print_error(identifier->location, "duplicate symbol %s", identifier->data.identifier.name);
    return 1;
  }
  return 0;

}

static int symbol_add_from_expression(struct symbol_table *table, struct node *expression);

static int symbol_add_from_expression_update(struct symbol_table *table, struct node *expression, bool add);

static int symbol_add_from_binary_operation(struct symbol_table *table, struct node *binary_operation) {
  struct symbol *s1 = symbol_get(table, get_name(binary_operation->data.binary_operation.left_operand));
  struct symbol *s2 = symbol_get(table, get_name(binary_operation->data.binary_operation.right_operand));
  binary_operation->result.table = table;
  binary_operation->data.binary_operation.left_operand->result.table = table;
  binary_operation->data.binary_operation.right_operand->result.table = table;
  if (s1 && s1->result.type) {
    binary_operation->data.binary_operation.left_operand->result.type = copyType(s1->result.type);
    binary_operation->result.type = copyType(s1->result.type);
  }
  if (s2 && s2->result.type)
    binary_operation->data.binary_operation.right_operand->result.type = copyType(s2->result.type);
  assert(NODE_BINARY_OPERATION == binary_operation->kind);

  switch (binary_operation->data.binary_operation.operation) {
    case BINOP_MULTIPLICATION:
    case BINOP_DIVISION:
    case BINOP_MOD:
    case BINOP_ADDITION:
    case BINOP_SUBTRACTION:
    case BINOP_EQUALITY:
    case BINOP_LOGICAL_OR:
    case BINOP_XOR:
    case BINOP_LOGICAL_AND:
    case BINOP_RELATIONAL:
    case BINOP_SHIFT:
      return symbol_add_from_expression(table, binary_operation->data.binary_operation.left_operand)
           + symbol_add_from_expression(table, binary_operation->data.binary_operation.right_operand);
    case BINOP_ASSIGN:
      if (NODE_IDENTIFIER == binary_operation->data.binary_operation.left_operand->kind ||
          NODE_DEREF == binary_operation->data.binary_operation.left_operand->kind) {
        return symbol_add_from_expression_update(table, binary_operation->data.binary_operation.left_operand, false)
             + symbol_add_from_expression(table, binary_operation->data.binary_operation.right_operand);
      } else {
        compiler_print_error(binary_operation->data.binary_operation.left_operand->location,
                             "left operand of assignment must be an identifier or deref");
        return 1
             + symbol_add_from_expression(table, binary_operation->data.binary_operation.left_operand)
             + symbol_add_from_expression(table, binary_operation->data.binary_operation.right_operand);
      }
      return 0;
    default:
      assert(0);
      return 1;
  }
}

static struct symbol_table *new_table(struct symbol_table *table) {
  struct symbol_table *new_tbl;
  char table_name[IDENTIFIER_MAX];
  table->table_count++;
  sprintf(table_name, "fn_other_names%d", table->table_count);
  new_tbl = malloc(sizeof(struct symbol_table));
  symbol_initialize_table(new_tbl);
  symbol_put(table, table_name);
  new_tbl->parent = table;
  new_tbl->reg_count = 0;
  table->variables->table = new_tbl;
  printf("new_table.table=%p, next=%p, top=%p, top.next=%p\n", table, table->variables->table, top_table, top_table->variables->table);
  return new_tbl;
}

static void insert_parent_type(struct symbol *sym, int ptype);
static void set_parent_type(struct symbol *sym, int ptype);
static struct type *get_parent_type(struct symbol *sym);
static void set_base_type(struct symbol *sym, struct node* type);

char *get_name(struct node *n) {
  struct node * id = get_id(n);
  return id? id->data.identifier.name: NULL;
}


struct node *get_id(struct node *n) {
  struct node * id = NULL;
  if (n) {
      switch (n->kind) {
        case NODE_IDENTIFIER:
          if (!n->data.identifier.is_op && !n->data.identifier.is_terminal &&
              !n->data.identifier.is_string)
             return n;
          return NULL;
        case NODE_ARRAY_DECL:
        case NODE_POINTER:
        case NODE_DEREF:
        case NODE_EXPR_LIST:
          id = get_id(n->data.expr_list.curr);
          if (id) return id;
          return get_id(n->data.expr_list.next);
        case NODE_DECL:
          return get_id(n->data.decl.dlist);
        case NODE_DECL_LIST:
          id = get_id(n->data.decl_list.decl);
          if (id) return id;
          return get_id(n->data.decl_list.dlist);
        case NODE_TERNARY_EXPR:
          if (n->data.ternary_expr.bin_op)
                return get_id(n->data.ternary_expr.bin_op);
          if (n->data.ternary_expr.is_conditional)
                return get_id(n->data.ternary_expr.unary_expr);
        default:
          return NULL;
      }
  }
  return NULL;
}


int size(char** a) {
  int i = 0;
  while (a != NULL && a[i] != NULL && i<IDENTIFIER_MAX)
    i++;
  return i;
}

char **append(char** a, char* b) {
  int i = 0;
  while (a[i] != NULL) 
    i++;
  a[i++] = b;
  a[i] = NULL;
  return a;
}

char **concat(char** a, char** b) {
  int i = 0;
  while (b != NULL && b[i] != NULL)
    append(a, b[i++]); 
  return a;
}

int contains(char** strs, char *match) {
  int i = 0;
  if (strs == NULL || strs[0] == NULL)
    return 0;
  for (i=0; i<IDENTIFIER_MAX; i++) {
    if (strs[i] && strcmp(strs[i], match) == 0)
	return 1;
  }
  return 0;
}

char **get_names(struct node *n) {
  char ** names = malloc(IDENTIFIER_MAX * sizeof(char *));
  names[0] = NULL;
  if (n) {
      switch (n->kind) {
        case NODE_IDENTIFIER:
          if (!n->data.identifier.is_op && !n->data.identifier.is_terminal &&
              !n->data.identifier.is_string)
             return append(names, n->data.identifier.name);
          return NULL;
   	case NODE_CHAR:
	  return append(names, "char");
	case NODE_SHORT:
	  return append(names, "short");
	case NODE_INT:
	  return append(names, "int");
	case NODE_LONG:
	  return append(names, "long");
        case NODE_ARRAY_DECL:
        case NODE_POINTER:
        case NODE_DEREF:
	case NODE_DSPECIFIER:
        case NODE_EXPR_LIST:
	  concat(names, get_names(n->data.expr_list.curr));
          return concat(names, get_names(n->data.expr_list.next));
        case NODE_DECL:
          return concat(names, get_names(n->data.decl.dlist));
        case NODE_DECL_LIST:
          concat(names, get_names(n->data.decl_list.decl));
          return concat(names, get_names(n->data.decl_list.dlist));
        case NODE_TERNARY_EXPR:
          if (n->data.ternary_expr.bin_op)
                return concat(names, get_names(n->data.ternary_expr.bin_op));
          if (n->data.ternary_expr.is_conditional)
                return concat(names, get_name(n->data.ternary_expr.unary_expr));
        default:
          return NULL;
      }
  }
  return NULL;
}

static int update_syms(struct symbol_table *table, struct node *n, 
	struct node *base_type, int ptype, long ubound, bool insert) {
  int errcount = 0;
  struct symbol *sym = NULL;
  struct type *parent_type = NULL;
  if (n) {
      switch (n->kind) {
        case NODE_IDENTIFIER:
          if (!n->data.identifier.is_op && !n->data.identifier.is_terminal &&
		!n->data.identifier.is_string) {
	      sym = symbol_get(table, n->data.identifier.name);
	      if (sym) {
	   	parent_type = get_parent_type(sym);
		if (parent_type) {
		    //type_print(stdout, parent_type);
		    //type_print(stdout, sym->result.type);
		}
	 	if (parent_type && parent_type->kind == TYPE_FUNCTION) {
		    if (ptype == TYPE_ARRAY) {
			compiler_print_error(n->location, "functions cannot return arrays: %s", n->data.identifier.name);
			return ++errcount;
		    }
		}
		if (base_type)
		    set_base_type(sym, base_type);
		if (ptype != TYPE_DECL) {
		  if (insert)
		    insert_parent_type(sym, ptype);
		  else if (ptype != TYPE_BASIC)
		    set_parent_type(sym, ptype);
		  if (ubound != ULONG_MAX)
		    get_parent_type(sym)->data.basic.ubound = ubound;
		}
	      } 
	  }
	  break;
	case NODE_DECL:
	  errcount += update_syms(table, n->data.decl.dlist, base_type, ptype, ubound, insert);
	  break;
        case NODE_ARRAY_DECL:
        case NODE_POINTER:
        case NODE_EXPR_LIST:
          while (n) {
	      errcount += update_syms(table, n->data.expr_list.curr, base_type, ptype, ubound, insert);
	      n = n->data.expr_list.next;
          }
	  break;
        default:
          break; 
      }
  }
  return 0;
}

static long eval_op(char *op, struct node *e1, struct node *e2, struct node *e3) {
    long n1 = e1? e1->data.number.value: ULONG_MAX;
    long n2 = e2? e2->data.number.value: ULONG_MAX;
    long n3 = e3? e3->data.number.value: ULONG_MAX;
    if (strcmp(op, "&") == 0) return n1 & n2;
    else if (strcmp(op, "*") == 0) {
      if (n2 == ULONG_MAX)
          return ULONG_MAX;
      return n1 * n2;
    } else if (strcmp(op, "/") == 0) return n1 / n2;
    else if (strcmp(op, "-") == 0) return n1 - n2;
    else if (strcmp(op, "--") == 0) return e1? n1--: --n2;
    else if (strcmp(op, "+") == 0) return n1 + n2;
    else if (strcmp(op, "++") == 0) return e1? n1++: ++n2;
    else if (strcmp(op, "!") == 0) return !n1;
    else if (strcmp(op, "=") == 0) { e1->data.number.value = n2; return n2; }
    else if (strcmp(op, "==") == 0) return n1 == n2;
    else if (strcmp(op, "+=") == 0) return e1->data.number.value = e1->data.number.value + n2;
    else if (strcmp(op, "-=") == 0) return e1->data.number.value = e1->data.number.value - n2;
    else if (strcmp(op, "*=") == 0) return e1->data.number.value = e1->data.number.value * n2;
    else if (strcmp(op, "/=") == 0) return e1->data.number.value = e1->data.number.value / n2;
    else if (strcmp(op, ">") == 0) return n1 > n2;
    else if (strcmp(op, "<") == 0) return n1 < n2; 
    else if (strcmp(op, "<=") == 0) return n1 <= n2;
    else if (strcmp(op, ">=") == 0) return n1 >= n2;
    else if (strcmp(op, "!=") == 0) return n1 != n2;
    else if (strcmp(op, "|=") == 0) return e1->data.number.value = e1->data.number.value | n2;
    else if (strcmp(op, "||") == 0) return n1 || n2;
    else if (strcmp(op, "&&") == 0) return n1 && n2;
    else if (strcmp(op, "|") == 0) return n1 | n2;
    else if (strcmp(op, "&") == 0) return n1 & n2;
    else if (strcmp(op, "&=") == 0) return e1->data.number.value = e1->data.number.value & n2;
    else if (strcmp(op, ">>") == 0) return n1 >> n2;
    else if (strcmp(op, "<<") == 0) return n1 << n2;
    else if (strcmp(op, "?") == 0) return n1? n2: n3;
    else if (strcmp(op, "~") == 0) return ~n1;
    else if (strcmp(op, "%") == 0) return n1 % n2;
    else if (strcmp(op, "%=") == 0) return e1->data.number.value = e1->data.number.value % n2;
    else if (strcmp(op, "^") == 0) return n1 ^ n2;
    else if (strcmp(op, "^=") == 0) return e1->data.number.value = e1->data.number.value ^ n2;
    return ULONG_MAX;
}

int is_op(struct node *expr) {
    return expr && expr->kind == NODE_IDENTIFIER && expr->data.identifier.is_op;
}

static long eval_const_expr(struct symbol_table *table, struct node *expr) {
  struct symbol *sym;
  struct node **nodes = malloc(5 * sizeof(struct node));
  struct node *op = NULL;
  int i = 0;
  for (i=0; i<5; i++)  
      nodes[i] = NULL;
  i = 0;
  if (expr) {
      switch (expr->kind) {
        case NODE_NUMBER:
          return expr->data.number.value;
        case NODE_IDENTIFIER:
          sym = symbol_get(table, expr->data.identifier.name);
          if (sym) return sym->result.ir_operand->data.number;
          compiler_print_error(expr->location, "unknown symbol %s", expr->data.identifier.name);
          return ULONG_MAX;
        case NODE_EXPR_LIST:
          while (expr) {
	      if (expr->data.expr_list.curr && i < 5) {
                if (expr->data.expr_list.curr->kind == NODE_IDENTIFIER) {
		  if (is_op(expr->data.expr_list.curr)) 
		    op = expr->data.expr_list.curr;
		} else if (expr->data.expr_list.curr->kind == NODE_NUMBER) {
                  nodes[i++] = expr->data.expr_list.curr;
                } else {
                  nodes[i++] = node_number(expr->data.expr_list.curr->location, "0");
		  nodes[i-1]->data.number.value = 
		      eval_const_expr(table, expr->data.expr_list.curr);
		  nodes[i-1]->result.table = table;
		  if (nodes[i-1]->data.number.value == ULONG_MAX) i--;
		}
	      }
              expr = expr->data.expr_list.next;
          }
	  //debug("eval_const_expr.op", op, 0);
	  if (op)
	      return eval_op(op->data.identifier.name, nodes[0], nodes[1], nodes[2]);
	  else if (!op && i > 0)
	      return nodes[0]->data.number.value;
        default:
          return ULONG_MAX;
      }
  }
  return ULONG_MAX;
}

static void eval_assign(struct symbol_table *table, struct node *expr) {
    struct symbol *sym;
    if (!expr) return;
    expr->result.ir_operand = new_operand(eval_const_expr(table, expr));
    expr->result.type = type_basic(false, TYPE_BASIC_INT);
    expr->result.type->kind = expr->result.ir_operand->data.number==ULONG_MAX? TYPE_VOID: TYPE_BASIC;
    //if (expr->result.type->kind == TYPE_VOID) {
        sym = symbol_get(table, get_name(expr));
	if (sym && sym->result.type) {
	    expr->result.type = copyType(sym->result.type);
	}
    //}
}

static struct node *get_types(struct node *n) {
  struct node *types = NULL;
  if (n) {
      switch (n->kind) {
	 case NODE_IDENTIFIER:
	   break;
         case NODE_CHAR:
         case NODE_SHORT:
         case NODE_INT:
         case NODE_LONG:
           return n;
 	 case NODE_DSPECIFIER:
         case NODE_EXPR_LIST:
           types = get_types(n->data.expr_list.curr);       
	   if (types) return types;
	   return get_types(n->data.expr_list.next);
         default:
           return NULL;
      }
      return NULL;
  }
  return NULL;
}

static int expr_types[4] = {
  NODE_DSPECIFIER, NODE_EXPR_LIST, NODE_PARAM_LIST, NODE_DEREF
};

bool expr_type(int entry) {
  int i = 0;
  for (i=0; i<4; i++)
    if (expr_types[i] == entry)
      return true;
  return false;
}

struct node *get_expr(struct node *expr, int i) {
  if (expr->kind == NODE_STATEMENT_LIST)
    return get_statement(expr, i);
  while (expr && i-- > 0) {
        expr = expr_type(expr->kind)? expr->data.expr_list.next: NULL;
  }
  return expr;
}

static void insert_parent_type(struct symbol *sym, int ptype) {
  struct type *type  = NULL;
  if (sym) {
    if (!sym->result.type)
        sym->result.type = type_basic(true, TYPE_BASIC_INT);
    type = type_basic(true, TYPE_BASIC_INT);
    type->kind = ptype;
    type->data.basic.parent_type = sym->result.type->data.basic.parent_type;
    sym->result.type->data.basic.parent_type = type;
  }
}

static void set_parent_type(struct symbol *sym, int ptype) {
  struct type *type = NULL;
  if (sym) { 
    if (!sym->result.type)
        sym->result.type = type_basic(true, TYPE_BASIC_INT);
    type = sym->result.type;
    while (type->data.basic.parent_type) 
        type = type->data.basic.parent_type;
    type->data.basic.parent_type =
        type_basic(true, TYPE_BASIC_INT);
    type->data.basic.parent_type->kind = ptype;
  }
}

static void set_base_type_int(struct symbol *sym, int btype, int is_unsigned) {
  if (sym) {
    if (sym->result.type == NULL)
        sym->result.type = type_basic(false, btype);
    sym->result.type->data.basic.datatype = btype;
    sym->result.type->data.basic.is_unsigned = is_unsigned;
  }
}

static struct type *get_parent_type(struct symbol *sym) {
  if (sym && sym->result.type) {
    return sym->result.type->data.basic.parent_type;
  }
  return NULL;
}

static void set_base_type(struct symbol *sym, struct node* type) {
  int base_type;
  if (sym && type) {
    assert(type->result.type);
    switch (type->kind) {
	case NODE_CHAR:
	  base_type = TYPE_BASIC_CHAR; break;
	case NODE_SHORT:
	case NODE_INT:
	  base_type = TYPE_BASIC_INT; break;
	case NODE_LONG:
	  base_type = TYPE_BASIC_LONG; break;
	default:
	  base_type = type->result.type->data.basic.datatype;
    }
    type->result.type->data.basic.datatype = base_type;
    set_base_type_int(sym, base_type, !type->data.type.is_signed);
  }
}

static int validate_parameters(struct table * table, struct node * n) {
   return 0;
}

static int symbol_add_from_expression_update_check(struct symbol_table *table, struct node *expression, bool add, bool check);

static int symbol_add_from_expr_list(struct symbol_table *table, struct node *expression, bool add, bool check) {
    struct node *main_expr = expression;
    struct symbol * sym;
    int errcount = 0;
    expression->result.table = table;
     validate_parameters(table, expression);
      if (expression->data.expr_list.is_func_decl) {
          errcount += symbol_add_from_expression_update_check(table->parent? table->parent: table, expression->data.expr_list.curr, true, false);
          errcount += update_syms(table, expression->data.expr_list.curr, NULL, TYPE_FUNCTION, ULONG_MAX, false);
          errcount += symbol_add_from_expression_update_check(table, expression->data.expr_list.next, add, check);
          return errcount;
      }
      if (expression->kind == NODE_PARAM_LIST && !table->parent)
          return errcount;
      check = check && expression->kind!=NODE_PARAM_LIST;
      add = add && !expression->data.expr_list.is_call;
      while (expression) {
          if (main_expr->data.expr_list.is_while) {
		debugf("symbol_add_from_expr_list.is_while.curr=%n\n", expression->data.expr_list.curr);
	  }
          errcount += symbol_add_from_expression_update_check(table, expression->data.expr_list.curr, add, check);
          expression = expression->data.expr_list.next;
      }
      if (main_expr->data.expr_list.is_return) {
        sym = symbol_get_by_type(table, TYPE_FUNCTION);
        if (sym && sym->result.type)
          main_expr->result.type = copyType(sym->result.type);
      } else if (main_expr->data.expr_list.is_call) {
        sym = symbol_get(table, get_name(main_expr));
        if (sym && sym->result.type)
          main_expr->result.type = copyType(sym->result.type);
      } else {
        if (main_expr->kind == NODE_DEREF) 
	  main_expr->result.ir_operand = new_register(table->reg_count++);
	if (get_name(main_expr))
	  debugf("eval_assign: main_expr=%s\n", get_name(main_expr));
	else
          eval_assign(table, main_expr);
      }
      return errcount;
}



static int symbol_add_from_expression(struct symbol_table *table, struct node *expression) {
  return symbol_add_from_expression_update(table, expression, false);
}

static int symbol_add_from_expression_safe(struct symbol_table *table, struct node *expression, bool add) {
  if (expression)
    return symbol_add_from_expression_update(table, expression, add);
  return 0;
}

static int symbol_add_from_expression_safe_check(struct symbol_table *table, struct node *expression, bool add, bool check) {
  if (expression)
    return symbol_add_from_expression_update_check(table, expression, add, check);
  return 0;
}

static int symbol_add_from_expression_update(struct symbol_table *table, struct node *expression, bool add) {
  return symbol_add_from_expression_update_check(table, expression, add, true);
}

static int symbol_add_from_expression_update_check(struct symbol_table *table, struct node *expression, bool add, bool check) {
  struct node *main_expr = expression;
  struct symbol *sym;
  int errcount = 0;
  if (!expression) return ++errcount;
  debugf("symbol_add_from_expression_update_check.expression=%n\n", expression);
  switch (expression->kind) {
    case NODE_BINARY_OPERATION:
      return symbol_add_from_binary_operation(table, expression);
    case NODE_IDENTIFIER:
      if (expression->data.identifier.is_op || expression->data.identifier.is_terminal ||
	  expression->data.identifier.is_string)
        return errcount;
      return symbol_add_from_identifier(table, expression, add);
    case NODE_CHAR:
    case NODE_SHORT:
    case NODE_INT:
    case NODE_LONG:
    case NODE_NUMBER:
      expression->result.table = table;
      return errcount;
    case NODE_STATEMENT_LIST:
      return symbol_add_from_statement_list(table, expression);
    case NODE_DECL: 
      errcount += symbol_add_from_expression_safe_check(table, expression->data.decl.dspec, true, check);
      errcount += symbol_add_from_expression_safe_check(table, expression->data.decl.dlist, true, check);
      update_syms(table, expression->data.decl.dlist, get_types(expression->data.decl.dspec), TYPE_DECL, ULONG_MAX, false);
      sym = symbol_get(table, get_name(expression->data.decl.dlist));
      sym->result.type->data.basic.is_unsigned = contains(get_names(expression->data.decl.dspec), "unsigned");
      expression->result.type = sym->result.type;
      expression->result.ir_operand = sym->result.ir_operand;
      expression->file_scope = table->parent? 0: 1;
      return errcount;
    case NODE_DECL_LIST:
      errcount += symbol_add_from_expression_safe_check(table, expression->data.decl_list.dlist, false, check);
      errcount += symbol_add_from_expression_safe_check(table, expression->data.decl_list.decl, true, check);
      return errcount;
    case NODE_FUNCTION:
      table = new_table(table);
      errcount += symbol_add_from_expression_safe(table, expression->data.unary_expr.p1, false);
      errcount += symbol_add_from_expression_safe(table, expression->data.unary_expr.p2, true);
      update_syms(table, expression->data.unary_expr.p1, NULL, TYPE_FUNCTION, ULONG_MAX, false);
      sym = symbol_get(table, get_name(expression->data.unary_expr.p1));
      sym->result.type->data.basic.is_unsigned = contains(get_names(expression->data.unary_expr.p1), "unsigned");
      expression->result.type = sym->result.type;
      return errcount;
    case NODE_PARAM_LIST:
    case NODE_DEREF:
    case NODE_DSPECIFIER:
    case NODE_EXPR_LIST:
	errcount += symbol_add_from_expr_list(table, expression, add, check);
        return errcount;
    case NODE_ARRAY_DECL:
      errcount += symbol_add_from_expression_update(table, expression->data.expr_list.curr, add);
      sym = symbol_get(table, get_name(expression->data.expr_list.curr));
      update_syms(table, expression->data.expr_list.curr, get_types(expression->data.expr_list.curr), TYPE_ARRAY, eval_const_expr(table, get_expr(expression->data.expr_list.next, 1)), true);
      if (check && sym && get_parent_type(sym) && get_parent_type(sym)->data.basic.ubound == ULONG_MAX) {
          compiler_print_error(expression->location, "array bounds missing %s", get_name(expression->data.expr_list.curr));
          ++errcount;
      }
      return errcount;
    case NODE_POINTER:
      errcount += symbol_add_from_expression_update_check(table, expression->data.expr_list.next, true, check);
      update_syms(table, expression, get_types(expression->data.expr_list.next), TYPE_POINTER, ULONG_MAX, true);
      return errcount;
    case NODE_LABEL:
      errcount += symbol_add_from_expression_update(table, expression->data.expr_list.next, true);
      update_syms(table, expression->data.expr_list.next, NULL, TYPE_LABEL, ULONG_MAX, false);
      return errcount;
    case NODE_TERNARY_EXPR:
      if (expression->data.ternary_expr.bin_op)
         return symbol_add_from_expression(table, expression->data.ternary_expr.bin_op);
    default:
      assert(0);
      return ++errcount;
  }
}

static int symbol_add_from_expression_statement(struct symbol_table *table, struct node *expression_statement) {
  /*assert(NODE_EXPRESSION_STATEMENT == expression_statement->kind);*/
  assert(expression_statement->kind);

  switch (expression_statement->kind) {
    case NODE_EXPRESSION_STATEMENT:
      return symbol_add_from_expression(table, expression_statement->data.expression_statement.expression);
    case NODE_DECL:
    case NODE_DECL_LIST:
    case NODE_EXPR_LIST:
    case NODE_PARAM_LIST:
    case NODE_DEREF:
    case NODE_TERNARY_EXPR:
    case NODE_FUNCTION:
      return symbol_add_from_expression(table, expression_statement);
    default:
      assert(0);
      return 1;
  }
}

int symbol_add_from_statement_list(struct symbol_table *table, struct node *statement_list)
{
  int error_count = 0;
  assert(NODE_STATEMENT_LIST == statement_list->kind);
  if (top_table == NULL)
    top_table = table;

  if (NULL != statement_list->data.statement_list.init) {
    error_count += symbol_add_from_statement_list(table, statement_list->data.statement_list.init);
  }
  return error_count
       + symbol_add_from_expression_statement(table, statement_list->data.statement_list.statement);
}

char *type_string(int type) {
    switch (type) {
        case TYPE_BASIC_CHAR:
            return "char";
        case TYPE_BASIC_SHORT:
            return "short";
        case TYPE_BASIC_INT:
            return "int";
        case TYPE_BASIC_LONG:
            return "long";
        default:
            return ""; 
    }
}


void symbol_print(FILE* output, struct symbol *sym) {
    struct type *type;
    if (sym == NULL)
        return;
    fprintf(output, " variable: ");
    type = sym->result.type;
    while (type && type->data.basic.parent_type) {
      fprintf(output, "(");
      type = type->data.basic.parent_type;
    }
    type = sym->result.type;
    if (type && type->kind!=TYPE_VOID && type->kind!=TYPE_LABEL) {
        fprintf(output, type->data.basic.is_unsigned? "unsigned ": "signed ");
        fprintf(output, "%s ", type_string(type->data.basic.datatype));
    }
    if (sym->result.type) {
      fprintf(output, " %s", sym->name);
      /*
      if (sym->result.type->data.basic.parent_type->kind==TYPE_LABEL)
        fprintf(output, "<label>");
      if (sym->result.type->data.basic.parent_type->kind==TYPE_DECL)
        fprintf(output, "<decl>");*/
    } else { fprintf(output, " %s", sym->name); }
    while (type && type->data.basic.parent_type) {
      if (type->data.basic.parent_type->kind==TYPE_POINTER)
        fprintf(output, "<pointer>)");
      else if (type->data.basic.parent_type->kind==TYPE_ARRAY)
	fprintf(output, "[%ld]<array>)", type->data.basic.parent_type->data.basic.ubound);
      else if (type->data.basic.parent_type->kind==TYPE_FUNCTION)
	fprintf(output, "<function>)");
      else fprintf(output, ")" );
      type = type->data.basic.parent_type;
    }

    fprintf(output, " /* %p */", (void *)sym);
}


/***********************
 * PRINT SYMBOL TABLES *
 ***********************/

void symbol_print_table(FILE *output, struct symbol_table *table) {
  struct symbol_list *iter;

  fputs("symbol table:\n", output);
  for (iter = table->variables; NULL != iter; iter = iter->next) {
    if (iter->table) {
      fprintf(output, "  %s:[\n", iter->symbol.name);
      symbol_print_table(output, iter->table);
      fprintf(output, "  ]\n");
    } else {
      symbol_print(output, &iter->symbol);
      fputs("\n", output);
    }
  }
  fputs("\n", output);
}
