%option yylineno
%option reentrant
%option bison-bridge
%option bison-locations
%option nounput
%option noyyalloc
%option noyyrealloc
%option noyyfree
%option noyywrap

%{
/*
 * scanner.lex
 *
 * This file contains the specification for the Flex generated scanner
 * for the CSCI E-95 sample language.
 *
 */

  #include <stdlib.h>
  #include <errno.h>
  #include <string.h>

  /* Suppress compiler warnings about unused variables and functions. */
  #define YY_EXIT_FAILURE ((void)yyscanner, 2)
  #define YY_NO_INPUT

  /* Track locations. */
  #define YY_EXTRA_TYPE int
  #define YY_USER_ACTION { yylloc->first_line = yylloc->last_line = yylineno; \
                           yylloc->first_column = yyextra; \
                           yylloc->last_column = yyextra + yyleng - 1; \
                           yyextra += yyleng; }

  #include "compiler.h"
  #include "parser.tab.h"
  #include "node.h"
  #include "type.h"
  #include "debug.h"

%}

newline         \n
ws              [ \t\v\f]

digit           [[:digit:]]
letter          [[:alpha:]]

id              ({letter}|[\_])({letter}|{digit}|[\_\.])*
number          {digit}+
char		[\']\\?.[\']

operator	([\!\>\<\&\|\+\-\/\*\%\^]=|[\+\-\=\&\|\>\<\^]{1,2}|[\*\/\%\?\!\~])
quote		[\"\']
string		{quote}[^\"\']*{quote}
reserved	(break|char|continue|do|else|for|goto|if|int|long|return|short|(un)?signed|void|while|bool|struct|union)
expr		[\[\]\(\)\{\}\;\:\,]

comment		\/\*.*\*\/
invalid		(\r[^\n]|[^\t\ \v\f\$\@\~\#\^\,\.\;\:\'\?])

%%

{comment}   *yylval = node_identifier(*yylloc, " ", yyleng);

{char}      {*yylval = node_number(*yylloc, "0"); 
     		debugf("yytext0=%s\n", yytext);
              if (strcmp(yytext, "'\\0'")!=0) {
	        debugf("yytext = %s\n", yytext);
	     	((struct node *)*yylval)->data.number.value = yytext[1];
	      }
              return NUMBER; 
            }

{string}    {*yylval = node_string(*yylloc, yytext, yyleng); return STRING; }

{newline}   yyextra = 1;

{ws}        /* do nothing */

{expr}     {
	     *yylval = node_identifier(*yylloc, yytext, 1);
             if (strcmp(yytext, ";") == 0) return SEMICOLON;
             else if (strcmp(yytext, ":") == 0) return COLON;
             else if (strcmp(yytext, ",") == 0) return COMMA;
             else if (strcmp(yytext, "(") == 0) return LEFT_PAREN;
             else if (strcmp(yytext, ")") == 0) return RIGHT_PAREN;
             else if (strcmp(yytext, "[") == 0) return LEFT_SQUARE;
             else if (strcmp(yytext, "]") == 0) return RIGHT_SQUARE;
             else if (strcmp(yytext, "{") == 0) return LEFT_CURLY;
             else if (strcmp(yytext, "}") == 0) return RIGHT_CURLY;
            } 


 /*(break|char|continue|do|else|for|goto|if|int|long|return|short|(un)?signed|void|while|struct|union)*/

{reserved}  {
             *yylval = node_identifier(*yylloc, yytext, yyleng);
             if (strcmp(yytext, "int") == 0) return INT;
             else if (strcmp(yytext, "char") == 0) return CHAR;
             else if (strcmp(yytext, "short") == 0) return SHORT;
             else if (strcmp(yytext, "long") == 0) return LONG;
             else if (strcmp(yytext, "signed") == 0) return SIGNED;
             else if (strcmp(yytext, "unsigned") == 0) return UNSIGNED;
             else if (strcmp(yytext, "for") == 0) return FOR;
             else if (strcmp(yytext, "goto") == 0) return GOTO;
             else if (strcmp(yytext, "do") == 0) return DO;
             else if (strcmp(yytext, "while") == 0) return WHILE;
             else if (strcmp(yytext, "if") == 0) return IF;
             else if (strcmp(yytext, "else") == 0) return ELSE;
             else if (strcmp(yytext, "void") == 0) return VOID;
             else if (strcmp(yytext, "break") == 0) return BREAK;
             else if (strcmp(yytext, "continue") == 0) return CONTINUE;
             else if (strcmp(yytext, "return") == 0) return RETURN;
             else if (strcmp(yytext, "struct") == 0) return STRUCT;
             else if (strcmp(yytext, "union") == 0) return UNION;
             else  return RESERVED;
            }

{operator}  {
             *yylval = node_operator(*yylloc, yytext, yyleng);
             if (strcmp(yytext, "&") == 0) return AMPERSAND;
             else if (strcmp(yytext, "*") == 0) return ASTERISK;
             else if (strcmp(yytext, "/") == 0) return SLASH;
             else if (strcmp(yytext, "-") == 0) return MINUS;
             else if (strcmp(yytext, "--") == 0) return MINUS_MINUS;
             else if (strcmp(yytext, "+") == 0) return PLUS;
             else if (strcmp(yytext, "++") == 0) return PLUS_PLUS;
             else if (strcmp(yytext, "!") == 0) return EXCLAMATION; 
             else if (strcmp(yytext, "=") == 0) return EQUAL;
             else if (strcmp(yytext, "==") == 0) return EQUAL_EQUAL;
             else if (strcmp(yytext, "+=") == 0) return PLUS_EQUAL;
             else if (strcmp(yytext, "-=") == 0) return MINUS_EQUAL;
             else if (strcmp(yytext, "*=") == 0) return ASTERISK_EQUAL;
             else if (strcmp(yytext, "/=") == 0) return SLASH_EQUAL;
             else if (strcmp(yytext, ">") == 0) return GREATER;
             else if (strcmp(yytext, "<") == 0) return LESS;
             else if (strcmp(yytext, "<=") == 0) return LESS_EQUAL;
             else if (strcmp(yytext, ">=") == 0) return GREATER_EQUAL;
             else if (strcmp(yytext, "!=") == 0) return NOT_EQUAL;
             else if (strcmp(yytext, "|=") == 0) return OR_EQUAL;
             else if (strcmp(yytext, "||") == 0) return LOGICAL_OR;
             else if (strcmp(yytext, "&&") == 0) return LOGICAL_AND;
             else if (strcmp(yytext, "|") == 0) return VBAR;
             else if (strcmp(yytext, "&") == 0) return AMPERSAND;
             else if (strcmp(yytext, "&=") == 0) return AMPERSAND_EQUAL;
             else if (strcmp(yytext, ">>") == 0) return RIGHT_SHIFT;
             else if (strcmp(yytext, "<<") == 0) return LEFT_SHIFT;
             else if (strcmp(yytext, "?") == 0) return QUESTION;
             else if (strcmp(yytext, "~") == 0) return TILDE;
             else if (strcmp(yytext, "%") == 0) return PERCENT;
             else if (strcmp(yytext, "%=") == 0) return PERCENT_EQUAL;
             else if (strcmp(yytext, "^") == 0) return XOR;
             else if (strcmp(yytext, "^=") == 0) return XOR_EQUAL;
             else  return OPERATOR;
            }

  /* constants begin */
{number}    *yylval = node_number(*yylloc, yytext); return NUMBER;

  /* constants end */

  /* identifiers */
{id}        { 
	     *yylval = node_identifier_id(*yylloc, yytext, yyleng); return IDENTIFIER; 
	     }

{invalid}       return SCANNING_ERROR;

.		return -1;

%%

void scanner_initialize(yyscan_t *scanner, FILE *input) {
  yylex_init(scanner);
  yyset_in(input, *scanner);
  yyset_extra(1, *scanner);
}

void scanner_destroy(yyscan_t *scanner) {
  yylex_destroy(*scanner);
  scanner = NULL;
}

void scanner_print_tokens(FILE *output, int *error_count, yyscan_t scanner) {
  YYSTYPE val;
  YYLTYPE loc;
  int token;

  token = yylex(&val, &loc, scanner);
  while (0 != token) {
    /*
     * Print the line number. Use printf formatting and tabs to keep columns
     * lined up.
     */
    fprintf(output, "line = %04d",
            loc.first_line); /*, loc.first_column, loc.last_line, loc.last_column); */

    /*
     * Print the scanned text. Try to use formatting but give up instead of
     * truncating if the text is too long.
     */
    if (yyget_leng(scanner) <= 20) {
      fprintf(output, "     text = %-20s", yyget_text(scanner));
    } else {
      fprintf(output, "     text = %s", yyget_text(scanner));
    }

    if (token <= 0) {
      fputs("     token = ERROR", output);
      (*error_count)++;
    } else {
      fprintf(output, "     token = %-20s", parser_token_name(token));

      switch (token) {
        case NUMBER:
          /* Print the type and value. */
          fputs("     type = ", output);
	  if (val->kind == NODE_IDENTIFIER || val->data.number.value < 0xFFul) {
	    fputs("char", output); 
	  } else if (val->data.number.value < 0xFFFFul) {
	    fputs("short", output);
	  } else if (val->data.number.value < 0xEFFFFFFFul) {
	    fputs("int", output);
	  } else if (val->data.number.value < 0xFFFFFFFFul) {
	    fputs("long", output);
	  }
	  if (val->kind == NODE_IDENTIFIER) {
	    sprintf(val->data.identifier.name, "%d", val->data.identifier.name[1]);
            fprintf(output, "     value = %s", val->data.identifier.name);
          } else {
	    fprintf(output, "     value = %-10lu", val->data.number.value);
            if (val->data.number.overflow) {
              fputs("     OVERFLOW", output);
              (*error_count)++;
            }
	  }
          break;

        case IDENTIFIER:
          fprintf(output, "     name = %s", val->data.identifier.name);
          break;

        case OPERATOR:
	  fprintf(output, "     op = %s", val->data.identifier.name);
	  break;

	case STRING:
	  fprintf(output, "     string = %s", val->data.identifier.name);
	  fprintf(output, "     length = %d", (int)strlen(val->data.identifier.name)-1);
	  break;

	case RESERVED:
	  fprintf(output, "     word = %s", val->data.identifier.name);
	  break;
      }
    }
    fputs("\n", output);
    token = yylex(&val, &loc, scanner);
  }
}

/* Suppress compiler warnings about unused parameters. */
void *yyalloc (yy_size_t size, yyscan_t yyscanner __attribute__((unused)))
{
  return (void *)malloc(size);
}

/* Suppress compiler warnings about unused parameters. */
void *yyrealloc  (void *ptr, yy_size_t size, yyscan_t yyscanner __attribute__((unused)))
{
  return (void *)realloc((char *)ptr, size );
}

/* Suppress compiler warnings about unused parameters. */
void yyfree (void *ptr, yyscan_t yyscanner __attribute__((unused)))
{
  free((char *)ptr); /* see yyrealloc() for (char *) cast */
}
