#ifndef _TYPE_H
#define _TYPE_H

#include <stdio.h>
#include <stdbool.h>

struct node;

enum type_kind {
  TYPE_BASIC,
  TYPE_VOID,
  TYPE_POINTER,
  TYPE_ARRAY,
  TYPE_FUNCTION,
  TYPE_LABEL,
  TYPE_DECL
};

enum type_basic_kind {
  TYPE_BASIC_CHAR,
  TYPE_BASIC_SHORT,
  TYPE_BASIC_INT,
  TYPE_BASIC_LONG
};
struct type {
  enum type_kind kind;
  union {
    struct {
      bool is_unsigned;
      long ubound;
      enum type_basic_kind datatype;
      struct type *parent_type;
    } basic;
  } data;
};

struct type *type_basic(bool is_unsigned, enum type_basic_kind datatype);
void push_symbol_type(struct symbol *sym, int ptype);
struct type* peek_parent_type(struct type *t);
struct type *copyType(struct type *t);

int type_assign_in_statement_list(struct node *statement_list);

void type_print(FILE *output, struct type *type);

#endif /* _TYPE_H */
