#ifndef _DEBUG_H
#define _DEBUG_H

#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>

#include "compiler.h"
#include "parser.tab.h"
#include "node.h"
#include "type.h"
#include "ir.h"

struct type;

static char *nstr[] = (char *[]) {
  "NODE_NUMBER",
  "NODE_IDENTIFIER",
  "NODE_BINARY_OPERATION",
  "NODE_EXPRESSION_STATEMENT",
  "NODE_STATEMENT_LIST",
  "NODE_NULL_STATEMENT",
  "NODE_DECL",
  "NODE_DECL_LIST",
  "NODE_SIGNED_INT",
  "NODE_UNSIGNED_INT",
  "NODE_CHAR",
  "NODE_SHORT",
  "NODE_INT",
  "NODE_LONG",
  "NODE_EXPR",
  "NODE_UNARY_EXPR",
  "NODE_TERNARY_EXPR",
  "NODE_QUINARY_EXPR",
  "NODE_EXPR_LIST",
  "NODE_PARAM_LIST",
  "NODE_EQUAL",
  "NODE_PLUS_EQUAL",
  "NODE_MINUS_EQUAL",
  "NODE_ASTERISK_EQUAL",
  "NODE_SLASH_EQUAL",
  "NODE_PERCENT_EQUAL",
  "NODE_LEFT_SHIFT_EQUAL",
  "NODE_RIGHT_SHIFT_EQUAL",
  "NODE_AMPERSAND_EQUAL",
  "NODE_XOR_EQUAL",
  "NODE_OR_EQUAL",
  "NODE_FUNCTION",
  "NODE_DEREF",
  "NODE_ARRAY_DECL",
  "NODE_POINTER",
  "NODE_LABEL",
  "NODE_DSPECIFIER"
  };

static char *tstr[] = (char * []) {
  "TYPE_BASIC",
  "TYPE_VOID",
  "TYPE_POINTER",
  "TYPE_ARRAY",
  "TYPE_FUNCTION",
  "TYPE_LABEL",
  "TYPE_DECL"
};

static char *bstr[] = (char * []) {
  "TYPE_BASIC_CHAR",
  "TYPE_BASIC_SHORT",
  "TYPE_BASIC_INT",
  "TYPE_BASIC_LONG"
};

static char *rstr[] = (char * []) {
  "IR_NO_OPERATION",
  "IR_MULTIPLY",
  "IR_DIVIDE",
  "IR_MOD",
  "IR_ADD",
  "IR_ADDI",
  "IR_SUBTRACT",
  "IR_OR",
  "IR_XOR",
  "IR_AND",
  "IR_SLT",
  "IR_SLL",
  "IR_SRL",
  "IR_JR",
  "IR_JAL",
  "IR_J",
  "IR_BEQ",
  "IR_BNE",
  "IR_DATA",
  "IR_LOAD_ADDRESS",
  "IR_LOAD_IMMEDIATE",
  "IR_LOAD_BYTE",
  "IR_LOAD_WORD",
  "IR_STORE_WORD",
  "IR_STORE_BYTE",
  "IR_COPY",
  "IR_ADDRESS_OF",
  "IR_LABEL",
  "IR_DECL",
  "IR_PRINT_NUMBER",
  "IR_CALL",
  "IR_PROC_BEGIN",
  "IR_PROC_END",
  NULL
};

static char *opstr[] = (char * []) {
  "OPERAND_NUMBER",
  "OPERAND_TEMPORARY",
  "OPERAND_IDENTIFIER",
  NULL
};


static char *binstr[] = (char *[]) {
  "BINOP_MULTIPLICATION",
  "BINOP_DIVISION",
  "BINOP_MOD",
  "BINOP_ADDITION",
  "BINOP_SUBTRACTION",
  "BINOP_ASSIGN",
  "BINOP_EQUALITY",
  "BINOP_LOGICAL_OR",
  "BINOP_LOGICAL_AND",
  "BINOP_XOR",
  "BINOP_RELATIONAL",
  "BINOP_SHIFT",
  NULL
};

static void debug(char *msg, void *obj, int len, ...) {
    va_list args;
    va_start(args, len);
    struct node *n = (sizeof(obj) == sizeof(struct node *)? (struct node *) obj: NULL);
    struct type *t = (sizeof(obj) == sizeof(struct type *)? (struct type *) obj: NULL);
    return; //TURNED OFF for DEMO
    printf("Debug: %s, kind=%s ", msg, n? nstr[n->kind]: "");
    if (n && n->kind == NODE_IDENTIFIER)
	printf("name=%s ", n->data.identifier.name);
    if (n) {
        printf("(%d, %d) to (%d, %d) ",
          n->location.first_line, n->location.first_column,
          n->location.last_line, n->location.last_column);
    }
    while (len-- > 0) {
        printf("%d ", va_arg(args, int));
    }
    va_end(args); 
    printf("\n");
}

static char * substr(char *str, int start, int end) {
    char * sub = (char *) malloc((end - start + 1) * sizeof(char));
    int i = start;
    while (i <= end) 
	sub[i - start] = str[i++];
    sub[end + 1] = '\0';
    return sub;
}

static void info(char *msg, ...) {
    va_list args;
    va_start(args, strlen(msg));
    struct node *n = NULL;
    struct type *t = NULL;
    struct ir_section *ir = NULL;
    struct ir_operand *op = NULL;
    char *tok = (char *) malloc(strlen(msg) * sizeof(char));
    int i = 0, j = 0;
    return; //LOGS TURNED OFF
    printf("Debugf: ");
    while (msg[i] != '\0') {
        if (msg[i] == '%' || msg[i+1] == '\0') {
            if (strcmp(substr(tok, 0, 1), "%n") == 0) {
                n = va_arg(args, struct node *);
                printf(" [kind=%s ", n? nstr[n->kind]: "");
                if (n && n->kind == NODE_IDENTIFIER)
                  printf("name=%s ", n->data.identifier.name);
                if (n) {
                  printf("(%d, %d) to (%d, %d)] ",
                    n->location.first_line, n->location.first_column,
                    n->location.last_line, n->location.last_column);
                }
                printf(substr(tok, 2, strlen(tok)));
            } else if (strcmp(substr(tok, 0, 1), "%t") == 0) {
                t = va_arg(args, struct type *);
                printf(" [kind=%s ", t? tstr[t->kind]: "");
                printf(", datatype=%s", t? bstr[t->data.basic.datatype]: "");
                t = peek_parent_type(t);
                printf(", parent=%s ] ", t? tstr[t->kind]: "");
                printf(substr(tok, 2, strlen(tok)));
            } else if (strcmp(substr(tok, 0, 1), "%r") == 0) {
                ir = va_arg(args, struct ir_section *);
                printf(" [first.kind=%s,", ir && ir->first? rstr[ir->first->kind]:"");
                printf("last.kind=%s] ", ir && ir->last? rstr[ir->last->kind]:"");
                printf(substr(tok, 2, strlen(tok)));
            } else if (strcmp(substr(tok, 0, 1), "%o") == 0) {
		op = va_arg(args, struct ir_operand *);
		if (op)
		  printf(" [kind=%s, ", opstr[op->kind]);
		if (op && op->kind == OPERAND_NUMBER) 
		  printf("number=%d] ", op->data.number);
		else if (op && op->kind == OPERAND_TEMPORARY)
                  printf("temporary=%d] ", op->data.temporary);
		else if (op && op->kind == OPERAND_IDENTIFIER)
                  printf("name=%s] ", op->data.name);
                printf(substr(tok, 2, strlen(tok)));
            } else if (strcmp(substr(tok, 0, 1), "%d") == 0) {
                printf(tok, va_arg(args, int));
            } else if (strcmp(substr(tok, 0, 1), "%l") == 0) {
                printf(tok, va_arg(args, long));
            } else if (strcmp(substr(tok, 0, 1), "%s") == 0) {
                printf(tok, va_arg(args, char *));
            } else if (strcmp(substr(tok, 0, 1), "%p") == 0) {
                printf(tok, va_arg(args, void *));
            } else
                printf(tok);
            j = 0;
        }
        tok[j++] = msg[i]; tok[j] = '\0';
        i++;
    }
    printf(tok);
}

static void debugf(char *msg, ...) {
    va_list args;
    va_start(args, strlen(msg));
    struct node *n = NULL;
    struct type *t = NULL;
    struct ir_section *ir = NULL;
    struct ir_operand *op = NULL;
    char *tok = (char *) malloc(strlen(msg) * sizeof(char));
    int i = 0, j = 0;
    //return; //LOGS TURNED OFF
    printf("Debugf: ");
    while (msg[i] != '\0') {
	if (msg[i] == '%' || msg[i+1] == '\0') {
	    if (strcmp(substr(tok, 0, 1), "%n") == 0) {
		n = va_arg(args, struct node *);
	    	printf(" [kind=%s ", n? nstr[n->kind]: "");
    		if (n && n->kind == NODE_IDENTIFIER)
        	  printf("name=%s ", n->data.identifier.name);
    		if (n) {
        	  printf("(%d, %d) to (%d, %d)] ",
          	    n->location.first_line, n->location.first_column,
          	    n->location.last_line, n->location.last_column);
    		}
		printf(substr(tok, 2, strlen(tok)));
	    } else if (strcmp(substr(tok, 0, 1), "%t") == 0) {
		t = va_arg(args, struct type *);
		printf(" [kind=%s ", t? tstr[t->kind]: "");
		printf(", datatype=%s", t? bstr[t->data.basic.datatype]: "");
		t = peek_parent_type(t);
		printf(", parent=%s ] ", t? tstr[t->kind]: "");
		printf(substr(tok, 2, strlen(tok)));
	    } else if (strcmp(substr(tok, 0, 1), "%r") == 0) {
		ir = va_arg(args, struct ir_section *);
		printf(" [first.kind=%s,", ir && ir->first? rstr[ir->first->kind]:"");
		printf("last.kind=%s] ", ir && ir->last? rstr[ir->last->kind]:"");
		printf(substr(tok, 2, strlen(tok)));
	    } else if (strcmp(substr(tok, 0, 1), "%o") == 0) {
                op = va_arg(args, struct ir_operand *);
                if (op)
                  printf(" [kind=%s, ", opstr[op->kind]);
                if (op && op->kind == OPERAND_NUMBER)
                  printf("number=%d] ", op->data.number);
                else if (op && op->kind == OPERAND_TEMPORARY)
                  printf("temporary=%d] ", op->data.temporary);
                else if (op && op->kind == OPERAND_IDENTIFIER)
                  printf("name=%s] ", op->data.name);
		printf(substr(tok, 2, strlen(tok)));
            } else if (strcmp(substr(tok, 0, 1), "%d") == 0) {
	 	printf(tok, va_arg(args, int));
	    } else if (strcmp(substr(tok, 0, 1), "%l") == 0) {
		printf(tok, va_arg(args, long));
	    } else if (strcmp(substr(tok, 0, 1), "%s") == 0) {
		printf(tok, va_arg(args, char *));
	    } else if (strcmp(substr(tok, 0, 1), "%p") == 0) {
		printf(tok, va_arg(args, void *));
	    } else  
		printf(tok);
	    j = 0;
    	}
	tok[j++] = msg[i]; tok[j] = '\0';
	i++;
    }
    printf(tok);
}

#endif
