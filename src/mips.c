#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "type.h"
#include "symbol.h"
#include "ir.h"
#include "mips.h"
#include "debug.h"

#define REG_EXHAUSTED   -1

#define FIRST_USABLE_REGISTER  8
#define LAST_USABLE_REGISTER  23
#define NUM_REGISTERS         32
#define MAX_USABLE_REGISTER   4


/****************************
 * MIPS TEXT SECTION OUTPUT *
 ****************************/

static char const * const opcodes[] = {
  "nop",
  "mult",
  "div",
  "div",
  "add",
  "addi",
  "sub",
  "or",
  "xor",
  "and",
  "slt",
  "sll",
  "srl",
  "jr",
  "jal",
  "j",
  "beq",
  "bne",
  "data", // DNE
  "la", // Likely doesn't exist
  "li", // Likely DNE
  "lb",
  "lw",
  "sw",
  "sb",
  "add", // "copy" DNE on Qt SPIM, so replaced with "add"
  "addr", // Likely DNE
  "label", // Placeholder
  "decl", // Placeholder
  "pnum", // DNE
  "syscall",
  "procbgn", // Placeholder
  "procend", // Placeholder
  NULL 
};


void mips_print_temporary_operand(FILE *output, struct ir_operand *operand) {
  assert(OPERAND_TEMPORARY == operand->kind);

  fprintf(output, "%8s%02d", "$", operand->data.temporary + FIRST_USABLE_REGISTER);
}

void mips_print_temporary_offset_operand(FILE *output, struct ir_operand *operand, int offset) {
  assert(OPERAND_TEMPORARY == operand->kind);

  fprintf(output, "%8d(%s%02d)", offset, "$", operand->data.temporary + FIRST_USABLE_REGISTER);
}

void mips_print_number_operand(FILE *output, struct ir_operand *operand) {
  assert(OPERAND_NUMBER == operand->kind);
 
  if (operand->is_signed)
    fprintf(output, "%10ld", operand->data.signed_number);
  else
    fprintf(output, "%10lu", operand->data.number);
}

void mips_print_identifier_operand(FILE *output, struct ir_operand *operand) {
  assert(OPERAND_IDENTIFIER == operand->kind);

  fprintf(output, "%10s", operand->data.name);
}

void mips_print_arithmetic(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs(", ", output);
  mips_print_temporary_operand(output, &instruction->operands[1]);
  fputs(", ", output);
  mips_print_temporary_operand(output, &instruction->operands[2]);
  fputs("\n", output);
}

void mips_print_arithmetic_immediate(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs(", ", output);
  mips_print_temporary_operand(output, &instruction->operands[1]);
  fputs(", ", output);
  mips_print_number_operand(output, &instruction->operands[2]);
  fputs("\n", output);
}

void mips_print_div(FILE *output, struct ir_instruction *instruction) {
  mips_print_arithmetic(output, instruction);

  fprintf(output, "%10s ", instruction->kind == IR_DIVIDE? "mflo": "mfhi");
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs("\n", output);
}

void mips_print_branch(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs(", ", output);
  mips_print_temporary_operand(output, &instruction->operands[1]);
  fputs(", ", output);
  mips_print_identifier_operand(output, &instruction->operands[2]);
  fputs("\n", output);
}

void mips_print_address(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs(", ", output);
  mips_print_identifier_operand(output, &instruction->operands[1]);
  fputs("\n", output);
}

void mips_print_nop(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  /*fprintf(output, "%8s ", "0");
  fputs(", ", output);
  fprintf(output, "%8s ", "0");*/
  fputs("\n", output);
}

void mips_print_copy(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs(", ", output);
  mips_print_temporary_operand(output, &instruction->operands[1]);
  fprintf(output, ", %10s\n", "$0");
}

void mips_print_mult(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs(", ", output);
  mips_print_temporary_operand(output, &instruction->operands[1]);
  fputs("\n", output);

  fprintf(output, "%10s ", "mflo");
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs("\n", output);
}

void mips_print_load_immediate(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs(", ", output);
  mips_print_number_operand(output, &instruction->operands[1]);
  fputs("\n", output);
}

void mips_print_load_store_word(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fputs(", ", output);
  mips_print_temporary_offset_operand(output, &instruction->operands[1], instruction->offset);
  fputs("\n", output);
}

void mips_print_label(FILE *output, struct ir_instruction *instruction) {
  assert(instruction->operands[0].kind == OPERAND_IDENTIFIER);
  fprintf(output, "%s: %8s", instruction->operands[0].data.name, " ");
  if (&instruction->operands[1] && instruction->operands[1].data.name) {
    mips_print_identifier_operand(output, &instruction->operands[1]);
    fputs(", ", output);
  }
  if (&instruction->operands[2] && instruction->operands[2].data.name) 
    mips_print_identifier_operand(output, &instruction->operands[2]);
  fputs("\n", output);
}

void mips_print_jump(FILE *output, struct ir_instruction *instruction) {
  fprintf(output, "%10s ", opcodes[instruction->kind]);
  if (instruction->operands[0].kind == OPERAND_TEMPORARY) 
    mips_print_temporary_operand(output, &instruction->operands[0]);
  else
    mips_print_identifier_operand(output, &instruction->operands[0]);
  fputs("\n", output);
}

void mips_print_print_number(FILE *output, struct ir_instruction *instruction) {
  /* Print the number. */
  fprintf(output, "%10s %10s, %10s, %10d\n", "ori", "$v0", "$0", 1);
  fprintf(output, "%10s %10s, %10s, ", "or", "$a0", "$0");
  mips_print_temporary_operand(output, &instruction->operands[0]);
  fprintf(output, "\n%10s\n", "syscall");

  /* Print a newline. */
  fprintf(output, "%10s %10s, %10s, %10d\n", "ori", "$v0", "$0", 4);
  fprintf(output, "%10s %10s, %10s", "la", "$a0", "newline");
  fprintf(output, "\n%10s\n", "syscall");
}

bool is_valid_register(int reg) {
  int abs = reg + FIRST_USABLE_REGISTER; // absolute value of register 'reg'
  if (reg < MAX_USABLE_REGISTER+3 || abs >=24 && abs <=25 || abs >= 28 && abs <= 31)
    return true;
  return false;
}

int usable_register(int taken[], int offset) {
  int reg = 0;
  if (taken[offset%3] >= 0 && taken[offset%3] != offset)
    if (taken[(offset+1)%3] >= 0 && taken[(offset+1)%3] != offset) 
	reg = (offset+2)%3;
    else
        reg = (offset+1)%3;
  else
    reg = offset%3;
  taken[reg] = offset;
  return reg;
}

/* Use this function to limit the number of registers used. */
void mips_sanitize_instruction(FILE *output, struct ir_instruction *instruction) {
  struct ir_instruction *lw = ir_instruction(IR_LOAD_WORD);
  struct ir_instruction *sw[3] = { NULL, NULL, NULL };
  bool found = false;
  int i = 0;
  int offset = 0;
  int taken[3] = { -1, -1, -1 };
  int reg = 0;
  for (i=0; i<3; i++) {
    if (instruction->operands[i].kind == OPERAND_TEMPORARY && 
        !is_valid_register(instruction->operands[i].data.temporary)) {
	found = true;
	offset = instruction->operands[i].data.temporary - (MAX_USABLE_REGISTER + 3);
	reg = usable_register(taken, offset);
	ir_operand_register(lw, 0, MAX_USABLE_REGISTER + reg);
	ir_operand_register(lw, 1, MAX_USABLE_REGISTER + 3);
	lw->offset = 4 * offset;
	mips_print_load_store_word(output, lw);
	sw[i] = ir_instruction(IR_STORE_WORD);
	ir_operand_register(sw[i], 0, MAX_USABLE_REGISTER + reg);
	ir_operand_register(sw[i], 1, MAX_USABLE_REGISTER + 3);
	sw[i]->offset = 4 * offset;
	instruction->operands[i].data.temporary = MAX_USABLE_REGISTER + reg;
    }
  }
  mips_print_instruction(output, instruction);
  for (i=0; i<3; i++) {
    if (instruction->operands[i].kind == OPERAND_TEMPORARY && found && sw[i]) {
	mips_print_load_store_word(output, sw[i]);
    }
  }
}

void mips_print_instruction(FILE *output, struct ir_instruction *instruction) {
  debugf("mips_print_instruction.kind=%r\n", ir_section(instruction, instruction));
  switch (instruction->kind) {
    case IR_MULTIPLY:
      mips_print_mult(output, instruction);
      break;

    case IR_DIVIDE:
    case IR_MOD:
      mips_print_div(output, instruction);
      break;

    case IR_ADD:
    case IR_SUBTRACT:
    case IR_SLT:
    case IR_SLL:
    case IR_SRL:
    case IR_OR:
    case IR_AND:
    case IR_XOR:
      mips_print_arithmetic(output, instruction);
      break;

    case IR_BEQ:
    case IR_BNE:
      mips_print_branch(output, instruction);
      break;

    case IR_ADD_IMMEDIATE:
      mips_print_arithmetic_immediate(output, instruction);
      break;

    case IR_NO_OPERATION:
      mips_print_nop(output, instruction);
      break;

    case IR_COPY:
      mips_print_copy(output, instruction);
      break;

    case IR_LOAD_IMMEDIATE:
    case IR_DECL:
      mips_print_load_immediate(output, instruction);
      break;

    case IR_LOAD_ADDRESS:
      mips_print_address(output, instruction);
      break;

    case IR_LOAD_BYTE:
    case IR_LOAD_WORD:
    case IR_STORE_WORD:
    case IR_STORE_BYTE:
      mips_print_load_store_word(output, instruction);
      break;

    case IR_LABEL:
    case IR_PROC_BEGIN:
    case IR_PROC_END:
      mips_print_label(output, instruction);
      break;

    case IR_JR:
    case IR_JL:
    case IR_J:
      mips_print_jump(output, instruction);
      break;

    case IR_CALL:
      fprintf(output, "%10s \n", opcodes[instruction->kind]);
      break;
      
    case IR_ADDRESS_OF:
    case IR_PRINT_NUMBER:
      mips_print_print_number(output, instruction);
      break;

    default:
      assert(0);
      break;
  }
}

static char* mips_type(char *type) {
  if (strcmp(type, "int") == 0) return ".word";
  else if (strcmp(type, "long") == 0) return ".word";
  else if (strcmp(type, "char *") == 0) return ".ascii";
  return type;
}

void mips_sanitize_instruction(FILE *output, struct ir_instruction *instruction);

void mips_print_text_section(FILE *output, struct ir_section *section) {
  struct ir_instruction *instruction;
  struct ir_instruction *la = ir_instruction(IR_LOAD_ADDRESS);
  ir_operand_register(la, 0, MAX_USABLE_REGISTER + 3);
  ir_operand_identifier(la, 1, "reg_space");

  fputs("\n.data\nreg_space: .space 20000\nnewline: .asciiz \"\\n\"\n", output);
  for (instruction = section->first; instruction != section->last->next; instruction = instruction->next) {
    if (instruction->kind == IR_DATA) {
	if (instruction->operands[2].data.name)
	  fprintf(output, "%s: %s %s\n", instruction->operands[0].data.name, instruction->operands[1].data.name, instruction->operands[2].data.name);
	else 
	  fprintf(output, "%s: %s 0\n", instruction->operands[0].data.name, mips_type(instruction->operands[1].data.name));
    }
  }
  fputs("\n.text\n", output);


  for (instruction = section->first; instruction != section->last->next; instruction = instruction->next) {
    if (instruction->kind != IR_DATA)
      mips_sanitize_instruction(output, instruction);
    if (instruction->kind == IR_PROC_BEGIN && strcmp(instruction->operands[0].data.name, "main") == 0)
      mips_print_instruction(output, la);
  }

  /* Return from main. */
  //fprintf(output, "\n%10s %10s\n", "jr", "$ra");
}

void mips_print_program(FILE *output, struct ir_section *section) {
  mips_print_text_section(output, section);
}
